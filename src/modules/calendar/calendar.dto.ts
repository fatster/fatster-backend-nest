import { ApiProperty, OmitType } from '@nestjs/swagger';
import {
  IsArray,
  IsDateString,
  IsEnum,
  IsNotEmpty,
  IsString,
  IsUUID,
} from 'class-validator';

import { AbstractDto } from '../../common/dto/AbstractDto';
import { isDto } from '../../common/utils/entities';
import { CalendarEntity } from './calendar.entity';
import { CalendarEventType } from './calendar.types';

export class CalendarDto extends AbstractDto {
  @IsNotEmpty()
  @IsDateString()
  @ApiProperty()
  start: Date;

  @IsNotEmpty()
  @IsDateString()
  @ApiProperty()
  end: Date;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  title: string;

  @IsNotEmpty()
  @IsEnum(CalendarEventType)
  @ApiProperty({ enum: CalendarEventType })
  type: CalendarEventType;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  meetingUrl: string;

  @IsUUID('all')
  @IsNotEmpty()
  @ApiProperty()
  ownerId: string;

  @IsArray()
  @IsUUID('all', { each: true })
  @IsNotEmpty()
  @ApiProperty({ isArray: true })
  participants: string[];

  constructor(calendar: CalendarEntity) {
    super(calendar);
    Object.assign(this, calendar);

    this.participants = (calendar.participants || []).map((p) =>
      isDto(p) ? p.toDto() : p,
    );
  }
}

export class CreateCalendarDto extends OmitType(CalendarDto, <const>[
  'id',
  'ownerId',
]) {}
