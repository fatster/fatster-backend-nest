import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';

import { PayloadSuccessDto } from '../../common/dto/PayloadSuccessDto';
import { AuthUser } from '../../decorators/auth-user.decorator';
import { UserEntity } from '../../entities/user.entity';
import { AuthGuard } from '../../guards/auth.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
import { GetHashtagPostsDto } from './dto/getHashtagPosts.dto';
import { HashtagService } from './hashtag.service';

@Controller('hashtag')
@ApiTags('Hashtag')
export class HashtagController {
  constructor(private _hashtagService: HashtagService) {}

  @Get('/')
  @HttpCode(HttpStatus.OK)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get all static hashtags',
  })
  async getUsersPosts(): Promise<PayloadSuccessDto> {
    const hashtags = await this._hashtagService.getAllHashtags();

    return {
      message: 'All static hashtags',
      data: hashtags,
    };
  }

  @Get('/:hashtagText/posts')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get all static hashtags',
  })
  async getHashtagPosts(
    @Query() getHashtagPostsDto: GetHashtagPostsDto,
    @Param('hashtagText') hashtagText: string,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    getHashtagPostsDto.hashtagText = hashtagText;
    getHashtagPostsDto.user = user;

    const hashtag = await this._hashtagService.getHashtag(
      hashtagText,
    );
    const payload = { message: 'Posts with hashtag', data: null };

    const isNewUserHashtag =
      hashtag && hashtag.keyName === '#newuser';

    const data = await this._hashtagService.getPostsByHashtag(
      isNewUserHashtag ? 48 : null,
      hashtagText,
      getHashtagPostsDto,
    );

    return { ...payload, data };
  }
}
