import {
  ApiHideProperty,
  ApiProperty,
  ApiPropertyOptional,
} from '@nestjs/swagger';
import {
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  Min,
} from 'class-validator';

import { PostStatusEnum } from '../../../common/enum/enum';

export class PostWeightDto {
  @IsNumber()
  @IsNotEmpty()
  @Min(0)
  @ApiProperty()
  readonly weight: number;

  @IsBoolean()
  @IsOptional()
  @ApiPropertyOptional()
  postOnFeed?: boolean;

  @ApiPropertyOptional()
  @IsOptional()
  readonly text?: string;

  @ApiPropertyOptional()
  @IsOptional()
  readonly status: PostStatusEnum = PostStatusEnum.PUBLIC;

  @ApiPropertyOptional()
  @IsOptional()
  readonly friendsToTag: string[];

  @ApiPropertyOptional()
  @IsOptional()
  photos?: string[];

  @ApiHideProperty()
  userId: string;
}
