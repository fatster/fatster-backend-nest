'use strict';

import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';

export class UserVerificationCodeDto {
  @IsString()
  @ApiProperty()
  userId: string;

  @IsInt()
  @ApiProperty()
  verificationCode: number;
}
