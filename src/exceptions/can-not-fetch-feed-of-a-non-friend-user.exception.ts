'use strict';

import { BadRequestException } from '@nestjs/common';

export class CanNotFetchFeedOfANonFriendUser extends BadRequestException {
  constructor(error?: string) {
    super('error.can_not-fetch-feed-of-a-non-friend-user', error);
  }
}
