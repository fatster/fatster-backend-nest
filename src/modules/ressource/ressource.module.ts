import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ActivityRepository } from '../../repositories/activity.repository';
import { FoodRepository } from '../../repositories/food.repository';
import { UserPreferencesRepository } from '../../repositories/userPreferences.repository';
import { UserWeightRepository } from '../../repositories/userWeight.repository';
import { RessourceController } from './ressource.controller';
import { RessourceService } from './ressource.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      FoodRepository,
      ActivityRepository,
      UserPreferencesRepository,
      UserWeightRepository,
    ]),
  ],
  controllers: [RessourceController],
  exports: [RessourceService],
  providers: [RessourceService],
})
export class RessourceModule {}
