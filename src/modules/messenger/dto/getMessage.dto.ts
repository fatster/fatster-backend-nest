'use strict';

import { ApiHideProperty } from '@nestjs/swagger';

export class GetMessageDto {
  messengerRoomId: string;

  friendUserId: string;

  @ApiHideProperty()
  userId: string;

  constructor(
    messengerRoomId: string,
    userId: string,
    friendUserId: string,
  ) {
    this.friendUserId = friendUserId;
    this.messengerRoomId = messengerRoomId;
    this.userId = userId;
  }
}
