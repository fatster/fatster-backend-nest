'use strict';

import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString, MinLength } from 'class-validator';

export class ResetPasswordDto {
  @IsString()
  @MinLength(6)
  @ApiProperty({ minLength: 6 })
  newPassword: string;

  @IsString()
  @ApiProperty()
  userId: string;

  @IsInt()
  @ApiProperty()
  verificationCode: number;
}
