import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { isDto } from '../common/utils/entities';
import { ActivityEntity } from '../entities/activity.entity';
import { ActivityCategoryDto } from './activityCategory.dto';
import { UserActivityDto } from './userActivity.dto';

export class ActivityDto extends AbstractDto {
  @ApiPropertyOptional()
  textEn: string;

  @ApiPropertyOptional()
  textFr: string;

  @ApiPropertyOptional()
  met: number;

  @ApiPropertyOptional()
  activityCategory: ActivityCategoryDto;

  @ApiPropertyOptional()
  userActivity: UserActivityDto[];

  constructor(activity: ActivityEntity) {
    super(activity);
    this.textEn = activity.textEn;
    this.textFr = activity.textFr;
    this.met = activity.met;
    this.activityCategory = isDto(activity.activityCategory)
      ? activity.activityCategory.toDto()
      : <any>activity.activityCategory;
  }
}
