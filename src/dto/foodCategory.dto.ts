import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { FoodDto } from './food.dto';

export class FoodCategoryDto extends AbstractDto {
  @ApiPropertyOptional()
  textEn: string;

  @ApiPropertyOptional()
  textFr: string;

  @ApiPropertyOptional()
  foods: FoodDto[];
}
