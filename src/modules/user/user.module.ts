import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AccountModule } from '../../modules/account/account.module';
import { FriendModule } from '../../modules/friend/friend.module';
import { ActivityRepository } from '../../repositories/activity.repository';
import { BlockRepository } from '../../repositories/block.repository';
import { FriendRepository } from '../../repositories/friend.repository';
import { FriendRequestRepository } from '../../repositories/friendRequest.repository';
import { PostRepository } from '../../repositories/post.repository';
import { ReportRepository } from '../../repositories/report.repository';
import { UserRepository } from '../../repositories/user.repository';
import { UserActivityRepository } from '../../repositories/userActivity.repository';
import { UserNutritionRepository } from '../../repositories/userNutrition.repository';
import { UserNutritionFoodRepository } from '../../repositories/userNutritionFood.repository';
import { UserWeightRepository } from '../../repositories/userWeight.repository';
import { AuthModule } from '../auth/auth.module';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
  imports: [
    forwardRef(() => AuthModule),
    forwardRef(() => AccountModule),
    forwardRef(() => FriendModule),
    TypeOrmModule.forFeature([
      UserRepository,
      PostRepository,
      UserActivityRepository,
      UserWeightRepository,
      FriendRequestRepository,
      UserNutritionRepository,
      BlockRepository,
      ReportRepository,
      FriendRepository,
      ActivityRepository,
      UserNutritionFoodRepository,
    ]),
  ],
  controllers: [UserController],
  exports: [UserService],
  providers: [UserService],
})
export class UserModule {}
