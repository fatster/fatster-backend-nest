'use strict';

import {
  ApiHideProperty,
  ApiProperty,
  ApiPropertyOptional,
} from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  Max,
  Min,
} from 'class-validator';

import { PostStatusEnum } from '../../../common/enum/enum';

export class PostBeforeAfterDto {
  @IsOptional()
  readonly text?: string;

  @ApiProperty()
  @IsNotEmpty()
  readonly photoBefore?: string;

  @ApiProperty()
  @IsNotEmpty()
  readonly photoAfter?: string;

  @ApiPropertyOptional()
  @IsOptional()
  readonly status: PostStatusEnum = PostStatusEnum.PUBLIC;

  @IsNotEmpty()
  @IsNumber()
  @Min(50)
  @Max(200)
  @ApiProperty()
  readonly weightBefore?: number;

  @IsNotEmpty()
  @IsNumber()
  @Min(50)
  @Max(200)
  @ApiProperty()
  readonly weightAfter?: number;

  @ApiHideProperty()
  userId: string;
}
