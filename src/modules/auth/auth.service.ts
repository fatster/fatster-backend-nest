/* eslint-disable quote-props */
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { UserDto } from '../../dto/user.dto';
import { UserEntity } from '../../entities/user.entity';
import { VerificationCodeEntity } from '../../entities/verificationCode.entity';
import { InvalidVerificationCodeException } from '../../exceptions/invalid-verification-code.exception';
import { UserNotFoundException } from '../../exceptions/user-not-found.exception';
import { ContextService } from '../../providers/context.service';
import { UtilsService } from '../../providers/utils.service';
import { FirebaseAuthRepository } from '../../repositories/firebaseAuth.repository';
import { UserRepository } from '../../repositories/user.repository';
import { VerificationCodeRepository } from '../../repositories/verificationCode.repository';
import { ConfigService } from '../../shared/services/config.service';
import { MailGunService } from '../../shared/services/mailgun.service';
import { UserService } from '../user/user.service';
import { ResetPasswordDto } from './dto/resetPassword.dto';
import { TokenPayloadDto } from './dto/tokenPayload.dto';
import { UserLoginDto } from './dto/userLogin.dto';
import { UserMailDto } from './dto/userMail.dto';
import { UserVerificationCodeDto } from './dto/userVerificationCode.dto';

@Injectable()
export class AuthService {
  private static _authUserKey = 'user_key';

  constructor(
    private readonly _jwtService: JwtService,
    private readonly _configService: ConfigService,
    private readonly _userService: UserService,
    private readonly _userRepository: UserRepository,
    private readonly _verificationCodeRepository: VerificationCodeRepository,
    private readonly _firebaseAuthRepository: FirebaseAuthRepository,
    private readonly _mailgunService: MailGunService,
  ) {}

  async createToken(
    user: UserEntity | UserDto,
  ): Promise<TokenPayloadDto> {
    return new TokenPayloadDto({
      expiresIn: this._configService.getNumber('JWT_EXPIRATION_TIME'),
      accessToken: await this._jwtService.signAsync({ id: user.id }),
    });
  }

  async validateUser(
    userLoginDto: UserLoginDto,
  ): Promise<UserEntity> {
    const user = await this._userService.findOne({
      email: userLoginDto.email,
    });

    let isPasswordValid: boolean;

    if (
      user &&
      user.fromFirebase &&
      user.authMethod === 'email' &&
      !user.password
    ) {
      const oldHash = await this._firebaseAuthRepository.findOne({
        where: { user },
      });
      isPasswordValid = await UtilsService.validateHashFromFirebase(
        userLoginDto.password,
        oldHash && oldHash.hash,
        oldHash && oldHash.salt,
      );
      if (isPasswordValid) {
        user.password = UtilsService.generateHash(
          userLoginDto.password,
        );
        this._userRepository.save(user);
      }
    } else {
      isPasswordValid = await UtilsService.validateHash(
        userLoginDto.password,
        user && user.password,
      );
    }
    if (!user || !isPasswordValid) {
      throw new UserNotFoundException();
    }

    return user;
  }

  async sendCode(email: UserMailDto) {
    const userEmail = email.email;
    const user = await this._userService.findOne({
      email: userEmail,
    });
    if (!user) {
      throw new UserNotFoundException();
    }

    const passwordHashCode = UtilsService.generateVerificationCode();
    const userVerificationCodeDto = new UserVerificationCodeDto();
    userVerificationCodeDto.userId = user.id;
    userVerificationCodeDto.verificationCode = passwordHashCode;

    await this._mailgunService.sendEmail({
      subject: userVerificationCodeDto.verificationCode,
      from: `Fatster <hi@fatster.app>`,
      to: userEmail,
      template: 'reset_password',
      'h:X-Mailgun-Variables': JSON.stringify({
        code: userVerificationCodeDto.verificationCode,
      }),
    });

    await this.createVerificationEntity(userVerificationCodeDto);
    return userVerificationCodeDto;
  }

  async createVerificationEntity(
    userVerificationCodeDto: UserVerificationCodeDto,
  ): Promise<VerificationCodeEntity> {
    const verificationCodeEntity = this._verificationCodeRepository.create();
    verificationCodeEntity.userId = userVerificationCodeDto.userId;
    verificationCodeEntity.verificationCode =
      userVerificationCodeDto.verificationCode;
    verificationCodeEntity.isValid = true;

    return this._verificationCodeRepository.save(
      verificationCodeEntity,
    );
  }

  async enterVerificationCode(
    userVerificationCodeDto: UserVerificationCodeDto,
  ) {
    const userId = userVerificationCodeDto.userId;
    const verificationCode = await this._verificationCodeRepository
      .createQueryBuilder('codes')
      .where('codes.userId = :userID', {
        userID: userId,
      })
      .orderBy('created_at', 'DESC')
      .getOne();

    const currentTime = new Date().getTime();
    const createdTime = verificationCode.createdAt.getTime();
    if (currentTime - createdTime > 60 * 60 * 10000) {
      verificationCode.isValid = false;
    }

    const isCodeValid =
      verificationCode.verificationCode ===
        userVerificationCodeDto.verificationCode &&
      verificationCode.isValid;
    if (!isCodeValid) {
      await this._verificationCodeRepository.save(verificationCode);
      throw new InvalidVerificationCodeException();
    }
    return userVerificationCodeDto;
  }

  async resetPassword(resetPasswordDto: ResetPasswordDto) {
    const userVerificationCodeDto = new UserVerificationCodeDto();
    userVerificationCodeDto.userId = resetPasswordDto.userId;
    userVerificationCodeDto.verificationCode =
      resetPasswordDto.verificationCode;

    const isValid = await this.enterVerificationCode(
      userVerificationCodeDto,
    );
    if (!isValid) {
      throw new InvalidVerificationCodeException();
    }

    const verificationCode = await this._verificationCodeRepository
      .createQueryBuilder('codes')
      .where('codes.userId = :userID', {
        userID: userVerificationCodeDto.userId,
      })
      .orderBy('created_at', 'DESC')
      .getOne();
    verificationCode.isValid = false;
    await this._verificationCodeRepository.save(verificationCode);

    const user = await this._userService.findOne({
      id: userVerificationCodeDto.userId,
    });
    user.password = UtilsService.generateHash(
      resetPasswordDto.newPassword,
    );

    return this._userRepository.save(user);
  }

  async logout(userId: string) {
    // Todo revoke the JWT token
    return this._userRepository.update(userId, {
      oneSignalPlayerId: null,
    });
  }

  static setAuthUser(user: UserEntity) {
    ContextService.set(AuthService._authUserKey, user);
  }

  static getAuthUser(): UserEntity {
    return ContextService.get(AuthService._authUserKey);
  }
}
