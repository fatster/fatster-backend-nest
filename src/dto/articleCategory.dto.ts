import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { ArticleDto } from './article.dto';

export class ArticleCategoryDto extends AbstractDto {
  @ApiPropertyOptional()
  typeEn: string;

  @ApiPropertyOptional()
  typeFr: string;

  @ApiPropertyOptional()
  article: ArticleDto[];
}
