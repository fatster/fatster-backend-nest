import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';

import { PayloadSuccessDto } from '../../../common/dto/PayloadSuccessDto';
import { AuthUser } from '../../../decorators/auth-user.decorator';
import { CreateCalendarSettingsDto } from '../../../dto/calendarSettings.dto';
import { CreateProfessionalInfoDto } from '../../../dto/professionalInfo.dto';
import { UserDto } from '../../../dto/user.dto';
import { UserEntity } from '../../../entities/user.entity';
import { AuthGuard } from '../../../guards/auth.guard';
import { AuthUserInterceptor } from '../../../interceptors/auth-user-interceptor.service';
import { ROOT_CONTROLLER_PATH } from '../pro.constants';
import { ProService } from '../services/pro.service';

@Controller(ROOT_CONTROLLER_PATH)
@ApiTags('Pro')
export class RootController {
  constructor(private readonly _proService: ProService) {}

  @Post('/onboard')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    type: UserDto,
    description: 'Successfully onboard',
  })
  public async onboarding(
    @AuthUser() user: UserEntity,
    @Body() onboardingDto: CreateProfessionalInfoDto,
  ): Promise<PayloadSuccessDto> {
    const { id } = user;
    const data = await this._proService.saveProByIdWithRelations({
      id,
      infoDto: onboardingDto,
    });

    return {
      data,
      message: 'Successfully onboard',
    };
  }

  @Post('/calendarSettings')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({
    type: UserDto,
    description: 'Successfully calendar set',
  })
  public async calendarSettings(
    @AuthUser() user: UserEntity,
    @Body() calendarSettingsDto: CreateCalendarSettingsDto,
  ): Promise<PayloadSuccessDto> {
    const { id } = user;
    const data = await this._proService.saveProByIdWithRelations({
      id,
      calendarSettingsDto,
    });

    return {
      data,
      message: 'Successfully calendar set',
    };
  }
}
