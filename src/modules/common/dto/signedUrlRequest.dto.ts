import { IsEnum, IsNotEmpty, IsString } from 'class-validator';

import { TypeUploadEnum } from '../../../common/enum/enum';

export class SignedUrlRequestDto {
  @IsString()
  @IsNotEmpty()
  readonly mimeType: string;

  @IsEnum(TypeUploadEnum)
  readonly type: TypeUploadEnum;
}
