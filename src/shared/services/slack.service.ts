import { HttpService, Injectable } from '@nestjs/common';

import { ConfigService } from './config.service';

@Injectable()
export class SlackService {
  private readonly _web;
  constructor(
    public configService: ConfigService,
    private readonly _httpServices: HttpService,
  ) {}

  async sendToSlack(text: string) {
    return this._httpServices
      .post(
        'https://hooks.slack.com/services/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',
        JSON.stringify({ text }),
      )
      .toPromise();
  }

  async sendToContactChannel(text: string) {
    return this._httpServices
      .post(
        'https://hooks.slack.com/services/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',
        JSON.stringify({ text }),
      )
      .toPromise();
  }

  async sendToSlackFoodChannel(text: string) {
    return this._httpServices
      .post(
        'https://hooks.slack.com/services/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',
        JSON.stringify({ text }),
      )
      .toPromise();
  }
}
