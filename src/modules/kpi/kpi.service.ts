import { Injectable } from '@nestjs/common';

import { MessengerMessageRepository } from '../../repositories/messengerMessage.repository';
import { NextFeaturesRepository } from '../../repositories/nextFeatures.repository';
import { PhotoRepository } from '../../repositories/photo.repository';
import { PostRepository } from '../../repositories/post.repository';
import { UserRepository } from '../../repositories/user.repository';
import { UserActivityRepository } from '../../repositories/userActivity.repository';
import { UserWeightRepository } from '../../repositories/userWeight.repository';

@Injectable()
export class KpiService {
  constructor(
    private readonly _userRepository: UserRepository,
    private readonly _userWeightRepository: UserWeightRepository,
    private readonly _postRepository: PostRepository,
    private readonly _photoRepository: PhotoRepository,
    private readonly _userActivityRepository: UserActivityRepository,
    private readonly _messengerMessageRepository: MessengerMessageRepository,
    private readonly _nextFeaturesRepository: NextFeaturesRepository,
  ) {}

  async totalUsers(startDate: Date, endDate: Date) {
    const table = await this._userRepository
      .createQueryBuilder('user')
      .select('DATE(user.createdAt)')
      .addSelect('COUNT (user.id) as daily_total')
      .where(':startDate < user.createdAt', { startDate })
      .andWhere('user.createdAt < :endDate', { endDate })
      .groupBy('DATE(user.createdAt)')
      .getRawMany();

    let total = 0;
    table.forEach((element) => {
      total += Number(element.daily_total);
    });

    return { table, total };
  }

  async totalPosts(startDate: Date, endDate: Date) {
    const table = await this._postRepository
      .createQueryBuilder('post')
      .select('DATE(post.createdAt)')
      .addSelect('COUNT (post.id) as daily_total')
      .where(':startDate < post.createdAt', { startDate })
      .andWhere('post.createdAt < :endDate', { endDate })
      .groupBy('DATE(post.createdAt)')
      .getRawMany();

    let total = 0;
    table.forEach((element) => {
      total += Number(element.daily_total);
    });

    return { table, total };
  }

  async totalActivity(startDate: Date, endDate: Date) {
    const table = await this._userActivityRepository
      .createQueryBuilder('activity')
      .select('DATE(activity.createdAt)')
      .addSelect('SUM(activity.duration) as daily_total')
      .where(':startDate < activity.createdAt', { startDate })
      .andWhere('activity.createdAt < :endDate', { endDate })
      .groupBy('DATE(activity.createdAt)')
      .getRawMany();

    let total = 0;
    table.forEach((element) => {
      total += Number(element.daily_total);
    });

    return { table, total };
  }

  async totalPhotos(startDate: Date, endDate: Date) {
    const table = await this._photoRepository
      .createQueryBuilder('photo')
      .select('DATE(photo.createdAt)')
      .addSelect('COUNT (photo.id) as daily_total')
      .where(':startDate < photo.createdAt', { startDate })
      .andWhere('photo.createdAt < :endDate', { endDate })
      .groupBy('DATE(photo.createdAt)')
      .getRawMany();

    let total = 0;
    table.forEach((element) => {
      total += Number(element.daily_total);
    });

    return { table, total };
  }

  async totalMessages(startDate: Date, endDate: Date) {
    const table = await this._messengerMessageRepository
      .createQueryBuilder('message')
      .select('DATE(message.createdAt)')
      .addSelect('COUNT (message.id) as daily_total')
      .where(':startDate < message.createdAt', { startDate })
      .andWhere('message.createdAt < :endDate', { endDate })
      .groupBy('DATE(message.createdAt)')
      .getRawMany();

    let total = 0;
    table.forEach((element) => {
      total += Number(element.daily_total);
    });

    return { table, total };
  }

  async totalWeight(startDate: Date, endDate: Date) {
    const weightLostQuery = this._userWeightRepository
      .createQueryBuilder('userWeight')
      .select('SUM(userWeight.weightDifference) as total')
      .where('userWeight.weightDifference < 0')
      .andWhere(':startDate < userWeight.createdAt', { startDate })
      .andWhere('userWeight.createdAt < :endDate', { endDate })
      .getRawOne();

    const weightGainedQuery = this._userWeightRepository
      .createQueryBuilder('userWeight')
      .select('SUM(userWeight.weightDifference) as total')
      .where('userWeight.weightDifference > 0')
      .andWhere(':startDate < userWeight.createdAt', {
        startDate,
      })
      .andWhere('userWeight.createdAt < :endDate', { endDate })
      .getRawOne();

    const [weightLost, weightGained] = await Promise.all([
      weightLostQuery,
      weightGainedQuery,
    ]);

    return {
      lost: Math.abs(weightLost.total),
      gained: weightGained.total,
    };
  }

  async activeUsers(startDate: Date, endDate: Date) {
    const table = await this._userRepository
      .createQueryBuilder('user')
      .select('DATE(user.lastConnection)')
      .addSelect('COUNT (user.id) as daily_total')
      .where(':startDate < user.lastConnection', { startDate })
      .andWhere('user.lastConnection < :endDate', { endDate })
      .groupBy('DATE(user.lastConnection)')
      .getRawMany();

    let total = 0;
    table.forEach((element) => {
      total += Number(element.daily_total);
    });

    return { table, total };
  }

  async genderActiveUsers(startDate: Date, endDate: Date) {
    return this._userRepository
      .createQueryBuilder('user')
      .select('user.gender')
      .addSelect('COUNT (user.id) as total')
      .where(':startDate < user.lastConnection', { startDate })
      .andWhere('user.lastConnection < :endDate', { endDate })
      .groupBy('user.gender')
      .getRawMany();
  }

  async ageActiveUsers(startDate: Date, endDate: Date) {
    return this._userRepository
      .createQueryBuilder('user')
      .select('user.age')
      .addSelect('COUNT (user.id) as total')
      .where(':startDate < user.lastConnection', { startDate })
      .andWhere('user.lastConnection < :endDate', { endDate })
      .groupBy('user.age')
      .getRawMany();
  }

  async activityTypes(startDate: Date, endDate: Date) {
    return this._userActivityRepository
      .createQueryBuilder('userActivity')
      .select('SUM(userActivity.duration) as total')
      .addSelect('activity.textFr')
      .leftJoin('userActivity.activity', 'activity')
      .groupBy('activity.textFr')
      .where(':startDate < userActivity.createdAt', { startDate })
      .andWhere('userActivity.createdAt < :endDate', { endDate })
      .getRawMany();
  }

  async weightEvolutionProportion(startDate: Date, endDate: Date) {
    const users = await this._userWeightRepository
      .createQueryBuilder('userWeight')
      .select('DISTINCT("userWeight".user_id)')
      .addSelect('SUM(userWeight.weightDifference)', 'sum')
      .where(':startDate < userWeight.createdAt', { startDate })
      .andWhere('userWeight.createdAt < :endDate', { endDate })
      .groupBy('"userWeight".user_id')
      .getRawMany();

    let gainCount = 0;
    let lossCount = 0;
    users.forEach((user) => {
      if (user.sum > 0) {
        gainCount += 1;
      } else if (user.sum < 0) {
        lossCount += 1;
      }
    });
    return { lost: lossCount, gain: gainCount, total: users.length };
  }

  async averageActivity(startDate: Date, endDate: Date) {
    const activeUsers = this.activeUsers(startDate, endDate);
    const activity = this.totalActivity(startDate, endDate);
    return Math.round(
      (await activity).total / (await activeUsers).total,
    );
  }

  async averageWeight(startDate: Date, endDate: Date) {
    const avg = await this._userWeightRepository
      .createQueryBuilder('userWeight')
      .select('AVG(userWeight.weight)', 'avg')
      .where(':startDate < userWeight.createdAt', { startDate })
      .andWhere('userWeight.createdAt < :endDate', { endDate })
      .getRawOne();

    return Number(avg.avg.toFixed(1));
  }

  async bmiProportion(startDate: Date, endDate: Date) {
    // IMPROVE THIS
    const users = await this._userRepository
      .createQueryBuilder('user')
      .select(['id', 'height'])
      .where(':startDate < user.createdAt', { startDate }) // Change to user.lastConnection when updated
      .andWhere('user.createdAt < :endDate', { endDate }) // Change to user.lastConnection when updated
      .getRawMany();

    const lastWeightQueries = [];
    users.forEach((user) => {
      lastWeightQueries.push(
        this._userWeightRepository
          .createQueryBuilder('userWeight')
          .where('userWeight.user = :id', { id: user.id })
          .orderBy('userWeight.createdAt', 'DESC')
          .getRawOne(),
      );
    });
    const userLastWeights = await Promise.all(lastWeightQueries);

    const bmis = [];
    users.forEach((user) => {
      userLastWeights.forEach((weight) => {
        if (weight && weight.userWeight_user_id === user.id) {
          bmis.push({
            userId: user.id,
            bmi: Number(
              (
                weight.userWeight_weight /
                Math.pow(user.height / 100, 2)
              ).toFixed(1),
            ),
          });
        }
      });
    });

    let low = 0,
      normal = 0,
      overweight = 0,
      obese1 = 0,
      obese2 = 0,
      obese3 = 0;
    bmis.forEach((bmi) => {
      if (bmi.bmi < 18.5) {
        low += 1;
      } else if (18.5 < bmi.bmi && bmi.bmi < 25) {
        normal += 1;
      } else if (25 < bmi.bmi && bmi.bmi < 30) {
        overweight += 1;
      } else if (28 < bmi.bmi && bmi.bmi < 35) {
        obese1 += 1;
      } else if (35 < bmi.bmi && bmi.bmi < 40) {
        obese2 += 1;
      } else if (40 < bmi.bmi) {
        obese3 += 1;
      }
    });

    return {
      low,
      normal,
      overweight,
      obese1,
      obese2,
      obese3,
    };
  }

  async nextFeaturesStats() {
    return this._nextFeaturesRepository
      .createQueryBuilder('feature')
      .select(['feature.name', 'feature.likesCount'])
      .orderBy('feature.likesCount', 'DESC')
      .getRawMany();
  }
}
