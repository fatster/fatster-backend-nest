'user strict';

import { UserDto } from '../../../dto/user.dto';
import { UserEntity } from '../../../entities/user.entity';

export class GetFriendsSuggestionsDto {
  readonly user: UserDto;

  constructor(user: UserEntity) {
    this.user = user.toDto();
  }
}
