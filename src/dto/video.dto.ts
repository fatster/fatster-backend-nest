import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { VideoEntity } from '../entities/video.entity';
import { PostDto } from './post.dto';

export class VideoDto extends AbstractDto {
  @ApiPropertyOptional()
  path: string;

  @ApiPropertyOptional()
  post: PostDto;

  constructor(video: VideoEntity) {
    super(video);
    this.path = video.path;
  }
}
