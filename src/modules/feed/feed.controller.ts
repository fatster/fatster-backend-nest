import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOkResponse,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';

import { PayloadSuccessDto } from '../../common/dto/PayloadSuccessDto';
import { LikeTypeEnum, PostTypeEnum } from '../../common/enum/enum';
import { AuthUser } from '../../decorators/auth-user.decorator';
import { UserEntity } from '../../entities/user.entity';
import { AuthGuard } from '../../guards/auth.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
import { IFile } from '../../interfaces/IFile';
import { Post as IPost } from '../../interfaces/Post';
import { FileUploadDto } from '../../modules/account/dto/fileUpload.dto';
import { AddCommentDto } from './dto/addComment.dto';
import { AddNutritionDto } from './dto/addNutrition.dto';
import { PostBeforeAfterDto } from './dto/beforeAfter.dto';
import { DeletePostDto } from './dto/deletePost.dto';
import { EditCommentDto } from './dto/editComment.dto';
import { GetPostsDto } from './dto/get-posts.dto';
import { GetFriendsPostsDto } from './dto/getFriendsPosts.dto';
import { GetPostByIdDto } from './dto/getPostById.dto';
import { GetTrendingPostsDto } from './dto/getTrendingPosts.dto';
import { GetUsersPostsDto } from './dto/getUsersPosts.dto';
import { PostActivityDto } from './dto/postActivity.dto';
import { PostWeightDto } from './dto/postWeight.dto';
import { RemoveCommentDto } from './dto/removeComment.dto';
import { ShareArticleDto } from './dto/shareArticle.dto';
import { UpdatePostDto } from './dto/updatePost.dto';
import { WritePostDto } from './dto/writePost.dto';
import {
  DefaultImages,
  getDefaultImageUrlByType,
} from './feed.consts';
import { PostTypeValidationPipe } from './pipes/post-validation.pipe';
import { FeedActionService } from './services/action.service';
import { FeedPostService } from './services/feed-post.service';
import { FeedFeedService } from './services/feed.service';
import { NutritionPostService } from './services/nutrition-post.service';
import { PostService } from './services/post.service';

@Controller('feed')
@ApiTags('Feed')
export class FeedController {
  constructor(
    private _postService: PostService,
    private _feedFeedService: FeedFeedService,
    private _feedPostService: FeedPostService,
    private _feedActionService: FeedActionService,
    private _nutritionPostService: NutritionPostService,
  ) {}

  @Get('/post/user')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get Trending Posts',
  })
  async getUsersPosts(
    @Query()
    getUsersPostsDto: GetUsersPostsDto,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const usersPosts = await this._feedFeedService.getUsersPosts(
      getUsersPostsDto,
      user.toDto(),
    );

    return {
      message: 'Users posts',
      data: usersPosts,
    };
  }

  @Get('/post/trending')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get Trending Posts',
  })
  async getTrendingPosts(
    @Query()
    getTrendingPostsDto: GetTrendingPostsDto,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    getTrendingPostsDto.user = user;
    const trendingPosts = await this._feedFeedService.getTrendingPosts(
      getTrendingPostsDto,
    );

    return {
      message: 'Trending posts',
      data: trendingPosts,
    };
  }

  @Get('/trending/post/ids')
  @HttpCode(HttpStatus.OK)
  // @UseGuards(AuthGuard)
  // @UseInterceptors(AuthUserInterceptor)
  // @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get Trending Posts Ids',
  })
  async getTrendingPostsIds(
    @Query()
    getTrendingPostsDto: GetTrendingPostsDto,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    getTrendingPostsDto.user = user;
    // const trendingPosts = await this._feedFeedService.getTrendingPostsIds(
    //   getTrendingPostsDto,
    // );

    return {
      message: 'Trending posts',
      data: null,
    };
  }

  @Get('/post/friends')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get Friends Posts',
  })
  async getFriendsPosts(
    @Query() getFriendsPostsDto: GetFriendsPostsDto,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    getFriendsPostsDto.user = user;

    const friendsPosts = await this._feedFeedService.getFriendsPosts(
      getFriendsPostsDto,
    );

    return {
      message: 'Friends posts',
      data: friendsPosts,
    };
  }

  @Get('/post/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @ApiOkResponse({
    description: 'Get Post By ID',
  })
  async getPostById(
    @Param('id') id: string,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const getPostByIdRequestDto = new GetPostByIdDto(id);
    const createdPost = await this._feedFeedService.getPostById(
      getPostByIdRequestDto,
      user,
    );

    return {
      message: 'Post has been found',
      data: createdPost,
    };
  }

  @Put('/post/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @ApiOkResponse({
    description: 'Update Post',
  })
  async updatePost(
    @Param('id') postId: string,
    @AuthUser() user: UserEntity,
    @Body() updatePostDto: UpdatePostDto,
  ): Promise<PayloadSuccessDto> {
    updatePostDto.userId = user.id;

    const post = await this._feedPostService.updatePost(
      updatePostDto,
      postId,
    );

    return { message: 'post has been updated', data: post };
  }

  @Post('/post/')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'post added',
  })
  async writePost(
    @Body() writePostDto: WritePostDto,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    writePostDto.userId = user.id;
    const useDefaultImage = !(
      writePostDto.photos && writePostDto.photos.length
    );
    if (useDefaultImage) {
      writePostDto.photos = [
        getDefaultImageUrlByType(DefaultImages.ARTICLE),
      ];
    }
    const post = await this._feedPostService.writePost(
      writePostDto,
      useDefaultImage,
    );

    return { message: 'post has been added', data: post };
  }

  @Get('/posts')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'post search by date',
  })
  public async getPosts(
    @Query() getPostsDto: GetPostsDto,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const data = await this._postService.getPosts(getPostsDto, user);

    return {
      data,
      message: `list of posts`,
    };
  }

  @Post('/posts')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'post added',
  })
  public async createPost(
    @Body(new PostTypeValidationPipe()) post: IPost,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const userId = user.id;

    switch (post.type) {
      case PostTypeEnum.WEIGHT: {
        const weightPost = await this._postService.createWeightPost(
          post,
          userId,
        );
        return {
          message: `post type ${PostTypeEnum.WEIGHT} has been added`,
          data: weightPost,
        };
      }
      case PostTypeEnum.FOOD: {
        const foodPost = await this._nutritionPostService.post(
          post,
          userId,
        );
        return {
          message: `post type ${PostTypeEnum.FOOD} has been added`,
          data: foodPost,
        };
      }

      default:
        return {
          message: `${post.type} doesn't exist`,
          data: null,
        };
    }
  }

  @Post('/post/activity')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Post Activity',
  })
  async postActivity(
    @AuthUser() user: UserEntity,
    @Body() postActivityDto: PostActivityDto,
  ): Promise<PayloadSuccessDto> {
    postActivityDto.userId = user.id;
    const useDefaultImage = !(
      postActivityDto.photos && postActivityDto.photos.length
    );
    if (useDefaultImage) {
      postActivityDto.photos = [
        getDefaultImageUrlByType(DefaultImages.ACTIVITY),
      ];
    }
    const post = await this._feedPostService.postActivity(
      postActivityDto,
      useDefaultImage,
    );

    return { message: 'Post has been created', data: post };
  }

  @Post('/post/nutrition')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Post Nutrition',
  })
  async postNutrition(
    @AuthUser() user: UserEntity,
    @Body() addNutritionDto: AddNutritionDto,
  ): Promise<PayloadSuccessDto> {
    addNutritionDto.userId = user.id;

    const createdPost = await this._feedPostService.postNutrition(
      addNutritionDto,
    );

    return {
      message: 'Post has been created',
      data: createdPost,
    };
  }

  @Post('/post/weight')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Post Weight',
  })
  async postWeight(
    @Body() postWeightDto: PostWeightDto,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    postWeightDto.userId = user.id;
    const useDefaultImage = !(
      postWeightDto.photos && postWeightDto.photos.length
    );
    if (useDefaultImage) {
      postWeightDto.photos = [
        getDefaultImageUrlByType(DefaultImages.WEIGHT),
      ];
    }
    const createdPost = await this._feedPostService.postWeight(
      postWeightDto,
      useDefaultImage,
    );

    return { message: 'weight has been posted', data: createdPost };
  }

  @Post('/post/beforeafter')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Post Before After',
  })
  async postBeforeAftter(
    @Body() postBeforeAfterDto: PostBeforeAfterDto,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    postBeforeAfterDto.userId = user.id;

    const createdPost = await this._feedPostService.postBeforeAfter(
      postBeforeAfterDto,
    );

    return {
      message: 'before/after has been posted',
      data: createdPost,
    };
  }

  @Post('/post/shareArticle')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Share article',
  })
  async shareArticle(
    @Body() shareArticleDto: ShareArticleDto,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    shareArticleDto.userId = user.id;
    const createdPost = await this._feedPostService.shareArticle(
      shareArticleDto,
    );

    return { message: 'article has been shared', data: createdPost };
  }

  @Delete('/post/:postId')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiParam({ name: 'postId', type: String })
  @ApiOkResponse({
    description: 'Delete Post',
  })
  async deletePost(
    @AuthUser() user: UserEntity,
    @Param() deletePostDto: DeletePostDto,
  ): Promise<PayloadSuccessDto> {
    deletePostDto.userId = user.id;
    const post = await this._feedPostService.deletePost(
      deletePostDto,
      user,
    );

    return { message: 'post has beed deleteed', data: post };
  }

  @Get('/post/:id/likes')
  @ApiParam({ name: 'id', type: String })
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get users who liked a post',
  })
  async getPostLikes(
    @AuthUser() me: UserEntity,
    @Param('id') postId,
  ): Promise<PayloadSuccessDto> {
    const results = await this._feedActionService.getLikes(
      postId,
      me,
      LikeTypeEnum.POST,
    );

    return {
      message: 'Users who liked the post',
      data: results,
    };
  }

  @Post('/post/:id/like')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @ApiOkResponse({
    description: 'Like Post',
  })
  async likePost(
    @AuthUser() user: UserEntity,
    @Param('id') postId,
  ): Promise<PayloadSuccessDto> {
    await this._feedActionService.likePost(user.id, postId);

    return { message: 'Post has been liked' };
  }

  @Post('/post/:id/unlike')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @ApiOkResponse({
    description: 'UnLike Post',
  })
  async unLikePost(
    @AuthUser() user: UserEntity,
    @Param('id') postId,
  ): Promise<PayloadSuccessDto> {
    await this._feedActionService.unLikePost(user.id, postId);

    return { message: 'Post has been unliked' };
  }

  @Get('/post/:id/comments')
  @ApiParam({ name: 'id', type: String })
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get Comments',
  })
  async getComments(
    @AuthUser() user: UserEntity,
    @Param('id') postId,
  ): Promise<PayloadSuccessDto> {
    const comments = await this._feedActionService.getComments(
      user.id,
      postId,
    );

    return {
      message: 'Comments have been retrieved.',
      data: comments,
    };
  }

  @Post('/post/:id/comment')
  @ApiParam({ name: 'id', type: String })
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Add Comment',
  })
  async addComment(
    @AuthUser() user: UserEntity,
    @Body() addCommentRequestDto: AddCommentDto,
    @Param('id') postId,
  ): Promise<PayloadSuccessDto> {
    addCommentRequestDto.userId = user.id;
    addCommentRequestDto.postId = postId;

    const comment = await this._feedActionService.addComment(
      addCommentRequestDto,
    );

    return { message: 'comment has been added', data: comment };
  }

  @Put('/post/comment/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @ApiOkResponse({
    description: 'Edit Comment',
  })
  async editComment(
    @AuthUser() user: UserEntity,
    @Body() editCommentDto: EditCommentDto,
    @Param('id') commentId,
  ): Promise<PayloadSuccessDto> {
    editCommentDto.userId = user.id;
    editCommentDto.commentId = commentId;

    const editedComment = await this._feedActionService.editComment(
      editCommentDto,
    );
    return {
      message: 'comment has been edited',
      data: editedComment,
    };
  }

  @Delete('/post/comment/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @ApiOkResponse({
    description: 'Delete Comment',
  })
  async removeComment(
    @AuthUser() user: UserEntity,
    @Param('id') commentId,
  ): Promise<PayloadSuccessDto> {
    const removeCommentRequestDto = new RemoveCommentDto(
      commentId,
      user.id,
    );

    const comment = await this._feedActionService.removeComment(
      removeCommentRequestDto,
    );

    return { message: 'Comment has been deleted', data: comment };
  }

  @Get('/post/comment/:id/likes')
  @ApiParam({ name: 'id', type: String })
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get users who liked a comment',
  })
  async getCommentLikes(
    @AuthUser() me: UserEntity,
    @Param('id') commentId,
  ): Promise<PayloadSuccessDto> {
    const results = await this._feedActionService.getLikes(
      commentId,
      me,
      LikeTypeEnum.COMMENT,
    );

    return {
      message: 'Users who liked the comment',
      data: results,
    };
  }

  @Post('/post/comment/:id/like')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @ApiOkResponse({
    description: 'Like Comment',
  })
  async likeComment(
    @AuthUser() user: UserEntity,
    @Param('id') commentId,
  ): Promise<PayloadSuccessDto> {
    await this._feedActionService.likeComment(user.id, commentId);

    return { message: 'Comment has been liked' };
  }

  @Post('/post/comment/:id/unlike')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @ApiOkResponse({
    description: 'UnLike Comment',
  })
  async unLikeComment(
    @AuthUser() user: UserEntity,
    @Param('id') commentId,
  ): Promise<PayloadSuccessDto> {
    await this._feedActionService.unLikeComment(user.id, commentId);

    return { message: 'Comment has been unliked' };
  }

  @Get('/post/:id/tags')
  @ApiParam({ name: 'id', type: String })
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get Tags',
  })
  async getTags(
    @AuthUser() user: UserEntity,
    @Param('id') postId,
  ): Promise<PayloadSuccessDto> {
    const tags = await this._feedActionService.getTags(
      user.id,
      postId,
    );

    return {
      message: 'Tags have been retrieved.',
      data: tags,
    };
  }

  @Post('/post/photo')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FileInterceptor('file'))
  @ApiBearerAuth()
  @ApiBody({
    description: 'Profile pic',
    type: FileUploadDto,
  })
  @ApiOkResponse({
    description: 'profile picture updated',
  })
  async uploadPhoto(
    @AuthUser() user: UserEntity,
    @UploadedFile() photo: IFile,
  ): Promise<PayloadSuccessDto> {
    const photoId = await this._feedPostService.uploadPhoto(photo);

    return {
      message: 'the photo has been uploaded',
      data: { photoId },
    };
  }
}
