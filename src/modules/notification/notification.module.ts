import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CommentRepository } from '../../repositories/comment.repository';
import { FriendRepository } from '../../repositories/friend.repository';
import { FriendRequestRepository } from '../../repositories/friendRequest.repository';
import { LikeRepository } from '../../repositories/like.repository';
import { NotificationRepository } from '../../repositories/notification.repository';
import { UserRepository } from '../../repositories/user.repository';
import { UserPreferencesRepository } from '../../repositories/userPreferences.repository';
import { NotificationController } from './notification.controller';
import { NotificationService } from './notification.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      NotificationRepository,
      CommentRepository,
      LikeRepository,
      FriendRequestRepository,
      FriendRepository,
      UserRepository,
      UserPreferencesRepository,
    ]),
  ],
  controllers: [NotificationController],
  exports: [NotificationService],
  providers: [NotificationService],
})
export class NotificationModule {}
