import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthModule } from '../../modules/auth/auth.module';
import { BadgeModule } from '../../modules/badge/badge.module';
import { CalendarSettingsRepository } from '../../repositories/calendarSettings.repository';
import { UserProfessionalInfoRepository } from '../../repositories/user-professional.repository';
import { UserRepository } from '../../repositories/user.repository';
import { AuthController } from './controllers/auth.controller';
import { RootController } from './controllers/root.controller';
import { ProAuthService } from './services/pro-auth.service';
import { ProService } from './services/pro.service';

@Module({
  imports: [
    forwardRef(() => AuthModule),
    forwardRef(() => BadgeModule),
    TypeOrmModule.forFeature([
      UserRepository,
      CalendarSettingsRepository,
      UserProfessionalInfoRepository,
    ]),
  ],
  controllers: [RootController, AuthController],
  exports: [],
  providers: [ProService, ProAuthService],
})
export class ProModule {}
