import { Injectable } from '@nestjs/common';

import { ActivityRepository } from '../../repositories/activity.repository';
import { FoodRepository } from '../../repositories/food.repository';
import { UserPreferencesRepository } from '../../repositories/userPreferences.repository';
import { UserWeightRepository } from '../../repositories/userWeight.repository';

@Injectable()
export class RessourceService {
  constructor(
    private readonly _foodRepository: FoodRepository,
    private readonly _activityRepository: ActivityRepository,
    private readonly _userWeightRepository: UserWeightRepository,
    private readonly _userPreferencesRepository: UserPreferencesRepository,
  ) {}

  public async getFood(userId: string) {
    const {
      language,
    }: any = await this._userPreferencesRepository.findOne({
      where: {
        user: <any>userId,
      },
    });

    const foods = await this._foodRepository
      .createQueryBuilder('food')
      .select([
        'food.id',
        'food.textFr',
        'food.textEn',
        'food.energyEuRegulation',
      ])
      .getMany();

    foods.map((food) => {
      food.textFr = language === 'FR' ? food.textFr : food.textEn;
    });

    return foods;
  }

  public async getActivity(userId: string) {
    const userWeight = await this._userWeightRepository
      .createQueryBuilder('userWeight')
      .where('userWeight.user = :userId', { userId })
      .orderBy('created_at', 'DESC')
      .getOne();

    return this._activityRepository
      .createQueryBuilder('activity')
      .select(['activity.id', 'activity.textEn', 'activity.textFr'])
      .addSelect(
        `((activity.met * 3.5 * ${userWeight.weight}) / 200) * 60`,
        'caloriesPerHour',
      )
      .getRawMany();
  }
}
