export class FbProfileDto {
  email: string;
  firstName: string;
  lastName: string;
  avatar: string;
  id: string;
  constructor(
    email: string,
    firstName: string,
    lastName: string,
    avatar: string,
    id: string,
  ) {
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.avatar = avatar;
    this.id = id;
  }
}
