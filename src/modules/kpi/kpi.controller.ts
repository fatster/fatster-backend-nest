import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

import { PayloadSuccessDto } from '../../common/dto/PayloadSuccessDto';
import { AuthGuard } from '../../guards/auth.guard';
import { RolesGuard } from '../../guards/roles.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
import { KpirequestDto } from './dto/kpiRequest.dto';
import { KpiService } from './kpi.service';

@Controller('kpi')
@ApiTags('Kpi')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(AuthUserInterceptor)
@ApiBearerAuth()
export class KpiController {
  constructor(private _kpiService: KpiService) {}

  @Get('/total/users')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async getTotalUsers(
    @Query()
    request: KpirequestDto,
  ): Promise<PayloadSuccessDto> {
    const result = await this._kpiService.totalUsers(
      request.startDate,
      request.endDate,
    );
    return {
      message: 'number of users',
      data: result,
    };
  }

  @Get('/total/posts')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async getTotalPosts(
    @Query()
    request: KpirequestDto,
  ): Promise<PayloadSuccessDto> {
    const result = await this._kpiService.totalPosts(
      request.startDate,
      request.endDate,
    );
    return {
      message: 'number of posts',
      data: result,
    };
  }

  @Get('/total/activity')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async getTotalActivity(
    @Query()
    request: KpirequestDto,
  ): Promise<PayloadSuccessDto> {
    const result = await this._kpiService.totalActivity(
      request.startDate,
      request.endDate,
    );
    return {
      message: 'total activity duration',
      data: result,
    };
  }

  @Get('/total/photos')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async getTotalPhotos(
    @Query()
    request: KpirequestDto,
  ): Promise<PayloadSuccessDto> {
    const result = await this._kpiService.totalPhotos(
      request.startDate,
      request.endDate,
    );
    return {
      message: 'number of photos',
      data: result,
    };
  }

  @Get('/total/messages')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async getTotaMessages(
    @Query()
    request: KpirequestDto,
  ): Promise<PayloadSuccessDto> {
    const result = await this._kpiService.totalMessages(
      request.startDate,
      request.endDate,
    );
    return {
      message: 'number of messages',
      data: result,
    };
  }

  @Get('/total/weightEvolution')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async getTotaWeightEvolution(
    @Query()
    request: KpirequestDto,
  ): Promise<PayloadSuccessDto> {
    const result = await this._kpiService.totalWeight(
      request.startDate,
      request.endDate,
    );
    return {
      message: 'total weight lost/gained',
      data: result,
    };
  }

  @Get('/activeUsers/total')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async getActiveUsers(
    @Query()
    request: KpirequestDto,
  ): Promise<PayloadSuccessDto> {
    const result = await this._kpiService.activeUsers(
      request.startDate,
      request.endDate,
    );
    return {
      message: 'number of active users',
      data: result,
    };
  }

  @Get('/activeUsers/gender')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async getGenderActiveUsers(
    @Query()
    request: KpirequestDto,
  ): Promise<PayloadSuccessDto> {
    const result = await this._kpiService.genderActiveUsers(
      request.startDate,
      request.endDate,
    );
    return {
      message: 'gender of active users',
      data: result,
    };
  }

  @Get('/activeUsers/age')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async getAgeUsers(
    @Query()
    request: KpirequestDto,
  ): Promise<PayloadSuccessDto> {
    const result = await this._kpiService.ageActiveUsers(
      request.startDate,
      request.endDate,
    );
    return {
      message: 'age of active users',
      data: result,
    };
  }

  @Get('/proportion/activities')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async getActivitiesProportion(
    @Query()
    request: KpirequestDto,
  ): Promise<PayloadSuccessDto> {
    const result = await this._kpiService.activityTypes(
      request.startDate,
      request.endDate,
    );
    return {
      message: 'total activity duration',
      data: result,
    };
  }

  @Get('/proportion/weightEvolution')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async getWeightEvolutionProportion(
    @Query()
    request: KpirequestDto,
  ): Promise<PayloadSuccessDto> {
    const result = await this._kpiService.weightEvolutionProportion(
      request.startDate,
      request.endDate,
    );
    return {
      message: 'lost/gained weight proportion',
      data: result,
    };
  }

  @Get('/proportion/bmi')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async getBmiProportion(
    @Query()
    request: KpirequestDto,
  ): Promise<PayloadSuccessDto> {
    const result = await this._kpiService.bmiProportion(
      request.startDate,
      request.endDate,
    );
    return {
      message: 'BMI proportion',
      data: result,
    };
  }

  @Get('/average/activity')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async getAverageActivity(
    @Query()
    request: KpirequestDto,
  ): Promise<PayloadSuccessDto> {
    const result = await this._kpiService.averageActivity(
      request.startDate,
      request.endDate,
    );
    return {
      message: 'average activity duration per active user',
      data: result,
    };
  }

  @Get('/average/weight')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async getAverageWeight(
    @Query()
    request: KpirequestDto,
  ): Promise<PayloadSuccessDto> {
    const result = await this._kpiService.averageWeight(
      request.startDate,
      request.endDate,
    );
    return {
      message: 'average user weight',
      data: result,
    };
  }

  @Get('/nextFeatures/stats')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async getNextFeaturesStats(): Promise<PayloadSuccessDto> {
    const result = await this._kpiService.nextFeaturesStats();
    return {
      message: 'Next features stats',
      data: result,
    };
  }
}
