import { INestApplication } from '@nestjs/common';
import {
  DocumentBuilder,
  SwaggerDocumentOptions,
  SwaggerModule,
} from '@nestjs/swagger';

import { ConfigService } from './shared/services/config.service';
import { SharedModule } from './shared/shared.module';

export function setupSwagger(app: INestApplication) {
  const configService = app.select(SharedModule).get(ConfigService);
  const option: SwaggerDocumentOptions = {
    ignoreGlobalPrefix: false,
  };
  const config = new DocumentBuilder()
    .setTitle('API')
    .setVersion('0.0.1')
    .addBearerAuth();

  if (configService.get('NODE_ENV') !== 'development') {
    config.addServer(`/${process.env.NODE_ENV}`);
    option.ignoreGlobalPrefix = true;
  }

  const document = SwaggerModule.createDocument(
    app,
    config.build(),
    option,
  );

  SwaggerModule.setup('documentation', app, document);
}
