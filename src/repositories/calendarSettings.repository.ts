import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { CalendarSettingsEntity } from '../entities/calendarSettings.entity';

@EntityRepository(CalendarSettingsEntity)
export class CalendarSettingsRepository extends Repository<
  CalendarSettingsEntity
> {}
