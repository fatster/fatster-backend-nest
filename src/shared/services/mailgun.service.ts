import { Injectable } from '@nestjs/common';
import * as mailgun from 'mailgun-js';

import { ConfigService } from './config.service';

@Injectable()
export class MailGunService {
  private readonly _mg: mailgun.Mailgun;
  constructor(public configService: ConfigService) {
    this._mg = mailgun({
      apiKey: this.configService.get('API_KEY'),
      domain: this.configService.get('DOMAIN'),
      host: this.configService.get('HOST'),
    });
  }

  async addMailToMailingList(
    members: mailgun.lists.MemberAddMultipleData,
  ) {
    return this._mg
      .lists('fatster@mg.fatster.app')
      .members()
      .add(members);
  }

  async sendEmail(data) {
    return this._mg.messages().send(data);
  }
}
