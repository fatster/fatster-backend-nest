import './boilerplate.polyfill';

import {
  MiddlewareConsumer,
  Module,
  NestModule,
} from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { contextMiddleware } from './middlewares';
import { AccountModule } from './modules/account/account.module';
import { AuthModule } from './modules/auth/auth.module';
import { BadgeModule } from './modules/badge/badge.module';
import { CalendarModule } from './modules/calendar/calendar.module';
import { CommonModule } from './modules/common/common.module';
import { FeedModule } from './modules/feed/feed.module';
import { FoodModule } from './modules/food/food.module';
import { FriendModule } from './modules/friend/friend.module';
import { HashtagModule } from './modules/hashtag/hashtag.module';
import { MessengerModule } from './modules/messenger/messenger.module';
import { NavigationModule } from './modules/navigation/navigation.module';
import { NotificationModule } from './modules/notification/notification.module';
import { ProModule } from './modules/pro/pro.module';
import { RessourceModule } from './modules/ressource/ressource.module';
import { SearchModule } from './modules/search/search.module';
import { TranslationModule } from './modules/translation/translation.module';
import { UserModule } from './modules/user/user.module';
import { ConfigService } from './shared/services/config.service';
import { SharedModule } from './shared/shared.module';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      useFactory: (configService: ConfigService) =>
        configService.typeOrmConfig,
      inject: [ConfigService],
    }),
    SharedModule,
    CommonModule,
    NotificationModule,
    RessourceModule,
    AuthModule,
    AccountModule,
    UserModule,
    FriendModule,
    SearchModule,
    FeedModule,
    NavigationModule,
    MessengerModule,
    TranslationModule,
    HashtagModule,
    BadgeModule,
    FoodModule,
    ProModule,
    CalendarModule,
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): MiddlewareConsumer | void {
    consumer.apply(contextMiddleware).forRoutes('*');
  }
}
