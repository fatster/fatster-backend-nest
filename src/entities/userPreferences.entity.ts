import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { UserPreferencesDto } from '../dto/userPreferences.dto';
import { Language } from '../shared/services/translation.service';
import { UserEntity } from './user.entity';

@Entity({ name: 'user_preferences' })
export class UserPreferencesEntity extends AbstractEntity<
  UserPreferencesDto
> {
  // MY INFORMATION
  @Column({ default: true })
  showStatistics: boolean;

  @Column({ default: true })
  showFeed: boolean;

  @Column({ default: true })
  showPostsInPublicFeed: boolean;

  @Column({ default: true })
  showPostsAppLanguageOnly: boolean;

  @Column({ default: true })
  showProfile: boolean;

  @Column({ default: true })
  showPseudoInSearch: boolean;

  @Column({ nullable: true })
  language?: Language;

  // PUSH NOTIFICATION
  @Column({ default: true })
  pushNotificationLikes: boolean;

  @Column({ default: true })
  pushNotificationComments: boolean;

  @Column({ default: true })
  pushNotificationFriendRequest: boolean;

  @Column({ default: true })
  pushNotificationNewFriend: boolean;

  @Column({ default: true })
  pushNotificationAdminMessages: boolean;

  @Column({ default: true })
  pushNotificationMentionMessages: boolean;

  // EMAILS
  @Column({ default: true })
  newsLetter: boolean;

  @Column({ default: true })
  adminEmails: boolean;

  // USER
  @OneToOne(() => UserEntity, (user) => user.userPreferences, {
    nullable: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  user: UserEntity;

  dtoClass = UserPreferencesDto;
}
