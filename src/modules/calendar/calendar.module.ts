import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthModule } from '../../modules/auth/auth.module';
import { UserRepository } from '../../repositories/user.repository';
import { CalendarController } from './calendar.controller';
import { CalendarRepository } from './calendar.repository';
import { CalendarService } from './calendar.service';

@Module({
  imports: [
    forwardRef(() => AuthModule),
    TypeOrmModule.forFeature([UserRepository, CalendarRepository]),
  ],
  controllers: [CalendarController],
  exports: [],
  providers: [CalendarService],
})
export class CalendarModule {}
