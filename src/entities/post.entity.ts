import { IsIn } from 'class-validator';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { PostStatusEnum, PostTypeEnum } from '../common/enum/enum';
import { PostDto } from '../dto/post.dto';
import { ArticleEntity } from './article.entity';
import { BeforeAfterEntity } from './beforeAfter.entity';
import { CommentEntity } from './comment.entity';
import { LikeEntity } from './like.entity';
import { NotificationEntity } from './notification.entity';
import { PhotoEntity } from './photo.entity';
import { ReportEntity } from './report.entity';
import { UserEntity } from './user.entity';
import { UserActivityEntity } from './userActivity.entity';
import { UserNutritionEntity } from './userNutrition.entity';
import { UserWeightEntity } from './userWeight.entity';
import { VideoEntity } from './video.entity';

@Entity({ name: 'post' })
export class PostEntity extends AbstractEntity<PostDto> {
  @Column({ nullable: true })
  text: string;

  @Column({ nullable: true })
  imageId: string;

  @OneToMany(() => LikeEntity, (like) => like.post, {
    onDelete: 'CASCADE',
  })
  likes: LikeEntity[];

  @OneToMany(() => CommentEntity, (comment) => comment.post)
  comments: CommentEntity[];

  @ManyToOne(() => UserEntity, (user) => user.posts, {
    onDelete: 'CASCADE',
  })
  user: UserEntity;

  @OneToOne(
    () => UserActivityEntity,
    (userActivity) => userActivity.post,
  )
  userActivity: UserActivityEntity;

  @OneToOne(
    () => UserNutritionEntity,
    (userNutrition) => userNutrition.post,
  )
  userNutrition: UserNutritionEntity;

  @OneToOne(() => UserWeightEntity, (userWeight) => userWeight.post)
  userWeight: UserWeightEntity;

  @OneToOne(
    () => BeforeAfterEntity,
    (beforeAfter) => beforeAfter.post,
  )
  beforeAfter: BeforeAfterEntity;

  @ManyToOne(() => ArticleEntity, (article) => article.post)
  article: ArticleEntity;

  @OneToMany(() => PhotoEntity, (photo) => photo.post)
  photos: PhotoEntity[];

  @OneToMany(() => PhotoEntity, (video) => video.post)
  videos: VideoEntity[];

  @OneToMany(() => ReportEntity, (report) => report.post)
  reports?: ReportEntity[];

  @OneToMany(
    () => NotificationEntity,
    (notification) => notification.post,
  )
  notifications?: NotificationEntity[];

  @ManyToMany(() => UserEntity)
  @JoinTable()
  tags?: UserEntity[];

  @Column({
    type: 'enum',
    enum: PostTypeEnum,
  })
  @IsIn(['activity', 'food', 'weight', 'simple', 'shared_article'])
  type?: PostTypeEnum;

  @Column({
    type: 'enum',
    enum: PostStatusEnum,
    default: PostStatusEnum.PUBLIC,
    nullable: true,
  })
  status?: PostStatusEnum;

  @Column({
    default: 0,
  })
  likesCount?: number;

  @Column({ type: 'text', array: true, nullable: true })
  mentionedUsersPseudo?: string[];

  @Column({ type: 'text', array: true, nullable: true })
  hashtags: string[];

  @Column({
    default: 0,
  })
  commentsCount?: number;

  dtoClass = PostDto;
}
