import { Entity, OneToMany } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { MessengerRoomDto } from '../dto/messengerRoom.dto';
import { FriendEntity } from './friend.entity';
import { MessengerMessageEntity } from './messengerMessage.entity';

@Entity({ name: 'messenger_room' })
export class MessengerRoomEntity extends AbstractEntity<
  MessengerRoomDto
> {
  @OneToMany(() => FriendEntity, (friend) => friend.messengerRoom)
  friend: FriendEntity[];

  @OneToMany(
    () => MessengerMessageEntity,
    (message) => message.messengerRoom,
  )
  messages: MessengerMessageEntity[];

  dtoClass = MessengerRoomDto;
}
