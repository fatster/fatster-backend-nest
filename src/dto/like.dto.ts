import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { LikeTypeEnum } from '../common/enum/enum';
import { isDto } from '../common/utils/entities';
import { LikeEntity } from '../entities/like.entity';
import { CommentDto } from './comment.dto';
import { PostDto } from './post.dto';
import { UserDto } from './user.dto';

export class LikeDto extends AbstractDto {
  @ApiPropertyOptional()
  user: UserDto;

  @ApiPropertyOptional()
  type: LikeTypeEnum;

  @ApiPropertyOptional()
  post: PostDto;

  @ApiPropertyOptional()
  comment: CommentDto;

  constructor(like: LikeEntity) {
    super(like);
    this.comment = isDto(like.comment)
      ? like.comment.toDto()
      : <any>like.comment;
    this.post = isDto(like.post) ? like.post.toDto() : <any>like.post;
    this.user = isDto(like.user) ? like.user.toDto() : <any>like.user;
  }
}
