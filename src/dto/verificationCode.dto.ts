import { ApiProperty } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { VerificationCodeEntity } from '../entities/verificationCode.entity';

export class VerificationCodeDto extends AbstractDto {
  @ApiProperty()
  readonly userId: string;

  @ApiProperty()
  readonly verificationCode: number;

  @ApiProperty()
  readonly isValid: boolean;

  constructor(verificationCode: VerificationCodeEntity) {
    super(verificationCode);
    this.verificationCode = verificationCode.verificationCode;
  }
}
