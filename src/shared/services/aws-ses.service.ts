import { Injectable } from '@nestjs/common';
import * as AWS from 'aws-sdk';

import { ConfigService } from './config.service';

@Injectable()
export class AwsSesService {
  private readonly _ses: AWS.SES;
  constructor(public configService: ConfigService) {
    const awsSesConfig = configService.awsSesConfig;
    const options: AWS.SES.Types.ClientConfiguration = {
      region: awsSesConfig.region,
      apiVersion: '2020-12-01',
    };
    if (awsSesConfig.accessKeyId && awsSesConfig.secretAccessKey) {
      options.credentials = awsSesConfig;
    }
    this._ses = new AWS.SES(options);
  }

  async sendEmail(params: AWS.SES.SendTemplatedEmailRequest) {
    return this._ses.sendTemplatedEmail(params).promise();
  }
}
