'use strict';

import { ApiProperty } from '@nestjs/swagger';

import { UserDto } from '../../../dto/user.dto';
import { TokenPayloadDto } from './tokenPayload.dto';

export class LoginPayloadDto {
  @ApiProperty({ type: UserDto })
  user: UserDto;
  @ApiProperty({ type: TokenPayloadDto })
  token: TokenPayloadDto;

  constructor(user: UserDto, token: TokenPayloadDto) {
    this.user = user;
    this.token = token;
  }
}
