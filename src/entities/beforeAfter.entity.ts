import {
  AfterLoad,
  Column,
  Entity,
  JoinColumn,
  OneToOne,
} from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { BeforeAfterDto } from '../dto/beforeAfter.dto';
import { PostEntity } from './post.entity';

@Entity({ name: 'before_after' })
export class BeforeAfterEntity extends AbstractEntity<
  BeforeAfterDto
> {
  @Column({ type: 'float', nullable: true })
  weightBefore: number;

  @Column({ type: 'float', nullable: true })
  weightAfter: number;

  @OneToOne(() => PostEntity, (post) => post.userWeight, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  post: PostEntity;

  weightLost: number;

  @AfterLoad()
  prefixWithUrl() {
    if (this.weightBefore && this.weightAfter) {
      this.weightLost = this.weightBefore - this.weightAfter;
    }
  }

  dtoClass = BeforeAfterDto;
}
