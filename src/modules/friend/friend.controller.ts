import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';

import { PayloadSuccessDto } from '../../common/dto/PayloadSuccessDto';
import { FriendRequestEnum } from '../../common/enum/enum';
import { AuthUser } from '../../decorators/auth-user.decorator';
import { UserEntity } from '../../entities/user.entity';
import { AuthGuard } from '../../guards/auth.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
import { ProcessFriendDto } from './dto/processFriend.dto';
import { SendFriendRequestDto } from './dto/sendFriend.dto';
import { FriendService } from './friend.service';

@Controller('friend')
@ApiTags('Friend')
export class FriendController {
  constructor(private _friendService: FriendService) {}
  @Get('/')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'friend list',
  })
  async getAllFriend(
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const friendList = await this._friendService.getFriendList(
      user.id,
    );

    return {
      message: 'friend list',
      data: friendList,
    };
  }

  @Post('/request/')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'friend request sent',
  })
  async sendFriendRequest(
    @Body() sendFriendRequestDto: SendFriendRequestDto,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const friendRequest = await this._friendService.sendFriendRequest(
      user.id,
      sendFriendRequestDto.userId,
    );
    return { data: friendRequest, message: 'friend request sent' };
  }

  @Put('/request/:friendRequestId/')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiParam({ name: 'friendRequestId', type: String })
  @ApiOkResponse({
    description: 'friend request accepted/refused',
  })
  async processFriendRequest(
    @Param('friendRequestId') friendRequestId,
    @Body() processFriendRequestDto: ProcessFriendDto,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    processFriendRequestDto.userId = user.id;
    processFriendRequestDto.friendRequestId = friendRequestId;
    await this._friendService.processFriendRequest(
      processFriendRequestDto,
    );
    return { message: 'friend request accepted/refused' };
  }

  @Get('/request/received')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'pending request received list',
  })
  async getPendingRequest(
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const pendingRequest = await this._friendService.getFriendRequests(
      user.id,
      FriendRequestEnum.RECEIVED,
    );
    return {
      message: 'pending request received list',
      data: pendingRequest,
    };
  }

  @Get('/request/sent')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'pending request sent list',
  })
  async getFriendRequest(
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const friendRequest = await this._friendService.getFriendRequests(
      user.id,
      FriendRequestEnum.SENT,
    );
    return {
      message: 'pending request sent list',
      data: friendRequest,
    };
  }

  @Delete('/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiParam({ name: 'id', type: String })
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'friend has been removed',
  })
  async deleteFriend(
    @AuthUser() user: UserEntity,
    @Param('id') friendUserId,
  ): Promise<PayloadSuccessDto> {
    await this._friendService.removeFriend(user.id, friendUserId);
    return { message: 'friend has been deleted' };
  }
}
