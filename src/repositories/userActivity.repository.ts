import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { UserActivityEntity } from '../entities/userActivity.entity';

@EntityRepository(UserActivityEntity)
export class UserActivityRepository extends Repository<
  UserActivityEntity
> {
  public async getDurationActivityByUser(id: string, from?) {
    const query = this.createQueryBuilder('activity')
      .select('SUM(activity.duration)', 'sum')
      .where('activity.user = :id', { id });
    if (from) {
      query.andWhere('activity.createdAt >= :from', {
        from,
      });
    }
    return query.getRawOne();
  }
}
