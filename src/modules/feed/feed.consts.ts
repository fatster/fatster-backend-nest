import { sample } from 'lodash';

const ACTIVITY_DEFAULT_IMAGES = [
  // 'ballon-exercie-therapeutique.jpg',
  // 'boxe.png',
  // 'cross-fit.png',
  // 'danse-general.png',
  // 'entrainement-en-circuit.png',
  // 'gymnastique-general.png',
  // 'gymnastique-physique.png',
  // 'Halterophilie-general.png',
  // 'jogging-centre-ville.jpg',
  // 'jogging-general.png',
  // 'musculation-salle.jpg',
  // 'natation-brasse.png',
  // 'natation-crawl.jpg',
  // 'promenade-parc.jpg',
  // 'randonnee-en-circuit.jpg',
  // 'renforcement-musculaire.jpg',
  // 'saut-a-la-corde.png',
  // 'seance-yoga.jpg',
  // 'tennis-general.png',
  // 'velo-interieur.jpg',
  // 'velo-ville.jpg',
  'basket-metre.png',
  'equipement-pour-le-sport.png',
  'objets-de-sport.png',
  'sac-de-sport.png',
  'serviette-de-sport.png',
  'Sport-activitées.png',
  'sport-chaussures.png',
  'sport-poids.png',
  'Sport-product.png',
  'Tapis-de-sport.png',
];

const ARTICLE_DEFAULT_IMAGES = [
  'page-article.png',
  'stylo-a-cote-de-carnet.png',
  'stylo-dans-une-main.png',
  'stylo-noir-sur-carnet-blanc.png',
  'stylo-sur-carnet.png',
];

const NUTRITION_DEFAULT_IMAGES = [
  // 'assortiment-fruits.png',
  // 'assaisonnement.png',
  // 'pates.png',
  // 'petit-dejeuner.png',
  // 'plat-salade.png',
  'assiette-blanche-et-serviette.png',
  'assiette-bon-app.png',
  'carnet-recette.png',
  'ustensiles-cuisine.png',
  'ustensiles.png',
];

const WEIGHT_DEFAULT_IMAGES = [
  // 'metre-sur-balance.png',
  // 'metre-enroule.png',
  // 'metre-sur-taille.png',
  // 'perte-de-poids.png',
  // 'prise-de-poids.png',
  'balance-et-poids.png',
  'fat-burning.png',
  'metre-couverts.png',
  'metre-fruits-poids.png',
  'tape-measure-2074026_1920.jpg',
];

export enum DefaultImages {
  ACTIVITY = 'ACTIVITY',
  ARTICLE = 'ARTICLE',
  NUTRITION = 'NUTRITION',
  WEIGHT = 'WEIGHT',
}

function getUrl(imgName: string, type: DefaultImages): string {
  return `posts_default_images/${type}/${imgName}`;
}

export function getDefaultImageUrlByType(
  type: DefaultImages,
): string {
  let url: string;
  switch (type) {
    case DefaultImages.ACTIVITY:
      url = sample(ACTIVITY_DEFAULT_IMAGES);
      break;
    case DefaultImages.ARTICLE:
      url = sample(ARTICLE_DEFAULT_IMAGES);
      break;
    case DefaultImages.NUTRITION:
      url = sample(NUTRITION_DEFAULT_IMAGES);
      break;
    case DefaultImages.WEIGHT:
      url = sample(WEIGHT_DEFAULT_IMAGES);
  }

  return getUrl(url, type);
}
