import { Injectable } from '@nestjs/common';

import { HashtagEntityType } from '../../common/enum/enum';
import { HashtagRepository } from '../../repositories/hashtag.repository';
import { PostRepository } from '../../repositories/post.repository';
import { UserRepository } from '../../repositories/user.repository';
import { GetHashtagPostsDto } from './dto/getHashtagPosts.dto';

@Injectable()
export class HashtagService {
  constructor(
    private readonly _postRepository: PostRepository,
    private readonly _hashtagRepository: HashtagRepository,
    private readonly _userRepository: UserRepository,
  ) {}

  public async getAllHashtags() {
    return this._hashtagRepository.find();
  }

  public async getPostsByHashtag(
    hours: number,
    hashtagText: string,
    { order, take, skip, user }: GetHashtagPostsDto,
  ) {
    const whereCondition = hours
      ? `hashtags @> (ARRAY[:hashtagText]) AND user.createdAt + INTERVAL '${hours} HOURS' >= NOW()`
      : `hashtags @> (ARRAY[:hashtagText])`;

    const userPref = await this._userRepository.findOne({
      where: {
        id: user.id,
      },
      relations: ['userPreferences'],
    });

    const lang = userPref.userPreferences.showPostsAppLanguageOnly
      ? userPref.userPreferences.language
      : null;

    return this._postRepository.getPostsWithRelations(user, lang, {
      order,
      take,
      skip,
      rank: false,
      where: {
        where: whereCondition,
        parameters: {
          hashtagText,
        },
      },
      addFriendRequests: true,
    });
  }

  public async getHashtag(
    text: string,
  ): Promise<{
    entity: HashtagEntityType;
    keyName: string;
    id: string;
  }> {
    const hashtag = await this._hashtagRepository.findOne(
      { keyName: `#${text}` },
      {
        select: ['entity', 'keyName', 'id'],
      },
    );

    if (hashtag) {
      return hashtag;
    }
    return null;
  }
}
