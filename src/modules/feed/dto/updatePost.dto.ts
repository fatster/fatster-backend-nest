'use strict';

import {
  ApiHideProperty,
  ApiProperty,
  ApiPropertyOptional,
} from '@nestjs/swagger';
import {
  IsArray,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

export class UpdatePostDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly text: string;

  @IsArray()
  @ApiPropertyOptional()
  @IsOptional()
  readonly friendsToTag: string[];

  @IsArray()
  @ApiProperty()
  readonly photos: string[];

  @ApiHideProperty()
  userId: string;
}
