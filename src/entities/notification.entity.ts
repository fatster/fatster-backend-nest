import { Column, Entity, ManyToOne } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { NotificationEnum } from '../common/enum/enum';
import { NotificationDto } from '../dto/notification.dto';
import { CommentEntity } from './comment.entity';
import { FriendRequestEntity } from './friendRequest.entity';
import { LikeEntity } from './like.entity';
import { PostEntity } from './post.entity';
import { UserEntity } from './user.entity';

@Entity({ name: 'notification' })
export class NotificationEntity extends AbstractEntity<
  NotificationDto
> {
  @Column({
    default: false,
  })
  seen: boolean;

  @Column({
    type: 'enum',
    enum: NotificationEnum,
  })
  type: NotificationEnum;

  @ManyToOne(() => UserEntity, (user) => user.notifications, {
    onDelete: 'CASCADE',
  })
  user: UserEntity;

  @ManyToOne(() => PostEntity, (post) => post.id, {
    onDelete: 'CASCADE',
  })
  post?: PostEntity;

  @ManyToOne(() => CommentEntity, (comment) => comment.id, {
    onDelete: 'CASCADE',
  })
  comment?: CommentEntity;

  @ManyToOne(() => LikeEntity, (like) => like.id, {
    onDelete: 'CASCADE',
  })
  like?: LikeEntity;

  @ManyToOne(
    () => FriendRequestEntity,
    (friendRequest) => friendRequest.id,
    {
      onDelete: 'CASCADE',
    },
  )
  friendRequest?: FriendRequestEntity;

  @ManyToOne(() => UserEntity, (user) => user.id, {
    onDelete: 'CASCADE',
  })
  from?: UserEntity;

  dtoClass = NotificationDto;
}
