import { Injectable } from '@nestjs/common';
import { omit } from 'lodash';

import { CalendarSettingsDto } from '../../../dto/calendarSettings.dto';
import { ProfessionalInfoDto } from '../../../dto/professionalInfo.dto';
import { UserDto } from '../../../dto/user.dto';
import { UserEntity } from '../../../entities/user.entity';
import { CalendarSettingsRepository } from '../../../repositories/calendarSettings.repository';
import { UserProfessionalInfoRepository } from '../../../repositories/user-professional.repository';
import { UserRepository } from '../../../repositories/user.repository';

type ProDetails = Partial<
  Pick<
    UserDto,
    'email' | 'phone' | 'surname' | 'name' | 'authMethod'
  > & { password: string }
>;

interface ISaveProProps {
  id?: string;
  proDetails?: ProDetails;
  infoDto?: Partial<ProfessionalInfoDto>;
  calendarSettingsDto?: Partial<CalendarSettingsDto>;
}

@Injectable()
export class ProService {
  constructor(
    private readonly _userRepository: UserRepository,
    private readonly _calendarSettingsRepository: CalendarSettingsRepository,
    private readonly _professionalInfoRepository: UserProfessionalInfoRepository,
  ) {}

  public async findProByIdWithRelations(id: string) {
    const pro = await this._userRepository
      .createQueryBuilder('user')
      .where({ id })
      .leftJoinAndSelect('user.professionalInfo', 'info')
      .leftJoinAndSelect('user.calendarSettings', 'calendarSettings')
      .getOne();

    return pro.toDto();
  }

  public async saveProByIdWithRelations({
    id,
    calendarSettingsDto,
    proDetails,
    infoDto,
  }: ISaveProProps) {
    const user: UserEntity = await this._saveCreateProDetails(
      id,
      proDetails,
    );

    const [calendarSettings, professionalInfo] = await Promise.all([
      this._saveCreateCalendarSettings(id, calendarSettingsDto),
      this._saveCreateProInfo(id, infoDto),
    ]);
    return this._userRepository.create({
      ...user,
      professionalInfo,
      calendarSettings,
    });
  }

  private async _saveCreateCalendarSettings(
    id: string,
    settingsDto?: Partial<CalendarSettingsDto>,
  ) {
    if (!settingsDto) {
      return null;
    }

    const settings = await this._calendarSettingsRepository.findOne({
      user: <any>id,
    });
    if (settings) {
      await this._calendarSettingsRepository.update(
        { user: <any>id },
        omit(settingsDto, ['user']),
      );
      return this._calendarSettingsRepository
        .create({ ...settingsDto, user: <any>id })
        .toDto();
    }
    return this._calendarSettingsRepository.save({
      ...settingsDto,
      user: <any>id,
    });
  }

  private async _saveCreateProInfo(
    id: string,
    infoDto?: Partial<ProfessionalInfoDto>,
  ) {
    if (!infoDto) {
      return null;
    }

    const info = await this._professionalInfoRepository.findOne({
      user: <any>id,
    });
    if (info) {
      await this._professionalInfoRepository.update(
        { user: <any>id },
        omit(infoDto, ['user']),
      );
      return this._professionalInfoRepository
        .create({ ...infoDto, user: <any>id })
        .toDto();
    }
    return this._professionalInfoRepository.save({
      ...infoDto,
      user: <any>id,
    });
  }

  private async _saveCreateProDetails(
    id?: string,
    proDto?: ProDetails,
  ) {
    if (proDto) {
      return this._userRepository.save({
        ...proDto,
        ...(id ? { id } : {}),
      });
    }
    return this.findProByIdWithRelations(id);
  }
}
