/* eslint-disable quote-props */
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { groupBy } from 'lodash';
import * as moment from 'moment';
import * as format from 'pg-format';
import { FindConditions, SelectQueryBuilder } from 'typeorm';

import {
  AuthMethodEnum,
  HealthyTypeEnum,
  NutritionFoodType,
  TimeOfTheDayEnum,
} from '../../common/enum/enum';
import { minutesToHours } from '../../common/utils/utils';
import { getCaloriesFromFood } from '../../dto/userNutritionFood.dto';
import { UserEntity } from '../../entities/user.entity';
import { UserNutritionEntity } from '../../entities/userNutrition.entity';
import { AccountService } from '../../modules/account/account.service';
import { AppleProfileDto } from '../../modules/auth/dto/appleProfile.dto';
import { FriendService } from '../../modules/friend/friend.service';
import { UtilsService } from '../../providers/utils.service';
import { ActivityRepository } from '../../repositories/activity.repository';
import { BlockRepository } from '../../repositories/block.repository';
import { FriendRepository } from '../../repositories/friend.repository';
import { FriendRequestRepository } from '../../repositories/friendRequest.repository';
import { PostRepository } from '../../repositories/post.repository';
import { ReportRepository } from '../../repositories/report.repository';
import { UserRepository } from '../../repositories/user.repository';
import { UserActivityRepository } from '../../repositories/userActivity.repository';
import { UserNutritionRepository } from '../../repositories/userNutrition.repository';
import { UserNutritionFoodRepository } from '../../repositories/userNutritionFood.repository';
import { UserWeightRepository } from '../../repositories/userWeight.repository';
import { getCalories } from '../../utils/calories';
import {
  formatDate,
  getWeek,
  startOfDay,
  subtractYearsFromNow,
} from '../../utils/date';
import { FbProfileDto } from '../auth/dto/fbProfile.dto';
import { UserRegisterDto } from '../auth/dto/userRegister.dto';
import { AddReportDto } from './dto/addReport.dto';
import { BlockUserDto } from './dto/blockUser.dto';
import { UnBlockUserDto } from './dto/unblockUser.dto';

/* eslint-disable camelcase */
type UserHistory = {
  day: Date;
  nutr: {
    id: string;
    post_id: string;
    user_id: string;
    created_at: Date;
    updated_at: Date;
    healthy_type: HealthyTypeEnum;
    time_of_the_day: TimeOfTheDayEnum;
    nutrfood: {
      food: {
        id: string;
        text_en: string;
        text_fr: string;
        energy_eu_regulation: number;
        energy_n_jones_factor_with_fibres: number;
      };
      nutr: {
        id: string;
        grams: number;
        food_id: string;
        portion: number;
        calories: number;
        created_at: Date;
        updated_at: Date;
        portion_type: NutritionFoodType;
        user_nutrition_id: string;
      };
    }[];
  }[];
}[];

@Injectable()
export class UserService {
  constructor(
    private readonly _userRepository: UserRepository,
    private readonly _postRepository: PostRepository,
    private readonly _userActivityRepository: UserActivityRepository,
    private readonly _userWeightRepository: UserWeightRepository,
    private readonly _friendRequestRepository: FriendRequestRepository,
    private readonly _accountServices: AccountService,
    private readonly _userNutritionRepository: UserNutritionRepository,
    private readonly _userNutritionFoodRepository: UserNutritionFoodRepository,
    private readonly _blockRepository: BlockRepository,
    private readonly _friendRepository: FriendRepository,
    private readonly _activityRepository: ActivityRepository,
    private readonly _friendService: FriendService,
    private readonly _reportRepository: ReportRepository,
  ) {}

  public async findOne(conditions: FindConditions<UserEntity>) {
    return this._userRepository.findOne(conditions);
  }

  async findOneWithRelations(
    where: Parameters<SelectQueryBuilder<UserEntity>['where']>[0],
    parameters?: Parameters<
      SelectQueryBuilder<UserEntity>['where']
    >[1],
  ): Promise<UserEntity> {
    return this._userRepository
      .createQueryBuilder('user')
      .distinctOn(['user.id'])
      .select([
        'user.id',
        'user.pseudo',
        'weight.weight',
        'user.birthDate',
        'user.photoId',
        'user.gender',
        'user.targetWeight',
        'user.targetLooseWeight',
        'user.originalWeight',
        'user.email',
        'user.height',
      ])
      .leftJoin('user.userWeights', 'weight')
      .leftJoinAndSelect('user.badges', 'badges')
      .leftJoinAndSelect('user.userPreferences', 'userPreferences')
      .where(where, parameters)
      .orderBy({
        'user.id': 'DESC',
        'weight.created_at': 'DESC',
      })
      .getOne();
  }

  async findOneById(id: string, me: UserEntity) {
    const user = await this.findOneWithRelations('user.id = :id', {
      id,
    });

    const friendRequest = await this._friendRequestRepository
      .createQueryBuilder('friendRequest')
      .where(
        '(friendRequest.from = :fromId AND friendRequest.to = :toId) OR (friendRequest.from = :toId AND friendRequest.to = :fromId)',
        { fromId: me.id, toId: id },
      )
      .loadAllRelationIds()
      .getOne();

    if (friendRequest) {
      return [
        user,
        friendRequest.id,
        friendRequest.status,
        friendRequest.from === <any>me.id,
      ];
    }
    return [user, null, 'not added'];
  }

  async findOrCreateFromAppleId({ email, appleId }: AppleProfileDto) {
    let user: UserEntity;
    if (this._isEmpty(email)) {
      user = await this._userRepository.findOne({
        where: { appleId },
      });
    } else {
      user = await this._userRepository.findOne({
        where: { email, appleId },
      });
      if (!user) {
        user = await this._userRepository.findOne({
          where: { email },
        });
      }
    }

    if (user && user.appleId !== appleId) {
      await this._userRepository.update(user.id, { appleId });
      user = await this._userRepository.findOne({
        where: { appleId },
      });
    }

    if (!user) {
      const newUser = this._userRepository.create({
        appleId,
        email,
        authMethod: AuthMethodEnum.APPLE,
      });
      return this._userRepository.save(newUser);
    }
    return user;
  }

  async findOrCreateFromFacebook(fbProfile: FbProfileDto) {
    let user: UserEntity;
    if (!this._isEmpty(fbProfile.email)) {
      user = await this._userRepository.findOne({
        where: { email: fbProfile.email },
      });
    } else {
      user = await this._userRepository.findOne({
        where: { fbId: fbProfile.id },
      });
    }
    if (!user) {
      const createdUser = this._userRepository.create({
        fbId: fbProfile.id,
        email: fbProfile.email,
        authMethod: AuthMethodEnum.FACEBOOK,
      });
      return this._accountServices.updateProfilePictureFromUrl(
        createdUser,
        fbProfile.avatar,
      );
    }
    return user;
  }

  async createUser(
    userRegisterDto: UserRegisterDto,
  ): Promise<UserEntity> {
    const existingUser = await this._userRepository.findOne({
      where: {
        email: userRegisterDto.email,
      },
    });
    if (existingUser) {
      throw new BadRequestException('email_already_exists');
    }

    userRegisterDto.password = UtilsService.generateHash(
      userRegisterDto.password,
    );

    const user = this._userRepository.create({
      ...userRegisterDto,
      authMethod: AuthMethodEnum.EMAIL,
    });

    return this._userRepository.save(user);
  }

  private async _getRecommandedCalories(user: UserEntity) {
    const userLastWeight = await this._userWeightRepository
      .createQueryBuilder('userWeight')
      .where('userWeight.user = :id', { id: user.id })
      .orderBy('created_at', 'DESC')
      .getOne();

    let today = 0;

    const res = await this._userNutritionRepository.query(
      format(
        `SELECT SUM(user_nutrition_food.portion * food.energy_eu_regulation) AS total FROM user_nutrition
INNER JOIN user_nutrition_food ON user_nutrition.id = user_nutrition_food.user_nutrition_id
INNER JOIN food ON user_nutrition_food.food_id = food.id
WHERE user_nutrition.user_id = '${
          user.id
        }' AND user_nutrition.created_at > '${formatDate(
          'YYYY-MM-DD',
          startOfDay(),
        )}'
GROUP BY user_nutrition.user_id`,
      ),
    );

    if (res && res.length > 0) {
      today = res[0].total;
    }

    const { loseCalories, maintainCalories } = getCalories(
      userLastWeight.weight,
      user.height / 100,
      user.age,
      user.gender,
    );

    return {
      today: Math.round(today),
      maintain: maintainCalories,
      loose: loseCalories,
    };
  }

  public async getCalorieByActivity(
    id: string,
    activityId: string,
    duration: number,
  ) {
    const userLastWeightQuery = this._userWeightRepository
      .createQueryBuilder('userWeight')
      .where('userWeight.user = :id', { id })
      .orderBy('created_at', 'DESC')
      .getOne();

    const activityQuery = this._activityRepository
      .createQueryBuilder('activity')
      .where('activity.id = :activityId', { activityId })
      .getOne();

    const [userWeight, activity] = await Promise.all([
      userLastWeightQuery,
      activityQuery,
    ]);

    if (userWeight && activity && activity.met !== null) {
      return Math.round(
        ((activity.met * 3.5 * userWeight.weight) / 200) * duration,
      );
    }

    return null;
  }

  async getCalorie(id: string) {
    const user = await this._userRepository
      .createQueryBuilder('user')
      .select(['user', 'userPreferences.language'])
      .leftJoin('user.userPreferences', 'userPreferences')
      .where('user.id = :id', { id })
      .getOne();

    // eslint-disable-next-line import/namespace
    moment.locale(user.userPreferences.language);

    const calorieRecommanded = await this._getRecommandedCalories(
      user,
    );

    // console.log(calorieRecommanded);

    const res = await this._userNutritionRepository.query(
      format(
        // eslint-disable-next-line max-len
        `SELECT date_trunc('day', user_nutrition_food.created_at)::timestamp::date as "day", TRUNC(SUM(user_nutrition_food.portion * food.energy_eu_regulation)) AS total FROM user_nutrition INNER JOIN user_nutrition_food ON user_nutrition.id = user_nutrition_food.user_nutrition_id INNER JOIN food ON user_nutrition_food.food_id = food.id WHERE user_nutrition.user_id = '${id}' GROUP BY 1 order by "day"`,
      ),
    );

    const from = moment(user.createdAt).startOf('day').toDate();
    const to = moment().add(1, 'day').toDate();
    const currDate = moment(from);
    const lastDate = moment(to).startOf('day');
    const dates: any = {};

    while (
      lastDate.subtract(1, 'days').endOf('days').diff(currDate) >= 0
    ) {
      if (!dates[lastDate.clone().format('MMMM-YYYY')]) {
        dates[lastDate.clone().format('MMMM-YYYY')] = [];
      }
      // console.log(lastDate.clone().format('MMMM-YYYY'));
      const formatedDate = lastDate.clone().format('MMMM-YYYY');
      const weekMonthNumber = this._getWeek(
        moment(lastDate).toDate(),
      );

      if (
        // dates[formatedDate] &&
        !dates[formatedDate][weekMonthNumber] // &&
        // weekMonthNumber
      ) {
        dates[formatedDate][weekMonthNumber] = [];
      }

      dates[formatedDate][weekMonthNumber].unshift({
        date: lastDate.clone().format('YYYY-MM-DD'),
        calorie: this._setCalorie(
          res,
          lastDate.clone().format('YYYY-MM-DD'),
        ),
      });
    }

    return {
      history: dates,
      calorie: calorieRecommanded,
    };
  }

  private _getWeek(date) {
    const monthStart = new Date(date);
    monthStart.setDate(0);
    const offset = ((monthStart.getDay() + 1) % 7) - 1; // -1 is for a week starting on Monday
    return Math.ceil((date.getDate() + offset) / 7);
  }

  private _setCalorie(calorieList, date) {
    let calorie = 0;

    for (const entry of calorieList) {
      if (moment(entry.day).format('YYYY-MM-DD') === date) {
        calorie = entry.total;
        break;
      }
    }

    return calorie;
  }

  public async getDaysHistory(user: UserEntity) {
    const userId = user.id;
    const todayYearAgo = formatDate(
      'YYYY-MM-DD',
      subtractYearsFromNow(1),
    );

    const result: UserHistory = await this._userNutritionRepository.query(
      format(`WITH dateRange AS (
select greatest(min(nutr.created_at)::timestamp, timestamp '${todayYearAgo}')::date as minDate, max(nutr.created_at)::timestamp::date as maxDate
from user_nutrition as nutr where nutr.user_id = '${userId}')
SELECT (t.day::date) as day, array_agg(to_jsonb(nutr)) as nutr
FROM generate_series((select minDate from dateRange),(select maxDate from dateRange),interval  '1 day') AS t(day)
LEFT JOIN (
  select un.*,
		(select (array_agg(json_build_object('nutr', nutrFood, 'food', food))) as nutrs from user_nutrition_food as nutrFood
		left join food on nutrFood.food_id=food.id
		where nutrFood.user_nutrition_id=un.id)
	as nutrFood from user_nutrition as un
	where un.user_id = '${userId}'
) as nutr on nutr.created_at::date = t.day::date
group by t.day::date order by t.day::date asc`),
    );

    const monthGroup = groupBy(result.reverse(), ({ day }) =>
      day.toISOString().slice(0, 7),
    );

    const response = [];
    for (const [date, val] of Object.entries(monthGroup)) {
      const [year, month] = date.split('-');

      const weeksMap = groupBy(val, ({ day }) => getWeek(day));
      const weeks = Object.keys(weeksMap)
        .map(Number)
        .sort((a, b) => (a > b ? -1 : 1))
        .map((n) => {
          const days = weeksMap[n].map(({ day, nutr: daysNutr }) => {
            const calories = daysNutr
              .filter((i) => i && i.nutrfood)
              .reduce((acc, { nutrfood }) => {
                const foodCalories = nutrfood.reduce(
                  (foodCal, { food, nutr }) =>
                    foodCal +
                    getCaloriesFromFood(
                      {
                        portionType: nutr.portion_type,
                        portion: nutr.portion,
                        calories: nutr.calories,
                        grams: nutr.grams,
                      },
                      {
                        energyEuRegulation: food.energy_eu_regulation,
                      },
                    ),
                  0,
                );
                return acc + foodCalories;
              }, 0);
            return {
              calories,
              dayName: day.toLocaleDateString('en-Us', {
                weekday: 'long',
              }),
              date: day.toISOString(),
            };
          });

          const weekCalories = days.reduce(
            (weekCal, { calories }) => weekCal + calories,
            0,
          );
          return {
            days,
            calories: weekCalories,
          };
        });

      response.push({
        weeks,
        date: {
          month,
          year,
        },
      });
    }

    return response;
  }

  public async getTodayReport(user: UserEntity) {
    const start = startOfDay();
    const today = formatDate('YYYY-MM-DD', start);
    const userId = user.id;

    const [userLastWeight, userNutrition] = await Promise.all([
      this._userWeightRepository
        .createQueryBuilder('userWeight')
        .where('userWeight.user = :id', { id: userId })
        .orderBy('created_at', 'DESC')
        .getOne(),
      this._userNutritionRepository
        .createQueryBuilder('nutrition')
        .distinctOn(['nutrition.timeOfTheDay'])
        .where(
          'nutrition.user_id = :userId AND nutrition.created_at::timestamp::date = :today',
          { userId, today },
        )
        .orderBy('nutrition.timeOfTheDay', 'DESC')
        .addOrderBy('nutrition.createdAt', 'DESC')
        .getMany(),
    ]);

    const {
      nutritionByTime,
      todayCal,
    } = await this._getNutritionByTime(userNutrition);

    const { loseCalories, maintainCalories } = getCalories(
      userLastWeight.weight,
      user.height / 100,
      user.age,
      user.gender,
    );

    const calories = {
      today: Math.round(todayCal),
      maintain: maintainCalories,
      loose: loseCalories,
    };

    return {
      calories,
      nutritionByTime,
    };
  }

  private async _getNutritionByTime(
    userNutrition: UserNutritionEntity[],
  ) {
    const defaultMap = Object.fromEntries(
      Object.values(TimeOfTheDayEnum).map((e) => [e, []]),
    );

    if (userNutrition.length) {
      const userNutritionFood = await this._userNutritionFoodRepository
        .createQueryBuilder('nutritionFood')
        .leftJoinAndSelect('nutritionFood.food', 'food')
        .leftJoinAndSelect('nutritionFood.userNutrition', 'nutrition')
        .leftJoinAndSelect('nutrition.post', 'post')
        .where('user_nutrition_id in (:...ids)', {
          ids: userNutrition.map(({ id }) => id),
        })
        .getMany();

      const userNutritionFoodDto = userNutritionFood.toDtos();
      const nutritionByTime = {
        ...defaultMap,
        ...groupBy(
          userNutritionFoodDto,
          (i) => i.userNutrition.timeOfTheDay,
        ),
      };

      const todayCal = userNutritionFoodDto.reduce(
        (acc, { summaryCalories }) => acc + summaryCalories || 0,
        0,
      );

      return {
        nutritionByTime,
        todayCal: Math.round(todayCal * 10) / 10,
      };
    }
    return {
      nutritionByTime: defaultMap,
      todayCal: 0,
    };
  }

  async getStat(id: string) {
    const user = await this._userRepository
      .createQueryBuilder('user')
      .where('user.id = :id', { id })
      .leftJoinAndSelect('user.friends', 'friends')
      .getOne();

    if (!user) {
      throw new NotFoundException("user doesn't exist");
    }

    const durationActivityQuery = this._userActivityRepository.getDurationActivityByUser(
      id,
    );
    const durationMonthActivityQuery = this._userActivityRepository.getDurationActivityByUser(
      id,
      moment().subtract(1, 'months'),
    );

    const nbUserPostLikedQuery = this._postRepository.countLikedReceived(
      id,
    );
    const nbUserPostLikedMonthQuery = this._postRepository.countLikedReceived(
      id,
    );

    const nbUserHealthyNutritionQuery = this._userNutritionRepository.countHealthyNutrition(
      id,
    );

    const nbUserTotalNutritionQuery = this._userNutritionRepository.countNutrition(
      id,
    );

    const nbUserHealthyMonthlyNutritionQuery = this._userNutritionRepository.countHealthyNutrition(
      id,
      moment().subtract(1, 'months'),
    );

    const nbUserTotalMonthlyNutritionQuery = this._userNutritionRepository.countNutrition(
      id,
      moment().subtract(1, 'months'),
    );

    const userWeightQuery = this._userWeightRepository
      .createQueryBuilder('userWeight')
      .select(['created_at', 'weight'])
      .where('userWeight.user = :id', { id })
      .orderBy('created_at', 'ASC')
      .getRawMany();

    const userLastWeightQuery = this._userWeightRepository
      .createQueryBuilder('userWeight')
      .where('userWeight.user = :id', { id })
      .orderBy('created_at', 'DESC')
      .getOne();

    const userActivityQuery = this._userActivityRepository
      .createQueryBuilder('userActivty')
      .select(['created_at', 'duration'])
      .where('userActivty.user = :id', { id })
      .orderBy('created_at', 'DESC')
      .getRawMany();

    const userNutritionQuery = this._userNutritionRepository
      .createQueryBuilder('userNutrition')
      .where('user_id = :id', { id })
      .getMany();

    const res = await this._userNutritionRepository.query(
      format(
        `SELECT SUM(user_nutrition_food.portion * food.energy_eu_regulation) AS total FROM user_nutrition
INNER JOIN user_nutrition_food ON user_nutrition.id = user_nutrition_food.user_nutrition_id
INNER JOIN food ON user_nutrition_food.food_id = food.id
WHERE user_nutrition.user_id = '${id}' AND user_nutrition.created_at > '${formatDate(
          'YYYY-MM-DD',
          startOfDay(),
        )}'
GROUP BY user_nutrition.user_id`,
      ),
    );

    const resLength = res ? res.length : 0;
    const dayCalories = resLength > 0 ? res[0].total : 0;

    const [
      { likes: nbUserPostLiked },
      { likes: nbUserPostLikedMonth },
      userWeightHistory,
      userActivityHistory,
      { sum: totalActivity },
      { sum: monthActivity },
      userLastWeight,
      numberUserHealthyNutrition,
      numberUserTotalNutrition,
      numberUserHealthyMonthlyNutrition,
      numberUserTotalMonthlyNutrition,
      userNutrition,
    ] = await Promise.all([
      nbUserPostLikedQuery,
      nbUserPostLikedMonthQuery,
      userWeightQuery,
      userActivityQuery,
      durationActivityQuery,
      durationMonthActivityQuery,
      userLastWeightQuery,
      nbUserHealthyNutritionQuery,
      nbUserTotalNutritionQuery,
      nbUserHealthyMonthlyNutritionQuery,
      nbUserTotalMonthlyNutritionQuery,
      userNutritionQuery,
    ]);

    const friendCount = user.friends.length;

    const nutritionHistory = this._calculateHealthyFood(
      moment(user.createdAt).toDate(),
      moment().add(1, 'day').toDate(),
      userNutrition,
    );

    const { loseCalories, maintainCalories } = getCalories(
      userLastWeight.weight,
      user.height / 100,
      user.age,
      user.gender,
    );

    return {
      nutritionHistory,
      userWeightHistory,
      userActivityHistory,
      friendCount,
      dayCalories,
      bmi: Number(
        (
          userLastWeight.weight / Math.pow(user.height / 100, 2)
        ).toFixed(1),
      ),
      activity: {
        all: minutesToHours(Math.max(0, totalActivity)),
        month: minutesToHours(Math.max(0, monthActivity)),
      },
      like: {
        all: nbUserPostLiked,
        month: nbUserPostLikedMonth,
      },
      nutrition: {
        all:
          Math.round(
            (100 * numberUserHealthyNutrition.nb) /
              Math.max(numberUserTotalNutrition.nb, 1),
          ) + '%',
        month:
          Math.round(
            (100 * numberUserHealthyMonthlyNutrition.nb) /
              Math.max(numberUserTotalMonthlyNutrition.nb, 1),
          ) + '%',
      },
      calorie: {
        maintain: maintainCalories,
        loose: loseCalories,
      },
    };
  }

  async blockUser(blockUserDto: BlockUserDto) {
    const block = this._blockRepository.create();

    block.from = <any>blockUserDto.from;
    block.against = <any>blockUserDto.against;

    await this._blockRepository.save(block);

    if (
      await this._friendRepository.areFriends(
        blockUserDto.from,
        blockUserDto.against,
      )
    ) {
      this._friendService.removeFriend(
        blockUserDto.from,
        blockUserDto.against,
      );
    }
  }

  async unBlockUser(unBlockUserDto: UnBlockUserDto) {
    const block = await this._blockRepository.findOneOrFail({
      where: {
        from: <any>unBlockUserDto.from,
        against: <any>unBlockUserDto.against,
      },
    });

    this._blockRepository.remove(block);
  }

  async getBlockedUsers(userId: string) {
    return this._blockRepository.find({
      where: {
        from: <any>userId,
      },
    });
  }

  async addReport(addReportDto: AddReportDto) {
    const report = this._reportRepository.create();
    report.post = <any>addReportDto.postId;
    report.user = <any>addReportDto.userId;
    return this._reportRepository.save(report);
  }

  private _calculateHealthyFood(
    startDate,
    endDate,
    userNutrition: UserNutritionEntity[],
  ) {
    const dates = {};

    const currDate = moment(startDate).startOf('day');
    const lastDate = moment(endDate).startOf('day');

    while (
      lastDate.subtract(1, 'days').endOf('days').diff(currDate) >= 0
    ) {
      if (!dates[lastDate.clone().format('MMMM-YYYY')]) {
        dates[lastDate.clone().format('MMMM-YYYY')] = [];
      }

      dates[lastDate.format('MMMM-YYYY')].unshift({
        date: lastDate.clone().format('YYYY-MM-DD'),
        healthy: this._isHealthy(lastDate, userNutrition),
      });
    }

    return dates;
  }

  private _isHealthy(date, userNutrition: UserNutritionEntity[]) {
    const settings = {
      yes: 100,
      average: 50,
      no: 0,
    };
    const dayNutritions = userNutrition.filter((el) => {
      if (moment(el.createdAt).isSame(date, 'day')) {
        return el;
      }
    });
    if (dayNutritions.length === 0) {
      return 'unknown';
    }
    let score = 0;
    dayNutritions.map((nutrition) => {
      score += settings[nutrition.healthyType];
    });
    const average = score / dayNutritions.length;
    return average < 50 ? 'no' : average < 75 ? 'average' : 'healthy';
  }

  async updateLastConnexion(userId: string) {
    return this._userRepository.update(userId, {
      lastConnection: new Date(),
    });
  }

  /* private _minutesToHours(num) {
    if (num) {
      const hours = Math.floor(num / 60);
      const minutes = ('0' + (num % 60)).slice(-2);
      const formatedMinutes = Number(minutes);
      return formatedMinutes > 0 ? `${hours}h${minutes}` : `${hours}`;
    }
    return '0h';
  }*/
  private _isEmpty(str) {
    return !str || 0 === str.length;
  }
}
