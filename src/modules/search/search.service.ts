import { BadRequestException, Injectable } from '@nestjs/common';
import { In } from 'typeorm';

import { PageMetaDto } from '../../common/dto/PageMetaDto';
import { ResultPageDto } from '../../common/dto/ResultPage.dto';
import { GenderEnum } from '../../common/enum/enum';
import { UserDto } from '../../dto/user.dto';
import { UserEntity } from '../../entities/user.entity';
import { FriendRequestRepository } from '../../repositories/friendRequest.repository';
import { UserRepository } from '../../repositories/user.repository';
import { subtractYearsFromNow } from '../../utils/date';
import { SearchByEmailDto } from './dto/searchByEmail.dto';
import { SearchByPseudoDto } from './dto/searchByPseudo.dto';
import { UsersPageOptionsDto } from './dto/usersPageOptions.dto';

@Injectable()
export class SearchService {
  constructor(
    private readonly _userRepository: UserRepository,
    private readonly _friendRequestRepository: FriendRequestRepository,
  ) {}

  async searchByCriteria(
    userCriteria: UsersPageOptionsDto,
    meId: string,
  ) {
    const {
      minTargetWeightLoose,
      maxTargetWeightLoose,
      language,
      minAge,
      maxAge,
      skip,
      take,
      gender = [GenderEnum.FEMALE, GenderEnum.MALE],
    } = userCriteria;
    const minBirthDate = subtractYearsFromNow(maxAge).toISOString();
    const maxBirthDate = subtractYearsFromNow(minAge).toISOString();
    try {
      const users = await this._userRepository.query(
        `
      SELECT DISTINCT ON (us.id) uw.weight, us.id, us.created_at as createdAt, us.updated_at as updatedAt, us.email, us.fb_id as fbId,
us.apple_id as appleId, us.name, us.surname, us.pseudo, us.original_weight as originalWeight, us.target_loose_weight as targetLooseWeight,
us.target_weight as targetWeight, us.one_signal_player_id as oneSignalPlayerId, us.auth_method as authMethod, us.photo_id as photoId,
us.birth_date as birthDate, us.profile_completed as profileCompleted, us.last_connection as lastConnection, us.from_firebase as fromFirebase,
up.language as language,
date_part('year',age(us.birth_date)) as age FROM public.users as us
left join public.user_weight as uw ON us.id = uw.user_id
left join public.user_preferences as up ON us.id = up.user_id
where us.target_loose_weight BETWEEN $1 AND $2
and us.birth_date BETWEEN $3 AND $4
and us.gender IN ($5, $6)
and us.profile_completed = true
and us.id != $7
${language ? `and up.language = '${language.toUpperCase()}'` : ''}
ORDER BY us.id DESC, uw.created_at DESC
OFFSET $8 LIMIT $9;
      `,
        [
          minTargetWeightLoose,
          maxTargetWeightLoose,
          minBirthDate,
          maxBirthDate,
          gender[0],
          gender[1],
          meId,
          skip,
          take,
        ],
      );

      const response = await this._setFriendRequest(users, meId);

      const pageMetaDto = new PageMetaDto({
        pageOptionsDto: userCriteria,
        itemCount: response.length,
      });

      return new ResultPageDto(response, pageMetaDto);
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  async searchByEmail({ email }: SearchByEmailDto) {
    return this._userRepository.findOne({ email });
  }

  async searchByPseudo(request: SearchByPseudoDto, me: UserEntity) {
    const userId = me.id;

    try {
      let usersQuery = this._userRepository
        .createQueryBuilder('users')
        .where('pseudo ILIKE :name', {
          name: `%${request.pseudo}%`,
        })
        .andWhere('profile_completed = :true', { true: true })
        .orderBy('created_at', request.order)
        .skip(request.skip)
        .take(request.take);

      if (!request.withMe) {
        usersQuery = usersQuery.andWhere('id != :userId', { userId });
      }

      const users = await usersQuery.getMany();

      const response = await this._setFriendRequest(
        users.toDtos(),
        userId,
      );

      const pageMetaDto = new PageMetaDto({
        pageOptionsDto: request,
        itemCount: response.length,
      });

      return new ResultPageDto(response, pageMetaDto);
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  private async _setFriendRequest(users: UserDto[], userId) {
    const response = [];
    if (users.length > 0) {
      const userIds = users.map((user) => user.id);
      const friendRequests = await this._friendRequestRepository
        .createQueryBuilder('friendRequest')
        .where([
          { from: userId, to: In(userIds) },
          { to: userId, from: In(userIds) },
        ])
        .leftJoinAndSelect('friendRequest.from', 'from')
        .leftJoinAndSelect('friendRequest.to', 'to')
        .getMany();

      users.forEach((detectedUser) => {
        const elem: any = { ...detectedUser };
        if (detectedUser.id !== userId) {
          elem.friendRequestStatus = 'not invited';
          const friendRequest = friendRequests.find(
            (el) =>
              el.from.id === detectedUser.id ||
              el.to.id === detectedUser.id,
          );
          if (friendRequest) {
            elem.friendRequestId = friendRequest.id;
            elem.friendRequestStatus = friendRequest.status;
          }
        }

        response.push(elem);
      });
    }
    return response;
  }
}
