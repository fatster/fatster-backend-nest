export enum CalendarEventType {
  Consultation = 'consultation',
  Report = 'report',
  Custom = 'custom',
}

export interface ICalendarEvent {
  start: Date;
  end: Date;
  title: string;
  type: CalendarEventType;
  patience: string[];
  meetingUrl: string;
}
