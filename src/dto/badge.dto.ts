import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { isDtos } from '../common/utils/entities';
import { BadgeEntity } from '../entities/badge.entity';
import { UserDto } from './user.dto';

export class BadgeDto extends AbstractDto {
  @ApiPropertyOptional()
  nameEn: string;

  @ApiPropertyOptional()
  nameFr: string;

  @ApiPropertyOptional()
  descriptionFr: string;

  @ApiPropertyOptional()
  descriptionEn: string;

  @ApiPropertyOptional()
  users: UserDto[];

  constructor(badge: BadgeEntity) {
    super(badge);
    this.nameEn = badge.nameEn;
    this.nameFr = badge.nameFr;
    this.descriptionEn = badge.descriptionEn;
    this.descriptionFr = badge.descriptionFr;
    this.users = isDtos(badge.users)
      ? badge.users.toDtos()
      : <any>badge.users;
  }
}
