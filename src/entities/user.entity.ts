import {
  AfterLoad,
  Column,
  Entity,
  Index,
  ManyToMany,
  OneToMany,
  OneToOne,
} from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { GenderEnum, UserRoleEnum } from '../common/enum/enum';
import { EmailTransformer } from '../common/utils/email.transformer';
import { UserDto } from '../dto/user.dto';
import { CalendarEntity } from '../modules/calendar/calendar.entity';
import { calculateAge } from '../utils/date';
import { BadgeEntity } from './badge.entity';
import { BlockEntity } from './block.entity';
import { CalendarSettingsEntity } from './calendarSettings.entity';
import { CommentEntity } from './comment.entity';
import { FirebaseAuthEntity } from './firebaseAuth.entity';
import { FriendEntity } from './friend.entity';
import { FriendRequestEntity } from './friendRequest.entity';
import { LikeEntity } from './like.entity';
import { NotificationEntity } from './notification.entity';
import { PostEntity } from './post.entity';
import { ProfessionalInfoEntity } from './professionalInfo.entity';
import { ReportEntity } from './report.entity';
import { UserActivityEntity } from './userActivity.entity';
import { UserNutritionEntity } from './userNutrition.entity';
import { UserPreferencesEntity } from './userPreferences.entity';
import { UserWeightEntity } from './userWeight.entity';

@Entity({ name: 'users' })
export class UserEntity extends AbstractEntity<UserDto> {
  constructor() {
    super();
  }

  @Column({ transformer: new EmailTransformer(), nullable: true })
  @Index({ unique: false })
  email: string;

  @Column({ nullable: true })
  fbId?: string;

  @Column({ nullable: true })
  appleId?: string;

  @Column({ nullable: true })
  name?: string;

  @Column({ nullable: true })
  surname?: string;

  @Column({ nullable: true })
  @Index({ unique: true })
  pseudo: string;

  @Column({ nullable: true })
  phone?: string;

  @Column({
    nullable: true /*transformer: new PasswordTransformer() */,
  })
  password: string;

  @Column({ type: 'float', nullable: true })
  originalWeight?: number;

  @Column({ type: 'float', nullable: true })
  targetLooseWeight?: number;

  @Column({ type: 'float', nullable: true })
  targetWeight?: number;

  @Column({ nullable: true })
  height?: number;

  @Column({
    nullable: true,
    type: 'enum',
    enum: GenderEnum,
  })
  gender?: GenderEnum;

  @Column({
    type: 'enum',
    enum: UserRoleEnum,
    array: true,
    default: [],
    nullable: true,
  })
  roles?: UserRoleEnum;

  // For OneSignal push notification
  @Column({ nullable: true })
  oneSignalPlayerId?: string;

  @Column({ nullable: true })
  authMethod?: string;

  @Column({ nullable: true })
  photoId?: string;

  @Column({ nullable: true, type: 'timestamp without time zone' })
  birthDate?: Date;

  // eslint-disable-next-line @typescript-eslint/tslint/config
  age?: number;

  @Column({ default: false })
  profileCompleted?: boolean;

  @OneToMany(() => PostEntity, (post) => post.user, {
    onDelete: 'CASCADE',
  })
  posts?: PostEntity[];

  @OneToMany(() => ReportEntity, (report) => report.post, {
    onDelete: 'CASCADE',
  })
  reports?: ReportEntity[];

  @OneToMany(
    () => UserWeightEntity,
    (userWeight) => userWeight.user,
    {
      onDelete: 'CASCADE',
    },
  )
  userWeights?: UserWeightEntity[];

  @OneToMany(
    () => FriendRequestEntity,
    (friendRequest) => friendRequest.from,
    {
      onDelete: 'CASCADE',
    },
  )
  invitedFriends?: FriendRequestEntity[];

  @OneToMany(
    () => FriendRequestEntity,
    (friendRequest) => friendRequest.to,
    {
      onDelete: 'CASCADE',
    },
  )
  friendRequestsBy?: FriendRequestEntity[];

  @OneToMany(() => FriendEntity, (friend) => friend.user, {
    onDelete: 'CASCADE',
  })
  friends?: FriendEntity[];

  @OneToMany(() => BlockEntity, (block) => block.from, {
    onDelete: 'CASCADE',
  })
  usersBlocked?: BlockEntity[];

  @OneToMany(() => CommentEntity, (comment) => comment.user, {
    onDelete: 'CASCADE',
  })
  comments?: CommentEntity[];

  @OneToMany(() => LikeEntity, (like) => like.user, {
    onDelete: 'CASCADE',
  })
  likes?: LikeEntity[];

  @OneToMany(() => BlockEntity, (block) => block.against, {
    onDelete: 'CASCADE',
  })
  blockedBy?: BlockEntity[];

  @OneToMany(
    () => NotificationEntity,
    (notification) => notification.user,
    {
      onDelete: 'CASCADE',
    },
  )
  notifications?: NotificationEntity[];

  @OneToMany(
    () => UserNutritionEntity,
    (userNutritionEntity) => userNutritionEntity.user,
    {
      onDelete: 'CASCADE',
    },
  )
  userNutritions?: UserNutritionEntity[];

  @OneToMany(
    () => UserActivityEntity,
    (userActivity) => userActivity.user,
    { onDelete: 'CASCADE' },
  )
  userActivities?: UserActivityEntity[];

  @Column({ nullable: true })
  lastConnection?: Date;

  @Column({ default: false })
  fromFirebase?: boolean;

  @ManyToMany(() => BadgeEntity, (badge) => badge.users)
  badges?: BadgeEntity[];

  @OneToOne(
    () => FirebaseAuthEntity,
    (firebaseAuth) => firebaseAuth.user,
    { nullable: true },
  )
  firebaseAuth?: FirebaseAuthEntity;

  @OneToOne(
    () => UserPreferencesEntity,
    (userPreferences) => userPreferences.user,
    { nullable: true },
  )
  userPreferences?: UserPreferencesEntity;

  @OneToOne(() => ProfessionalInfoEntity, (info) => info.user, {
    nullable: true,
  })
  professionalInfo?: ProfessionalInfoEntity;

  @ManyToMany(
    () => CalendarEntity,
    (calendar) => calendar.participants,
    {
      onDelete: 'CASCADE',
    },
  )
  events?: CalendarEntity[];

  @OneToOne(
    () => CalendarSettingsEntity,
    (settings) => settings.user,
    {
      nullable: true,
    },
  )
  calendarSettings?: CalendarSettingsEntity;

  @AfterLoad()
  prefixWithUrl() {
    if (this.photoId) {
      this.photoId = `${process.env.S3_BASE_URL}${this.photoId}?auto=compress&q=20`;
    }
  }

  @AfterLoad()
  setAge() {
    this.age = calculateAge(new Date(this.birthDate).getTime());
  }
  dtoClass = UserDto;
}
