import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { ArticleDto } from './article.dto';

export class NavigationMainPictureDto extends AbstractDto {
  @ApiPropertyOptional()
  nameEn: string;

  @ApiPropertyOptional()
  ctaEn: string;

  @ApiPropertyOptional()
  nameFr: string;

  @ApiPropertyOptional()
  ctaFr: string;

  @ApiPropertyOptional()
  backgroundPic: string;

  @ApiPropertyOptional()
  url: string;

  @ApiPropertyOptional()
  isActive: boolean;

  article: ArticleDto;
}
