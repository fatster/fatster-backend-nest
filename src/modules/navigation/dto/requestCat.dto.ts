// eslint-disable-next-line import/named
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

import { PageOptionsDto } from '../../../common/dto/PageOptionsDto';

export class RequestCatDto extends PageOptionsDto {
  @ApiProperty()
  @IsNotEmpty()
  readonly categoryId: string;
}
