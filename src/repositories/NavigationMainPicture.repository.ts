import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { NavigationMainPictureEntity } from '../entities/navigationMainPicture.entity';

@EntityRepository(NavigationMainPictureEntity)
export class NavigationMainPictureRepository extends Repository<
  NavigationMainPictureEntity
> {}
