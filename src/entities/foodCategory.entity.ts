import { IsString } from 'class-validator';
import { Column, Entity, OneToMany } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { FoodCategoryDto } from '../dto/foodCategory.dto';
import { FoodEntity } from './food.entity';

@Entity({ name: 'food_category' })
export class FoodCategoryEntity extends AbstractEntity<
  FoodCategoryDto
> {
  @Column()
  @IsString()
  textEn: string;

  @Column()
  @IsString()
  textFr: string;

  @OneToMany(() => FoodEntity, (activity) => activity.foodCategory, {
    onDelete: 'CASCADE',
  })
  foods: FoodEntity[];

  dtoClass = FoodCategoryDto;
}
