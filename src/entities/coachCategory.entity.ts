import { IsString } from 'class-validator';
import { Column, Entity, OneToMany } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { CoachCategoryDto } from '../dto/coachCategory.dto';
import { CoachEntity } from './coach.entity';

@Entity({ name: 'coach_category' })
export class CoachCategoryEntity extends AbstractEntity<
  CoachCategoryDto
> {
  @Column()
  @IsString()
  typeEn: string;

  @Column()
  @IsString()
  typeFr: string;

  @OneToMany(() => CoachEntity, (coach) => coach.coachCategory, {
    onDelete: 'CASCADE',
  })
  coach: CoachEntity[];

  dtoClass = CoachCategoryDto;
}
