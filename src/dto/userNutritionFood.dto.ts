import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { NutritionFoodType } from '../common/enum/enum';
import { isDto } from '../common/utils/entities';
import { FoodEntity } from '../entities/food.entity';
import { UserNutritionEntity } from '../entities/userNutrition.entity';
import { UserNutritionFoodEntity } from '../entities/userNutritionFood.entity';

export function getCaloriesFromFood(
  nutrition: Pick<
    UserNutritionFoodEntity,
    'portionType' | 'portion' | 'grams' | 'calories'
  >,
  food: Pick<FoodEntity, 'energyEuRegulation'>,
) {
  let calories = 0;
  if (food) {
    switch (nutrition.portionType) {
      case NutritionFoodType.Portions:
        calories = nutrition.portion * food.energyEuRegulation;
        break;
      case NutritionFoodType.Grams:
        calories = (food.energyEuRegulation / 100) * nutrition.grams;
        break;
      case NutritionFoodType.Calories:
        calories = nutrition.calories;
    }
  }
  return Math.round(calories * 10) / 10;
}

export class UserNutritionFoodDto extends AbstractDto {
  @ApiPropertyOptional()
  userNutrition: UserNutritionEntity;

  @ApiPropertyOptional()
  food: FoodEntity;

  @ApiPropertyOptional()
  portion: number;

  @ApiPropertyOptional()
  calories: number;

  @ApiPropertyOptional()
  grams: number;

  @ApiProperty({
    enum: Object.values(NutritionFoodType),
  })
  portionType: NutritionFoodType;

  summaryCalories: number;
  constructor(userNutritionFood: UserNutritionFoodEntity) {
    super(userNutritionFood);

    this.portion = userNutritionFood.portion;
    this.calories = userNutritionFood.calories;
    this.grams = userNutritionFood.grams;
    this.portionType = userNutritionFood.portionType;
    this.food = isDto(userNutritionFood.food)
      ? userNutritionFood.food.toDto()
      : <any>userNutritionFood.food;
    this.userNutrition = isDto(userNutritionFood.userNutrition)
      ? userNutritionFood.userNutrition.toDto()
      : <any>userNutritionFood.userNutrition;
    this.summaryCalories = getCaloriesFromFood(
      userNutritionFood,
      this.food,
    );
  }
}
