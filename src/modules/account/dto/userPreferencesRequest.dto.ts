import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';

import { Language } from '../../../shared/services/translation.service';

export class UserPreferencesRequestDto {
  // MY INFORMAITON
  @ApiProperty()
  @IsBoolean()
  showStatistics: boolean;

  @ApiProperty()
  @IsBoolean()
  showFeed: boolean;

  @ApiProperty()
  @IsBoolean()
  showPostsInPublicFeed: boolean;

  @ApiProperty()
  @IsBoolean()
  showProfile: boolean;

  @ApiProperty()
  @IsBoolean()
  showPseudoInSearch: boolean;

  @ApiProperty()
  @IsBoolean()
  showPostsAppLanguageOnly: boolean;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  language: Language;

  // PUSH NOTIFICATION
  @ApiProperty()
  @IsBoolean()
  pushNotificationLikes: boolean;

  @ApiProperty()
  @IsBoolean()
  pushNotificationComments: boolean;

  @ApiProperty()
  @IsBoolean()
  pushNotificationFriendRequest: boolean;

  @ApiProperty()
  pushNotificationNewFriend: boolean;

  @ApiProperty()
  @IsBoolean()
  pushNotificationAdminMessages: boolean;

  @ApiProperty()
  @IsBoolean()
  pushNotificationMentionMessages: boolean;

  // EMAILS
  @ApiProperty()
  @IsBoolean()
  newsLetter: boolean;

  @ApiProperty()
  @IsBoolean()
  adminEmails: boolean;
}
