export function createProPath(path?: string) {
  if (path) {
    return `pro/${path}`;
  }
  return `pro`;
}
