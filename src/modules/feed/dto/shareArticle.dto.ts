import {
  ApiHideProperty,
  ApiPropertyOptional,
} from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class ShareArticleDto {
  @ApiPropertyOptional()
  @IsOptional()
  readonly language: string;

  @ApiPropertyOptional()
  @IsOptional()
  readonly type: string;

  @ApiPropertyOptional()
  @IsOptional()
  readonly articleId: string;

  @ApiHideProperty()
  userId: string;
}
