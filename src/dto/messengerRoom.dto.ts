import { AbstractDto } from '../common/dto/AbstractDto';
import { MessengerMessageDto } from './messengerMessage.dto';
import { UserDto } from './user.dto';

export class MessengerRoomDto extends AbstractDto {
  text: string;

  messages: MessengerMessageDto[];

  friends: UserDto[];
}
