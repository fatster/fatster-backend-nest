export function createUTCDate(now = new Date()): Date {
  return new Date(now.getTime() + now.getTimezoneOffset() * 60000);
}

export function formatDate(
  format: 'YYYY-MM-DD',
  date = new Date(),
): string {
  switch (format) {
    case 'YYYY-MM-DD':
      return date.toISOString().slice(0, 10);
  }
}

export function startOfDay(day = new Date()) {
  const currentTimeZoneOffsetInHours = -day.getTimezoneOffset() / 60;
  day.setHours(currentTimeZoneOffsetInHours, 0, 0, 0);
  return day;
}

export function getWeek(date = new Date()) {
  const onejan = new Date(date.getFullYear(), 0, 1);
  return Math.ceil(
    ((date.getTime() - onejan.getTime()) / 86400000 +
      onejan.getDay() +
      6) /
      7,
  );
}

export function calculateAge(birthday: number): number {
  const ageDifMs = Date.now() - birthday;
  const ageDate = new Date(ageDifMs);
  return Math.abs(ageDate.getUTCFullYear() - 1970);
}

export function isDateBeforeDate(date: Date, date2: Date): boolean {
  return (
    new Date(date.toDateString()) < new Date(date2.toDateString())
  );
}

export function addDays(date: Date, days: number): Date {
  const result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}

export function subtractYearsFromNow(years = 0): Date {
  const result = new Date();
  result.setFullYear(result.getFullYear() - years);
  return result;
}
