import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { isDto } from '../common/utils/entities';
import { FirebaseAuthEntity } from '../entities/firebaseAuth.entity';
import { UserDto } from './user.dto';

export class FirebaseAuthDto extends AbstractDto {
  @ApiPropertyOptional()
  hash: string;

  @ApiPropertyOptional()
  salt: string;

  @ApiPropertyOptional()
  user: UserDto;

  constructor(firebaseAuth: FirebaseAuthEntity) {
    super(firebaseAuth);
    this.hash = firebaseAuth.hash;
    this.salt = firebaseAuth.salt;
    this.user = isDto(firebaseAuth.user)
      ? firebaseAuth.user.toDto()
      : <any>firebaseAuth.user;
  }
}
