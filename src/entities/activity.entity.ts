import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { ActivityDto } from '../dto/activity.dto';
import { ActivityCategoryEntity } from './activityCategory.entity';
import { UserActivityEntity } from './userActivity.entity';

@Entity({ name: 'activity' })
export class ActivityEntity extends AbstractEntity<ActivityDto> {
  @Column()
  textEn: string;

  @Column()
  textFr: string;

  @Column({ type: 'float', nullable: true })
  met: number;

  @ManyToOne(
    () => ActivityCategoryEntity,
    (activityCategory) => activityCategory.activities,
  )
  activityCategory: ActivityCategoryEntity;

  @OneToMany(
    () => UserActivityEntity,
    (userActivity) => userActivity.activity,
  )
  userActivities: UserActivityEntity[];

  dtoClass = ActivityDto;
}
