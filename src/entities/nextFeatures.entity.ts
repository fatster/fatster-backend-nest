import { Column, Entity } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { NextFeaturesDto } from '../dto/nextFeatures.dto';

@Entity({ name: 'next_features' })
export class NextFeaturesEntity extends AbstractEntity<
  NextFeaturesDto
> {
  @Column()
  name: string;

  @Column({ default: 0 })
  likesCount: number;

  dtoClass = NextFeaturesDto;
}
