import { Column, Entity, ManyToOne } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { NutritionFoodType } from '../common/enum/enum';
import { UserNutritionFoodDto } from '../dto/userNutritionFood.dto';
import { FoodEntity } from './food.entity';
import { UserNutritionEntity } from './userNutrition.entity';

@Entity({ name: 'user_nutrition_food' })
export class UserNutritionFoodEntity extends AbstractEntity<
  UserNutritionFoodDto
> {
  @ManyToOne(
    () => UserNutritionEntity,
    (lineItem) => lineItem.nutritionFood,
    { onDelete: 'CASCADE' },
  )
  userNutrition: UserNutritionEntity;

  @ManyToOne(() => FoodEntity)
  food: FoodEntity;

  @Column({ type: 'float', nullable: true })
  portion: number;

  @Column({ type: 'float', nullable: true })
  calories: number;

  @Column({ type: 'float', nullable: true })
  grams: number;

  @Column({
    type: 'enum',
    enum: NutritionFoodType,
    default: NutritionFoodType.Portions,
  })
  portionType: NutritionFoodType;

  summaryCalories: number;

  dtoClass = UserNutritionFoodDto;
}
