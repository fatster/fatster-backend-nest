import {
  ApiHideProperty,
  ApiProperty,
  ApiPropertyOptional,
} from '@nestjs/swagger';
import { IsArray, IsOptional } from 'class-validator';

import { PostStatusEnum } from '../../../common/enum/enum';

export class WritePostDto {
  @IsOptional()
  readonly text?: string;

  @IsArray()
  @ApiPropertyOptional()
  @IsOptional()
  readonly friendsToTag: string[];

  @ApiPropertyOptional()
  @IsOptional()
  readonly status: PostStatusEnum = PostStatusEnum.PUBLIC;

  @ApiProperty()
  @ApiPropertyOptional()
  @IsOptional()
  photos?: string[];

  @ApiHideProperty()
  userId: string;
}
