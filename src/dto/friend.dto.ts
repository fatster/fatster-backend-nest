import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { isDto } from '../common/utils/entities';
import { FriendEntity } from '../entities/friend.entity';
import { UserDto } from './user.dto';

export class FriendDto extends AbstractDto {
  @ApiPropertyOptional()
  user: UserDto;

  @ApiPropertyOptional()
  friend: UserDto;

  constructor(friend: FriendEntity) {
    super(friend);
    this.user = isDto(friend.user)
      ? friend.user.toDto()
      : <any>friend.user;
    this.friend = isDto(friend.friend)
      ? friend.friend.toDto()
      : <any>friend.friend;
  }
}
