import { IsNumber, Min } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
} from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { UserActivityDto } from '../dto/userActivity.dto';
import { ActivityEntity } from './activity.entity';
import { PostEntity } from './post.entity';
import { UserEntity } from './user.entity';

@Entity({ name: 'user_activity' })
export class UserActivityEntity extends AbstractEntity<
  UserActivityDto
> {
  @Column()
  @IsNumber()
  @Min(0)
  duration: number;

  @Column({ nullable: true })
  @IsNumber()
  calorieBurned: number;

  @OneToOne(() => PostEntity, (post) => post.userActivity, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  post: PostEntity;

  @ManyToOne(
    () => ActivityEntity,
    (activity) => activity.userActivities,
  )
  activity: ActivityEntity;

  @ManyToOne(() => UserEntity, (user) => user.userActivities, {
    onDelete: 'CASCADE',
  })
  user: UserEntity;

  dtoClass = UserActivityDto;
}
