import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsDateString, IsEnum, IsOptional } from 'class-validator';

import { PageOptionsDto } from '../../../common/dto/PageOptionsDto';
import { PostTypeEnum } from '../../../common/enum/enum';

export class GetPostsDto extends PageOptionsDto {
  @IsOptional()
  @IsEnum(PostTypeEnum)
  @ApiPropertyOptional({ enum: Object.values(PostTypeEnum) })
  readonly postType: PostTypeEnum;

  @IsOptional()
  @IsDateString()
  @ApiPropertyOptional()
  readonly dateFrom?: string;

  @IsOptional()
  @IsDateString()
  @ApiPropertyOptional()
  readonly dateTo?: string;
}
