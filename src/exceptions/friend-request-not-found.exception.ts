'use strict';

import { NotFoundException } from '@nestjs/common';

export class FriendRequestNotFoundException extends NotFoundException {
  constructor(error?: string) {
    super('error.friend_request_not_found', error);
  }
}
