import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { FriendRepository } from '../../repositories/friend.repository';
import { FriendRequestRepository } from '../../repositories/friendRequest.repository';
import { MessengerRoomRepository } from '../../repositories/messengerRoom.repository';
import { AuthModule } from '../auth/auth.module';
import { NotificationModule } from '../notification/notification.module';
import { FriendController } from './friend.controller';
import { FriendService } from './friend.service';

@Module({
  imports: [
    forwardRef(() => AuthModule),
    forwardRef(() => NotificationModule),
    TypeOrmModule.forFeature([
      MessengerRoomRepository,
      FriendRepository,
      FriendRequestRepository,
    ]),
  ],
  controllers: [FriendController],
  exports: [FriendService],
  providers: [FriendService],
})
export class FriendModule {}
