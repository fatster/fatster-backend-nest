import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';

import { PayloadSuccessDto } from '../../common/dto/PayloadSuccessDto';
import { AuthUser } from '../../decorators/auth-user.decorator';
import { UserDto } from '../../dto/user.dto';
import { UserEntity } from '../../entities/user.entity';
import { AuthGuard } from '../../guards/auth.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
import { UserService } from '../../modules/user/user.service';
import { AccountService } from './account.service';
import { ChangePasswordDto } from './dto/changePassword.dto';
import { DeleteAccountByIdDto } from './dto/deleteAccountById.dto';
import { UpdateOneSignalPlayerDto } from './dto/updateOneSignalPlayer.dto';
import { UpdateProfilePictureDto } from './dto/updateProfilePictureDto';
import { UserOnBoardingDto } from './dto/userOnBoarding.dto';
import { UserPreferencesRequestDto } from './dto/userPreferencesRequest.dto';
import { UserUpdateDto } from './dto/userUpdate.dto';

@Controller('account')
@ApiTags('Account')
export class AccountController {
  constructor(
    private _accountService: AccountService,
    private _userService: UserService,
  ) {}

  @Put('password/')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Password has been updated',
  })
  async changePassword(
    @Body() changePasswordDto: ChangePasswordDto,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    await this._accountService.changePassword(
      changePasswordDto,
      user,
    );
    return { message: 'password updated' };
  }

  @Get('me')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({ type: UserDto, description: 'current user info' })
  async getCurrentUser(
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const u = await this._userService.findOneWithRelations(
      'user.id = :id',
      {
        id: user.id,
      },
    );
    return {
      message: 'current user info',
      data: u,
    };
  }

  @Post('photo')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'profile picture updated',
  })
  public async updateProfilePicture(
    @AuthUser() user: UserEntity,
    @Body() updateProfilePictureDto: UpdateProfilePictureDto,
  ): Promise<PayloadSuccessDto> {
    const photoId = await this._accountService.updateProfilePicture(
      user.id,
      updateProfilePictureDto.fileName,
    );
    return {
      message: 'profile picture has been updated',
      data: photoId,
    };
  }

  @Put()
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({ type: UserDto, description: 'user updated' })
  public async update(
    @Body() userRegisterDto: UserUpdateDto,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const updatedUser = await this._accountService.updateUser(
      userRegisterDto,
      user,
    );

    const response = await this._userService.findOne({
      id: updatedUser.id,
    });

    return {
      message: 'user updated',
      data: response.toDto(),
    };
  }

  @Post('onboarding')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({ type: UserDto, description: 'onboarding done' })
  public async onboarding(
    @Body() userOnBoardingDto: UserOnBoardingDto,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const updatedUser = await this._accountService.updateUser(
      userOnBoardingDto,
      user,
      true,
    );

    const response = await this._userService.findOne({
      id: updatedUser.id,
    });

    return {
      message: 'onboarding done',
      data: response.toDto(),
    };
  }

  @Post('userOnboarding')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({ type: UserDto, description: 'onboarding call' })
  public async userOnboarding(
    @Body() userOnBoardingDto: UserOnBoardingDto,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const updatedUser = await this._accountService.userOnboarding(
      userOnBoardingDto,
      user,
    );

    return {
      message: 'onboarding done',
      data: updatedUser,
    };
  }

  @Post('userAfterOnboarding')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    type: Boolean,
    description: 'After onboarding call',
  })
  public async userAfterOnboarding(
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const result = await this._accountService.userAfterOnboarding(
      user,
    );

    return {
      message: 'After onboarding done',
      data: result,
    };
  }

  @Delete('delete')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'delete account' })
  public async deleteAccount(
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const deleteAccount = await this._accountService.deleteAccount(
      user,
    );
    return {
      message: 'account deleted',
      data: deleteAccount,
    };
  }

  @Delete('/delete/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @ApiOkResponse({
    description: 'Delete Post',
  })
  async deleteAccountById(
    @AuthUser() user: UserEntity,
    @Param() deletePostDto: DeleteAccountByIdDto,
  ): Promise<PayloadSuccessDto> {
    const post = await this._accountService.deleteAccountById(
      deletePostDto,
      user,
    );

    return { message: 'post has beed deleteed', data: post };
  }

  @Get('setCoach')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'set coach' })
  async setCoach() {
    /*const preferences = await this._accountService.coachOldUser(
      user.id,
    );
    return {
      message: 'user preferences',
      data: preferences,
    };*/
  }

  @Get('preferences')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'user preferences' })
  async getPreferences(
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const preferences = await this._accountService.getPreferences(
      user.id,
    );
    return {
      message: 'user preferences',
      data: preferences,
    };
  }

  @Put('preferences')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'edit preferences' })
  public async editPreferences(
    @AuthUser() user: UserEntity,
    @Body() preferences: UserPreferencesRequestDto,
  ): Promise<PayloadSuccessDto> {
    const setPreferences = await this._accountService.editPreferences(
      user,
      preferences,
    );
    return {
      message: 'preferences saved',
      data: setPreferences,
    };
  }

  @Put('oneSignalPlayerId')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  public async updateOneSignalPlayerId(
    @Body() updateOneSignalPlayerDto: UpdateOneSignalPlayerDto,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    updateOneSignalPlayerDto.userId = <any>user.id;
    await this._accountService.updateOneSignalPlayerId(
      updateOneSignalPlayerDto,
    );
    return {
      message: 'One Signal player ID has been updated',
    };
  }
}
