import {
  BadRequestException,
  ClassSerializerInterceptor,
  ValidationPipe,
} from '@nestjs/common';
import { NestFactory, Reflector } from '@nestjs/core';
import { ExpressAdapter } from '@nestjs/platform-express';
import { APIGatewayProxyHandler } from 'aws-lambda';
import * as awsServerlessExpress from 'aws-serverless-express';
import * as express from 'express';
import * as helmet from 'helmet';
import { Server } from 'http';

import { AppModule } from './app.module';
import { AllExceptionsFilter } from './filters/all-exception.filter';
import { BadRequestExceptionFilter } from './filters/bad-request.filter';
import { ResponseTransformInterceptor } from './interceptors/response-interceptor.service';
import { ConfigService } from './shared/services/config.service';
import { RollbarService } from './shared/services/rollbar.service';
import { SharedModule } from './shared/shared.module';
import { setupSwagger } from './viveo-swagger';

let cachedServer: Server;

const bootstrapServer = async (): Promise<Server> => {
  const expressApp = express();
  const adapter = new ExpressAdapter(expressApp);
  const app = await NestFactory.create(AppModule, adapter, {
    cors: true,
  });

  const configService = app.select(SharedModule).get(ConfigService);

  if (['development', 'staging'].includes(configService.nodeEnv)) {
    setupSwagger(app);
  }

  app.use(helmet());

  const reflector = app.get(Reflector);
  const roolBarService = app.select(SharedModule).get(RollbarService);

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      exceptionFactory: (errors) => new BadRequestException(errors),
    }),
  );

  app.useGlobalFilters(
    new AllExceptionsFilter(roolBarService),
    new BadRequestExceptionFilter(reflector),
  );

  app.useGlobalInterceptors(
    new ClassSerializerInterceptor(reflector),
    new ResponseTransformInterceptor(),
  );

  await app.init();

  return awsServerlessExpress.createServer(expressApp);
};

export const handler: APIGatewayProxyHandler = async (
  event,
  context,
) => {
  context.callbackWaitsForEmptyEventLoop = false;
  if (!cachedServer) {
    const server = await bootstrapServer();
    cachedServer = server;
    // server.close();
    return awsServerlessExpress.proxy(
      server,
      event,
      context,
      'PROMISE',
    ).promise;
  }
  return awsServerlessExpress.proxy(
    cachedServer,
    event,
    context,
    'PROMISE',
  ).promise;
};
