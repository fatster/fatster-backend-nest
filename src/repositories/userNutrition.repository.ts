import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { UserNutritionEntity } from '../entities/userNutrition.entity';

@EntityRepository(UserNutritionEntity)
export class UserNutritionRepository extends Repository<
  UserNutritionEntity
> {
  public async countHealthyNutrition(id: string, from?) {
    const query = this.createQueryBuilder('nutrition')
      .select('COUNT(nutrition.id) AS nb')
      .where('user_id = :id', { id })
      .andWhere('healthy_type = :type', { type: 'yes' });

    if (from) {
      query.andWhere('created_at > :from', {
        from,
      });
    }
    return query.getRawOne();
  }

  public async countNutrition(id: string, from?) {
    const query = this.createQueryBuilder('nutrition')
      .select('COUNT(nutrition.id) AS nb')
      .where('user_id = :id', { id });

    if (from) {
      query.andWhere('created_at > :from', {
        from,
      });
    }
    return query.getRawOne();
  }
}
