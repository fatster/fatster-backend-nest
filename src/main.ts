import {
  BadRequestException,
  ClassSerializerInterceptor,
  ValidationPipe,
} from '@nestjs/common';
import { NestFactory, Reflector } from '@nestjs/core';
import {
  ExpressAdapter,
  NestExpressApplication,
} from '@nestjs/platform-express';
// import * as helmet from 'helmet';
import * as morgan from 'morgan';

import { AppModule } from './app.module';
// import { AllExceptionsFilter } from './filters/all-exception.filter';
import { BadRequestExceptionFilter } from './filters/bad-request.filter';
import { ResponseTransformInterceptor } from './interceptors/response-interceptor.service';
import { ConfigService } from './shared/services/config.service';
// import { RollbarService } from './shared/services/rollbar.service';
import { SharedModule } from './shared/shared.module';
import { setupSwagger } from './viveo-swagger';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(
    AppModule,
    new ExpressAdapter(),
    { cors: true },
  );

  // app.use(helmet());

  app.setGlobalPrefix('api/v1');
  app.use(morgan('combined'));

  const reflector = app.get(Reflector);

  // const roolBarService = app.select(SharedModule).get(RollbarService);
  const configService = app.select(SharedModule).get(ConfigService);

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      exceptionFactory: (errors) => new BadRequestException(errors),
    }),
  );

  app.useGlobalFilters(
    // new AllExceptionsFilter(roolBarService),
    new BadRequestExceptionFilter(reflector),
  );

  app.useGlobalInterceptors(
    new ClassSerializerInterceptor(reflector),
    new ResponseTransformInterceptor(),
  );

  if (['development', 'staging'].includes(configService.nodeEnv)) {
    setupSwagger(app);
  }

  const port = configService.getNumber('PORT');
  await app.listen(port);

  console.info(`server running on port ${port}`);
}

bootstrap();
