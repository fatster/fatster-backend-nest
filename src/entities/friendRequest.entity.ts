import { IsIn } from 'class-validator';
import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { StatusFriendRequestEnum } from '../common/enum/enum';
import { FriendRequestDto } from '../dto/friendRequest.dto';
import { NotificationEntity } from './notification.entity';
import { UserEntity } from './user.entity';

@Entity({ name: 'friend_request' })
export class FriendRequestEntity extends AbstractEntity<
  FriendRequestDto
> {
  @ManyToOne(() => UserEntity, (user) => user.invitedFriends, {
    // primary: true,
    onDelete: 'CASCADE',
  })
  from: UserEntity;

  @ManyToOne(() => UserEntity, (user) => user.friendRequestsBy, {
    // primary: true,
    onDelete: 'CASCADE',
  })
  to: UserEntity;

  @Column({
    nullable: true,
    type: 'enum',
    enum: StatusFriendRequestEnum,
  })
  @IsIn(['accepted', 'refused', 'pending'])
  status: StatusFriendRequestEnum;

  @OneToMany(
    () => NotificationEntity,
    (notification) => notification.friendRequest,
    {
      onDelete: 'CASCADE',
    },
  )
  notifications?: NotificationEntity[];

  friendsCount;

  dtoClass = FriendRequestDto;
}
