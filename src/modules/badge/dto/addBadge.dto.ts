import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsUUID } from 'class-validator';

import { BadgeEnum } from '../../../common/enum/enum';

export class AddBadgeDto {
  @IsUUID()
  @IsNotEmpty()
  @ApiProperty()
  readonly userId: string;

  @IsEnum(BadgeEnum)
  @IsNotEmpty()
  @ApiProperty()
  readonly badge: BadgeEnum;
}
