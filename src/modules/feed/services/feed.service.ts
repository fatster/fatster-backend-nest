import { BadRequestException, Injectable } from '@nestjs/common';

import { PostStatusEnum } from '../../../common/enum/enum';
import { UserDto } from '../../../dto/user.dto';
import { UserEntity } from '../../../entities/user.entity';
import { CanNotFetchFeedOfANonFriendUser } from '../../../exceptions/can-not-fetch-feed-of-a-non-friend-user.exception';
import { BlockRepository } from '../../../repositories/block.repository';
import { FriendRepository } from '../../../repositories/friend.repository';
import { PostRepository } from '../../../repositories/post.repository';
import { UserRepository } from '../../../repositories/user.repository';
import { UserPreferencesRepository } from '../../../repositories/userPreferences.repository';
import { GetFriendsPostsDto } from '.././dto/getFriendsPosts.dto';
import { GetPostByIdDto } from '.././dto/getPostById.dto';
import { GetTrendingPostsDto } from '.././dto/getTrendingPosts.dto';
import { GetUsersPostsDto } from '.././dto/getUsersPosts.dto';

@Injectable()
export class FeedFeedService {
  constructor(
    private readonly _postRepository: PostRepository,
    private readonly _friendRepository: FriendRepository,
    private readonly _userRepository: UserRepository,
    private readonly _blockRepository: BlockRepository,
    private readonly _userPreferencesRepository: UserPreferencesRepository,
  ) {}

  public async getPostById(
    getPostByIdRequestDto: GetPostByIdDto,
    user: UserEntity,
  ) {
    try {
      const lang = await this.getUserLangPosts(user.id);
      const result = await this._postRepository.getPostsWithRelations(
        user,
        lang,
        {
          where: {
            where: { id: getPostByIdRequestDto.postId },
          },
          addFriendRequests: true,
        },
      );

      return result[0];
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  public async getUsersPosts(
    getUsersPostsDto: GetUsersPostsDto,
    currentUser: UserDto,
  ) {
    try {
      let userToFetch = currentUser.id;

      // If userId exist, that means you're fetching user's feed that is not yours, so we check if it's one of your friends
      if (getUsersPostsDto.userId) {
        const showFeedQuery = this._userPreferencesRepository
          .createQueryBuilder('userPreferences')
          .select('userPreferences.showFeed')
          .where('userPreferences.user = :id', {
            id: getUsersPostsDto.userId,
          })
          .getOne();
        const areFriendsQuery = this._friendRepository.areFriends(
          currentUser.id,
          getUsersPostsDto.userId,
        );
        const [showFeed, areFriends] = await Promise.all([
          showFeedQuery,
          areFriendsQuery,
        ]);

        if (
          !(
            areFriends || currentUser.id === getUsersPostsDto.userId
          ) &&
          showFeed.showFeed === false &&
          // quick fix, @Todo add Admin role to nutrimis user
          currentUser.id !== '8288a255-cc0d-4c04-95dd-45bad25fdeac'
        ) {
          throw new CanNotFetchFeedOfANonFriendUser();
        }
        userToFetch = getUsersPostsDto.userId;
      }

      const lang = await this.getUserLangPosts(currentUser.id);
      return this._postRepository.getPostsWithRelations(
        <any>currentUser,
        lang,
        {
          ...(getUsersPostsDto.userId
            ? {
                where: {
                  where: 'user.id = :id and posts.status != :status',
                  parameters: {
                    status: PostStatusEnum.PRIVATE,
                    id: userToFetch,
                  },
                },
              }
            : {
                where: {
                  where: 'user.id = :id',
                  parameters: {
                    id: userToFetch,
                  },
                },
              }),
          order: getUsersPostsDto.order,
          take: getUsersPostsDto.take,
          skip: getUsersPostsDto.skip,
          addFriendRequests: getUsersPostsDto.userId ? true : false,
        },
      );
    } catch (err) {
      throw new Error(err);
    }
  }

  public async getTrendingPostsIds() {
    return this._postRepository.getPostsByIds([
      '007f2c60-3d86-4523-a4f3-b89ec76e2bc7',
      '00ae1c35-720d-486d-83a4-d5fdf318d5c0',
    ]);
  }

  public async getTrendingPosts(
    getTrendingPostsDto: GetTrendingPostsDto,
  ) {
    try {
      const lang = await this.getUserLangPosts(
        getTrendingPostsDto.user.id,
      );
      return this._postRepository.getPostsWithRelations(
        getTrendingPostsDto.user,
        lang,
        {
          order: getTrendingPostsDto.order,
          take: getTrendingPostsDto.take,
          skip: getTrendingPostsDto.skip,
          rank: true,
          addFriendRequests: true,
          ...(getTrendingPostsDto.withPrivacy
            ? {
                where: {
                  where: 'posts.status = :status',
                  parameters: {
                    status: PostStatusEnum.PUBLIC,
                  },
                },
              }
            : {}),
        },
      );
    } catch (err) {
      throw new Error(err);
    }
  }

  public async getFriendsPosts(
    getFriendsPostsDto: GetFriendsPostsDto,
  ) {
    try {
      const friends = await this._friendRepository
        .createQueryBuilder('friends')
        .select('friends.friend_id')
        .where('friends.user_id = :id', {
          id: getFriendsPostsDto.user.id,
        })
        .getRawMany();

      const userIdToFetch = friends.map((x) => x.friend_id);
      userIdToFetch.push(getFriendsPostsDto.user.id);

      const lang = await this.getUserLangPosts(
        getFriendsPostsDto.user.id,
      );
      return this._postRepository.getPostsWithRelations(
        getFriendsPostsDto.user,
        lang,
        {
          order: getFriendsPostsDto.order,
          take: getFriendsPostsDto.take,
          skip: getFriendsPostsDto.skip,
          rank: false,
          addFriendRequests: false,
          ...(getFriendsPostsDto.withPrivacy
            ? {
                where: {
                  where:
                    'user.id IN (:...ids) AND posts.status != :status',
                  parameters: {
                    ids: userIdToFetch,
                    status: PostStatusEnum.PRIVATE,
                  },
                },
              }
            : {
                where: {
                  where: 'user.id IN (:...ids)',
                  parameters: { ids: userIdToFetch },
                },
              }),
        },
      );
    } catch (err) {
      throw new Error(err);
    }
  }

  public async getUserLangPosts(userId: string) {
    const user = await this._userRepository.findOne({
      where: {
        id: userId,
      },
      relations: ['userPreferences'],
    });

    return user.userPreferences.showPostsAppLanguageOnly
      ? user.userPreferences.language
      : null;
  }
}
