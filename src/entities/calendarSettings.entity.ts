import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import {
  CalendarSettingsDto,
  WorkDaysRange,
  WorkTimeTuple,
} from '../dto/calendarSettings.dto';
import { UserEntity } from './user.entity';

@Entity({ name: 'calendar_settings' })
export class CalendarSettingsEntity extends AbstractEntity<
  CalendarSettingsDto
> {
  @Column({ type: 'text' })
  region: string;

  @Column({ type: 'text' })
  country: string;

  @Column({ type: 'text' })
  dataFormat: string;

  @Column({ type: 'text' })
  timeFormat: string;

  @Column({ type: 'text', array: true })
  workTimeRange: WorkTimeTuple;

  @Column({ type: 'text', array: true })
  workDaysRange: WorkDaysRange;

  @Column({ type: 'text' })
  timeZone: string;

  @OneToOne(() => UserEntity, (user) => user.calendarSettings, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  user: UserEntity;

  dtoClass = CalendarSettingsDto;
}
