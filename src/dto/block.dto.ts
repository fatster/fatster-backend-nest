import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { UserDto } from './user.dto';

export class BlockDto extends AbstractDto {
  @ApiPropertyOptional()
  from: UserDto;

  @ApiPropertyOptional()
  against: UserDto;
}
