import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsNumberString,
  IsString,
} from 'class-validator';

export class CalorieByActivityDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly activityId: string;

  @IsNotEmpty()
  @ApiProperty()
  @IsNumberString()
  readonly duration: number;
}
