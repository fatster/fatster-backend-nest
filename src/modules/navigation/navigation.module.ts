import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ArticleRepository } from '../../repositories/article.repository';
import { ArticleCategoryRepository } from '../../repositories/articleCategory.repository';
import { CoachRepository } from '../../repositories/coach.repository';
import { CoachCategoryRepository } from '../../repositories/coachCategory.repository';
import { FriendRepository } from '../../repositories/friend.repository';
import { FriendRequestRepository } from '../../repositories/friendRequest.repository';
import { LikeRepository } from '../../repositories/like.repository';
import { NavigationMainPictureRepository } from '../../repositories/NavigationMainPicture.repository';
import { UserRepository } from '../../repositories/user.repository';
import { NavigationController } from './navigation.controller';
import { NavigationService } from './navigation.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ArticleRepository,
      ArticleCategoryRepository,
      CoachRepository,
      CoachCategoryRepository,
      UserRepository,
      FriendRequestRepository,
      FriendRepository,
      LikeRepository,
      NavigationMainPictureRepository,
    ]),
  ],
  controllers: [NavigationController],
  exports: [NavigationService],
  providers: [NavigationService],
})
export class NavigationModule {}
