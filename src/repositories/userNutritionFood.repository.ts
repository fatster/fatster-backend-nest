import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { UserNutritionFoodEntity } from '../entities/userNutritionFood.entity';

@EntityRepository(UserNutritionFoodEntity)
export class UserNutritionFoodRepository extends Repository<
  UserNutritionFoodEntity
> {}
