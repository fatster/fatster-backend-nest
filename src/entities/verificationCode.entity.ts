import { Column, Entity } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { VerificationCodeDto } from '../dto/verificationCode.dto';

@Entity({ name: 'verification_code' })
export class VerificationCodeEntity extends AbstractEntity<
  VerificationCodeDto
> {
  @Column()
  userId: string;

  @Column()
  verificationCode: number;

  @Column()
  isValid: boolean;

  dtoClass = VerificationCodeDto;
}
