/* eslint-disable quote-props */
import {
  BadRequestException,
  HttpService,
  Injectable,
} from '@nestjs/common';

import {
  GenderEnum,
  NotificationEnum,
  StatusFriendRequestEnum,
  TypeUploadEnum,
  UserRoleEnum,
} from '../../common/enum/enum';
import { UserEntity } from '../../entities/user.entity';
import { UserNotFoundException } from '../../exceptions/user-not-found.exception';
import { NotificationService } from '../../modules/notification/notification.service';
import { UtilsService } from '../../providers/utils.service';
import { UserRepository } from '../../repositories/user.repository';
import { UserPreferencesRepository } from '../../repositories/userPreferences.repository';
import { UserWeightRepository } from '../../repositories/userWeight.repository';
import { AwsS3Service } from '../../shared/services/aws-s3.service';
import { ConfigService } from '../../shared/services/config.service';
import { MailGunService } from '../../shared/services/mailgun.service';
import { SlackService } from '../../shared/services/slack.service';
import { Language } from '../../shared/services/translation.service';
import { calculateAge } from '../../utils/date';
import { FriendService } from '../friend/friend.service';
import { ChangePasswordDto } from './dto/changePassword.dto';
import { DeleteAccountByIdDto } from './dto/deleteAccountById.dto';
import { UpdateOneSignalPlayerDto } from './dto/updateOneSignalPlayer.dto';
import { UserOnBoardingDto } from './dto/userOnBoarding.dto';
import { UserPreferencesRequestDto } from './dto/userPreferencesRequest.dto';

@Injectable()
export class AccountService {
  constructor(
    private readonly _awsS3Service: AwsS3Service,
    private readonly _userRepository: UserRepository,
    private readonly _userWeightRepository: UserWeightRepository,
    private readonly _httpServices: HttpService,
    private readonly _notificationService: NotificationService,
    private readonly _userPreferencesRepository: UserPreferencesRepository,
    private readonly _friendService: FriendService,
    private readonly _configService: ConfigService,
    private readonly _mailgunService: MailGunService,
    private readonly _slackService: SlackService,
  ) {}

  async changePassword(
    changePasswordDto: ChangePasswordDto,
    user: UserEntity,
  ) {
    const isPasswordValid = await UtilsService.validateHash(
      changePasswordDto.oldPassword,
      user && user.password,
    );
    if (isPasswordValid) {
      user.password = UtilsService.generateHash(
        changePasswordDto.newPassword,
      );
      return this._userRepository.save(user);
    }
    throw new BadRequestException('Incorrect old password');
  }

  async updateProfilePicture(userId: string, fileName: string) {
    const url = `profile/${fileName}`;
    await this._userRepository
      .createQueryBuilder('user')
      .update()
      .set({ photoId: url })
      .where('id = :userID', {
        userID: userId,
      })
      .execute();
    return { url };
  }

  async updateProfilePictureFromUrl(
    user: UserEntity,
    photoUrl: string,
  ) {
    const photoData = await this._httpServices.axiosRef({
      url: photoUrl,
      method: 'GET',
      responseType: 'arraybuffer',
    });

    const photo = {
      buffer: photoData.data,
      mimetype: 'image/png',
      encoding: null,
      fieldname: null,
      originalname: null,
      size: null,
    };

    user.photoId = await this._awsS3Service.uploadImage(
      photo,
      TypeUploadEnum.PROFILE,
    );
    return this._userRepository.save(user);
  }

  private _getTargetLooseWeight(
    originalWeight: number,
    targetWeight: number,
  ) {
    return Number((originalWeight - targetWeight).toFixed(1));
  }

  private async _saveWeight(originalWeight: number, userId: string) {
    const userWeightRepository = this._userWeightRepository.create();

    userWeightRepository.user = <any>userId;
    userWeightRepository.weight = originalWeight;

    return this._userWeightRepository.save(userWeightRepository);
  }

  private async _saveLanguage(user: UserEntity, language: Language) {
    let userPreferencesRepository = await this._userPreferencesRepository.findOne(
      { where: { user } },
    );

    if (!userPreferencesRepository) {
      userPreferencesRepository = this._userPreferencesRepository.create();
      userPreferencesRepository.user = user;
    }

    userPreferencesRepository.language = language;

    return this._userPreferencesRepository.save(
      userPreferencesRepository,
    );
  }

  public async userAfterOnboarding(user: UserEntity) {
    try {
      await this._notificationService.addNotification({
        type: NotificationEnum.WELCOME,
        userId: user.id,
      });
      this._slackNotifyUserCreation(user);

      if (user.email) {
        const userPreferencesRepository = await this._userPreferencesRepository.findOne(
          { where: { user } },
        );
        const language =
          userPreferencesRepository &&
          userPreferencesRepository.language;
        this._sendWelcomeEmail(user.email, language);

        const mailOptions = {
          members: [{ address: user.email, subscribed: true }],
        };
        await this._mailgunService.addMailToMailingList(mailOptions);
      }

      await this._addCoach(user.id);

      return true;
    } catch (error) {
      return false;
    }
  }

  public async userOnboarding(
    userOnBoardingDto: UserOnBoardingDto,
    user: UserEntity,
  ) {
    const {
      pseudo,
      gender,
      originalWeight,
      targetWeight,
      birthDate,
      language,
      height,
    } = userOnBoardingDto;
    const targetLooseWeight = this._getTargetLooseWeight(
      userOnBoardingDto.originalWeight,
      userOnBoardingDto.targetWeight,
    );

    try {
      const userOnboardingPayload = {
        pseudo,
        birthDate,
        gender,
        targetLooseWeight,
        height: Math.round(height),
        targetWeight: Math.round(targetWeight * 100) / 100,
        originalWeight: Math.round(originalWeight * 100) / 100,
        id: user.id,
        profileCompleted: true,
      };

      const [updatedUser, pref] = await Promise.all([
        this._userRepository.save(userOnboardingPayload),
        this._saveLanguage(user, language),
        this._saveWeight(userOnBoardingDto.originalWeight, user.id),
      ]);

      return {
        ...updatedUser,
        userPreferences: pref,
      };
    } catch (error) {
      return error;
    }
  }

  async updateUser(
    userOnBoardingDto: UserOnBoardingDto,
    user: UserEntity,
    isOnBoarding = false,
  ) {
    const {
      pseudo,
      originalWeight,
      targetWeight,
      birthDate,
      height,
      gender,
      language,
    } = userOnBoardingDto;

    if (user.pseudo && user.pseudo !== pseudo) {
      const pseudoDuplicate = await this._userRepository.findOne({
        pseudo,
      });
      if (pseudoDuplicate) {
        throw new BadRequestException(
          'user with that pseudo already exist',
        );
      }
    }

    const targetLooseWeight = this._getTargetLooseWeight(
      userOnBoardingDto.originalWeight,
      userOnBoardingDto.targetWeight,
    );

    const updatedUser = await this._userRepository.save({
      pseudo,
      birthDate,
      gender,
      targetLooseWeight,
      height: Math.round(height),
      targetWeight: Math.round(targetWeight * 100) / 100,
      originalWeight: Math.round(originalWeight * 100) / 100,
      id: user.id,
      profileCompleted: true,
    });

    if (isOnBoarding) {
      await this._saveWeight(
        userOnBoardingDto.originalWeight,
        user.id,
      );
      await this._saveLanguage(updatedUser, language);

      try {
        this._sendWelcomeEmail(updatedUser.email, language);
        this._slackNotifyUserCreation(updatedUser);

        await this._addCoach(updatedUser.id);
      } catch (err) {
        // console.log(err);
      }
      try {
        await this._notificationService.addNotification({
          type: NotificationEnum.WELCOME,
          userId: user.id,
        });

        if (updatedUser.email) {
          await this._mailgunService.addMailToMailingList({
            members: [
              { address: updatedUser.email, subscribed: true },
            ],
          });
        }
      } catch (err) {
        // console.log(err);
      }
    }

    return {
      ...updatedUser,
      language,
    };
  }

  private async _slackNotifyUserCreation(user: UserEntity) {
    const genderEmoji = user.gender === GenderEnum.MALE ? '👨' : '👩';
    const age = calculateAge(new Date(user.birthDate).getTime());

    await this._slackService.sendToSlack(
      `${genderEmoji} ${user.pseudo} has just joined Fatster 🚀🔥🔥🔥🔥🔥.
I'm ${age} years old
My weight is ${user.originalWeight}kg
My target weight is ${user.targetWeight}kg
Id : ${user.id}
Email : ${user.email}`,
    );
  }

  async updateOneSignalPlayerId(
    updateOneSignalPlayerDto: UpdateOneSignalPlayerDto,
  ) {
    return this._userRepository.update(
      updateOneSignalPlayerDto.userId,
      {
        oneSignalPlayerId: updateOneSignalPlayerDto.oneSignalPlayerId,
      },
    );
  }

  async deleteAccount(user: UserEntity) {
    const userToDelete = await this._userRepository.findOne(user.id);
    if (!userToDelete) {
      throw new UserNotFoundException();
    }
    return this._userRepository.remove(userToDelete);
  }

  async deleteAccountById(
    { id }: DeleteAccountByIdDto,
    user: UserEntity,
  ) {
    const isAdmin = user.roles.includes(UserRoleEnum.ADMIN);

    if (!isAdmin) {
      throw new BadRequestException(
        `Unauthorized action : you don't have permission`,
      );
    }

    const userToDelete = await this._userRepository.findOne(id);
    if (!userToDelete) {
      throw new UserNotFoundException();
    }

    const deletedUser = await this._userRepository.remove(
      userToDelete,
    );

    if (isAdmin) {
      const env = this._configService.nodeEnv;

      const deletionTime = new Date().toLocaleDateString('fr', {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
      });
      this._slackService
        .sendToContactChannel(
          `
        ${deletionTime} on nodeEnv - ${env}
        Admin - ${user.pseudo} had deleted
        User with pseudo - ${userToDelete.pseudo}
        and email - ${userToDelete.email}`,
        )
        .then();
    }

    return deletedUser;
  }

  async getPreferences(userId: string) {
    return this._userPreferencesRepository.findOne({
      where: { user: userId },
    });
  }

  async editPreferences(
    user: UserEntity,
    request: UserPreferencesRequestDto,
  ) {
    const userPreferences = await this._userPreferencesRepository
      .createQueryBuilder('userPreferences')
      .where('userPreferences.user = :id', { id: user.id })
      .getOne();

    if (!userPreferences) {
      throw new UserNotFoundException();
    }
    // MY INFORMATION
    userPreferences.showStatistics = request.showStatistics;
    userPreferences.showFeed = request.showFeed;
    userPreferences.showPostsInPublicFeed =
      request.showPostsInPublicFeed;
    userPreferences.showPseudoInSearch = request.showPseudoInSearch;
    userPreferences.showPostsAppLanguageOnly =
      request.showPostsAppLanguageOnly;
    userPreferences.showProfile = request.showProfile;
    userPreferences.language = request.language;

    // PUSH NOTIFICATIONS
    userPreferences.pushNotificationLikes =
      request.pushNotificationLikes;
    userPreferences.pushNotificationComments =
      request.pushNotificationComments;
    userPreferences.pushNotificationFriendRequest =
      request.pushNotificationFriendRequest;
    userPreferences.pushNotificationNewFriend =
      request.pushNotificationNewFriend;
    userPreferences.pushNotificationAdminMessages =
      request.pushNotificationAdminMessages;
    userPreferences.pushNotificationMentionMessages =
      request.pushNotificationMentionMessages;

    // EMAIL
    userPreferences.newsLetter = request.newsLetter;
    userPreferences.adminEmails = request.adminEmails;

    return this._userPreferencesRepository.save(userPreferences);
  }

  /*async coachOldUser() {
    const users = await this._userRepository.find({
      order: { createdAt: 'ASC' },
    });
    users.map(async (user) => {
      const friend = await this._friendRepository.count({
        where: {
          user: user.id,
          friend: '0d8887b3-2359-4bf2-adbf-3ac17d9dc488',
        },
      });
      if (friend === 0) {
        // console.log('NEED TO BE ADDED');
        try {
          await this._addCoach(user.id);
          // eslint-disable-next-line no-empty
        } catch (err) {}
      }
    });
    return;
  }*/

  private async _addCoach(userId: string) {
    const friendId =
      this._configService.nodeEnv === 'prod'
        ? '0d8887b3-2359-4bf2-adbf-3ac17d9dc488'
        : '362e6ed9-7091-4304-97a5-37f1eddf5b79'; // bichvld@gmail.com user can be replaced with any EXISTED user

    const friendRequest = await this._friendService.sendFriendRequest(
      userId,
      friendId,
      false,
    );

    await this._friendService.processFriendRequest(
      {
        userId,
        status: StatusFriendRequestEnum.ACCEPTED,
        friendRequestId: friendRequest.id,
      },
      false,
    );
  }

  private async _sendWelcomeEmail(email, language: string) {
    if (language.toLowerCase() === 'fr') {
      return this._mailgunService.sendEmail({
        subject: 'Bienvenue',
        from: `Fatster <hi@fatster.app>`,
        to: email,
        template: 'welcome',
      });
    }
    return this._mailgunService.sendEmail({
      subject: 'Welcome',
      from: `Fatster <hi@fatster.app>`,
      to: email,
      template: 'welcome_en',
    });
  }
}
