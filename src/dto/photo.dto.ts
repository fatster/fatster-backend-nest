import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { PhotoEntity } from '../entities/photo.entity';
import { PostDto } from './post.dto';

export class PhotoDto extends AbstractDto {
  @ApiPropertyOptional()
  path: string;

  @ApiPropertyOptional()
  post: PostDto;

  constructor(photo: PhotoEntity) {
    super(photo);
    this.path = photo.path;
  }
}
