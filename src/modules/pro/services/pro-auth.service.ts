import { BadRequestException, Injectable } from '@nestjs/common';

import { AuthMethodEnum, BadgeEnum } from '../../../common/enum/enum';
import { BadgeService } from '../../../modules/badge/badge.service';
import { UtilsService } from '../../../providers/utils.service';
import { UserRepository } from '../../../repositories/user.repository';
import { ProLoginDto } from '../dto/pro-login.dto';
import { ProRegisterDto } from '../dto/pro-register.dto';
import { ProService } from './pro.service';

@Injectable()
export class ProAuthService {
  constructor(
    private readonly _userRepository: UserRepository,
    private readonly _badgeService: BadgeService,
    private readonly _proService: ProService,
  ) {}

  public async loginPro({ email, password }: ProLoginDto) {
    const user = await this._userRepository.findOne({
      where: { email },
      relations: ['badges'],
    });

    if (!user) {
      throw new BadRequestException('login_error');
    }

    const isPasswordValid = await UtilsService.validateHash(
      password,
      user.password,
    );

    if (!isPasswordValid) {
      throw new BadRequestException('login_error');
    }

    const isPro = user.badges.find(({ nameEn }) => nameEn === 'Pro');
    if (isPro) {
      return this._proService.findProByIdWithRelations(user.id);
    }

    throw new BadRequestException('user_is_not_a_pro');
  }

  public async registerPro(dto: ProRegisterDto) {
    const { email, password, phone, firstName, lastName } = dto;

    const existingUser = await this._userRepository.findOne({
      email,
    });

    if (existingUser) {
      throw new BadRequestException('email_or_pseudo_already_exist');
    }

    const hashedPassword = UtilsService.generateHash(password);

    const proDetails = {
      email,
      phone,
      password: hashedPassword,
      name: firstName,
      surname: lastName,
      authMethod: AuthMethodEnum.EMAIL,
    };

    const user = await this._proService.saveProByIdWithRelations({
      proDetails,
    });

    await this._badgeService.addBadgeToUser(user.id, BadgeEnum.PRO);

    return user;
  }
}
