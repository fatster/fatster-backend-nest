import { ApiProperty } from '@nestjs/swagger';
import {
  IsIn,
  IsNotEmpty,
  IsNumber,
  IsString,
} from 'class-validator';

import { GenderEnum } from '../../../common/enum/enum';
import { Language } from '../../../shared/services/translation.service';

export class UserOnBoardingDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly pseudo: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly language: Language;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty()
  readonly originalWeight: number;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty()
  readonly targetWeight: number;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty()
  readonly height: number;

  @IsString()
  @IsNotEmpty()
  @IsIn(Object.values(GenderEnum))
  @ApiProperty()
  readonly gender: GenderEnum;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly birthDate: string;
}
