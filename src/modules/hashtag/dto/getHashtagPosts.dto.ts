import { ApiHideProperty } from '@nestjs/swagger';

import { PageOptionsDto } from '../../../common/dto/PageOptionsDto';
import { UserEntity } from '../../../entities/user.entity';

export class GetHashtagPostsDto extends PageOptionsDto {
  @ApiHideProperty()
  hashtagText: string;

  @ApiHideProperty()
  user: UserEntity;
}
