import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { CoachCategoryEntity } from '../entities/coachCategory.entity';

@EntityRepository(CoachCategoryEntity)
export class CoachCategoryRepository extends Repository<
  CoachCategoryEntity
> {}
