import { last, sortBy } from 'lodash';
import {
  getRepository,
  In,
  ObjectLiteral,
  Repository,
  SelectQueryBuilder,
} from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { Order } from '../common/constants/order';
import { minutesToHoursFeed } from '../common/utils/utils';
import { FriendRequestEntity } from '../entities/friendRequest.entity';
import { PostEntity } from '../entities/post.entity';
import { UserEntity } from '../entities/user.entity';
import { Language } from '../shared/services/translation.service';
import { addDays, isDateBeforeDate } from '../utils/date';

@EntityRepository(PostEntity)
export class PostRepository extends Repository<PostEntity> {
  public async countLikedReceived(id: string, from?) {
    const query = this.createQueryBuilder('post')
      .select('COUNT(likes.id) as likes')
      .leftJoin('post.likes', 'likes')
      .where('post.user = :id', { id });
    if (from) {
      query.andWhere('post.createdAt > :from', {
        from,
      });
    }
    return query.getRawOne();
  }

  public async getPostsByIds(ids: string[]) {
    return this.createQueryBuilder('post')
      .whereInIds(ids)
      .select(['post', 'user', 'nutrition'])
      .leftJoin('post.user', 'user')
      .leftJoin(
        'post.userNutrition',
        'nutrition',
        `post.type = 'food'`,
      )
      .getMany();
  }

  public async getPostsWithRelations(
    user: UserEntity,
    userLang: Language | null,
    options?: {
      take?: number;
      skip?: number;
      where?: {
        where: Parameters<SelectQueryBuilder<PostEntity>['where']>[0];
        parameters?: Parameters<
          SelectQueryBuilder<PostEntity>['where']
        >[1];
      };
      order?: Order;
      addFriendRequests?: boolean;
      rank?: boolean;
      parameters?: ObjectLiteral;
    },
  ) {
    const postsQuery: any = this.createQueryBuilder('posts')
      .leftJoinAndSelect('posts.userActivity', 'userActivity')
      .leftJoinAndSelect('posts.userWeight', 'userWeight')
      .leftJoinAndSelect('posts.userNutrition', 'userNutrition')
      .leftJoinAndSelect('posts.beforeAfter', 'beforeAfter')
      .leftJoinAndSelect('userActivity.activity', 'activity')
      .leftJoinAndSelect('userNutrition.nutritionFood', 'foods')
      .leftJoinAndSelect('foods.food', 'foodsData')
      .leftJoinAndSelect('posts.tags', 'tags')
      .leftJoinAndSelect('posts.article', 'article')
      .leftJoinAndSelect('tags.badges', 'tags.badges')
      .leftJoinAndSelect('posts.user', 'user')
      .leftJoinAndSelect('user.badges', 'user.badges')
      .leftJoinAndSelect('user.userWeights', 'weight')
      .leftJoinAndSelect('user.userPreferences', 'userPreferences')
      .leftJoinAndSelect('posts.likes', 'likes')
      .leftJoinAndSelect('posts.photos', 'photos')
      .leftJoinAndSelect('posts.comments', 'comments')
      .orderBy('posts.createdAt', options.order)
      .take(options.take)
      .skip(options.skip);

    if (options.where) {
      postsQuery.where(options.where.where, options.where.parameters);
      if (userLang) {
        postsQuery.andWhere(
          `userPreferences.language = '${userLang}'`,
        );
      }
    } else if (userLang) {
      postsQuery.where(`userPreferences.language = '${userLang}'`);
    }

    //  IN CASE OFF ATTACK
    // if (options.where) {
    //   posts = posts
    //     .where(options.where.where, options.where.parameters)
    //     .andWhere(`user.createdAt + INTERVAL '7 days' < NOW()`);
    // } else {
    //   posts = posts.where(
    //     `user.createdAt + INTERVAL '7 days' < NOW()`,
    //   );
    // }

    const posts = await postsQuery.getMany();

    let friendRequests: FriendRequestEntity[];

    if (posts.length === 0) {
      return [];
    }

    if (options.addFriendRequests) {
      const userIds = posts.map((post) => post.user.id);
      friendRequests = await getRepository(FriendRequestEntity)
        .createQueryBuilder('friendRequests')
        .where([
          { from: user, to: In(userIds) },
          { to: user, from: In(userIds) },
        ])
        .leftJoinAndSelect('friendRequests.from', 'from')
        .leftJoinAndSelect('friendRequests.to', 'to')
        .getMany();
    }

    return posts
      .map((post) =>
        this._formatPost(
          post,
          user,
          friendRequests || null,
          options.rank,
        ),
      )
      .sort((a, b) => {
        if (options.rank) {
          return options.order === 'DESC'
            ? b.rank - a.rank
            : a.rank - b.rank;
        }
        return options.order === 'DESC'
          ? b.createdAt.getTime() - a.createdAt.getTime()
          : a.createdAt.getTime() - b.createdAt.getTime();
      });
  }

  private _formatPost(
    post: PostEntity,
    user: UserEntity,
    friendRequests?: FriendRequestEntity[],
    rank?: boolean,
  ) {
    const postDto = post.toDto();
    const expireNewUserHashtagDate = addDays(
      postDto.user.createdAt,
      2,
    );
    const postCreatedDate = post.createdAt || new Date();

    postDto.tags = postDto.tags.map((tag) => ({
      pseudo: tag.pseudo,
      photoId: tag.photoId,
      gender: tag.gender,
      badges: tag.badges,
    }));
    const weightPost = last(
      sortBy(postDto.user.userWeights || [], 'createdAt'),
    );
    postDto.user = {
      userId: postDto.user.id,
      pseudo: postDto.user.pseudo,
      photoId: postDto.user.photoId,
      gender: postDto.user.gender,
      badges: postDto.user.badges,
      originalWeight: postDto.user.originalWeight,
      targetWeight: postDto.user.targetWeight,
      currentWeight:
        (weightPost && weightPost.weight) ||
        postDto.user.originalWeight,
    };

    if (
      postDto.userNutrition &&
      postDto.userNutrition.foods.length > 0
    ) {
      postDto.userNutrition.foods = postDto.userNutrition.foods.map(
        (item) => ({
          portionType: item.portionType,
          grams: item.grams,
          calories: item.calories,
          portion: item.portion,
          id: item.id,
          textEn: item.food.textEn,
          textFr: item.food.textFr,
          energyEuRegulation: item.food.energyEuRegulation,
        }),
      );
    }
    if (postDto.userActivity) {
      if (postDto.userActivity.duration < 60) {
        postDto.userActivity.duration =
          postDto.userActivity.duration + 'min';
      } else {
        postDto.userActivity.duration = minutesToHoursFeed(
          postDto.userActivity.duration,
        );
      }
    }

    if (postDto.userWeight) {
      postDto.userWeight.weightDifference =
        postDto.userWeight.weightDifference > 0
          ? `+${postDto.userWeight.weightDifference}`
          : `${postDto.userWeight.weightDifference}`;
    }

    if (friendRequests && postDto.user.userId !== user.id) {
      const friendRequest = friendRequests.find(
        (el) =>
          el.from.id === postDto.user.userId ||
          el.to.id === postDto.user.userId,
      );

      postDto.user.friendRequest = { status: 'not invited' };
      if (friendRequest) {
        postDto.user.friendRequest = {
          status: friendRequest.status,
          id: friendRequest.id,
          isOut: friendRequest.from.id === user.id,
        };
      }
    }

    postDto.isLiked = user
      ? Boolean(postDto.likes.find((like) => like.userId === user.id))
      : false;

    if (rank) {
      postDto.rank = 0;
      const timeSinceCreate = Math.abs(
        (new Date().getTime() - post.createdAt.getTime()) /
          (60 * 60 * 1000),
      );
      const timeSinceUpdate = Math.abs(
        (new Date().getTime() - post.createdAt.getTime()) /
          (60 * 60 * 1000),
      );
      postDto.rank +=
        (0.75 + 0.5 * post.commentsCount + post.likesCount) /
        (1 +
          Math.pow(timeSinceCreate, 1.8) -
          Math.pow(timeSinceCreate - timeSinceUpdate, 1.2));
    }

    if (postDto.hashtags) {
      postDto.hashtags = postDto.hashtags
        .map((hashTag) => {
          if (
            hashTag === 'newuser' &&
            !isDateBeforeDate(
              postCreatedDate,
              expireNewUserHashtagDate,
            )
          ) {
            return null;
          }
          return hashTag;
        })
        .filter(Boolean);
    }

    if (postDto.comments && postDto.comments.length) {
      postDto.likesCount = postDto.comments.reduce(
        (acc, c) => acc + c.likesCount,
        postDto.likesCount,
      );
    }

    delete postDto.likes;
    return postDto;
  }
}
