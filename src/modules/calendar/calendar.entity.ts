import {
  Column,
  Entity,
  Index,
  JoinTable,
  ManyToMany,
} from 'typeorm';

import { AbstractEntity } from '../../common/abstract.entity';
import { UserEntity } from '../../entities/user.entity';
import { CalendarDto } from './calendar.dto';
import { CalendarEventType } from './calendar.types';

@Entity({ name: 'calendar' })
export class CalendarEntity extends AbstractEntity<CalendarDto> {
  @Column({ type: 'timestamp without time zone', nullable: false })
  start: Date;

  @Column({ type: 'timestamp without time zone', nullable: false })
  end: Date;

  @Column({ type: 'text', nullable: false })
  title: string;

  @Column({ type: 'uuid', nullable: false })
  @Index()
  ownerId: string;

  @Column('enum', {
    enum: Object.values(CalendarEventType),
    nullable: false,
  })
  type: CalendarEventType;

  @Column({ type: 'text', nullable: false })
  meetingUrl: string;

  @ManyToMany(() => UserEntity, (user) => user.events, {
    onDelete: 'CASCADE',
  })
  @JoinTable()
  participants: UserEntity[];

  dtoClass = CalendarDto;
}
