import { Column, Entity } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { PlatformEnum } from '../common/enum/enum';
import { AppVersionDto } from '../dto/appVersion.dto';

@Entity('app_version')
export class AppVersionEntity extends AbstractEntity<AppVersionDto> {
  @Column({
    type: 'enum',
    enum: PlatformEnum,
  })
  platform: PlatformEnum;

  @Column()
  version: string;

  @Column()
  mandatory: boolean;

  dtoClass = AppVersionDto;
}
