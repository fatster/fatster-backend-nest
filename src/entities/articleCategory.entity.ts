import { IsString } from 'class-validator';
import { Column, Entity, OneToMany } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { ArticleCategoryDto } from '../dto/articleCategory.dto';
import { ArticleEntity } from './article.entity';
// import { ArticleCategoryTypeEnum } from '../common/enum/enum';

@Entity({ name: 'article_category' })
export class ArticleCategoryEntity extends AbstractEntity<
  ArticleCategoryDto
> {
  @Column()
  @IsString()
  typeEn: string;

  @Column()
  @IsString()
  typeFr: string;

  @OneToMany(
    () => ArticleEntity,
    (article) => article.articleCategory,
    {
      onDelete: 'CASCADE',
    },
  )
  article: ArticleEntity[];

  /*@Column({
    type: 'enum',
    enum: ArticleCategoryTypeEnum,
  })
  type?: ArticleCategoryTypeEnum;*/

  dtoClass = ArticleCategoryDto;
}
