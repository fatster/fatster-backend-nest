import { IsString } from 'class-validator';
import { Column, Entity, OneToMany } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { ActivityCategoryDto } from '../dto/activityCategory.dto';
import { ActivityEntity } from './activity.entity';

@Entity({ name: 'activity_category' })
export class ActivityCategoryEntity extends AbstractEntity<
  ActivityCategoryDto
> {
  @Column()
  @IsString()
  textEn: string;

  @Column()
  @IsString()
  textFr: string;

  @OneToMany(
    () => ActivityEntity,
    (activity) => activity.activityCategory,
    { onDelete: 'CASCADE' },
  )
  activities: ActivityEntity[];

  dtoClass = ActivityCategoryDto;
}
