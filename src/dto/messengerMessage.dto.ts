import { AbstractDto } from '../common/dto/AbstractDto';
import { MessengerRoomDto } from './messengerRoom.dto';

export class MessengerMessageDto extends AbstractDto {
  text: string;

  messengerRoom: MessengerRoomDto;
}
