'use strict';

export class DeleteNotificationDto {
  notificationId: string;

  userId: string;
}
