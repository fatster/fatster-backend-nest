import {
  HealthyTypeEnum,
  PostStatusEnum,
  PostTypeEnum,
  TimeOfTheDayEnum,
} from '../common/enum/enum';
import { NutritionFood } from './NutritionFood';

export interface ISimplePost {
  type: PostTypeEnum.SIMPLE;
  text: string;
  friendsToTag: string[];
  photos: string[];
}

export interface IActivityPost {
  type: PostTypeEnum.ACTIVITY;
  text: string;
  activityId: string;
  duration: number;
  friendsToTag: string[];
  photos: string[];
}

export interface INutritionPost {
  type: PostTypeEnum.FOOD;
  status: PostStatusEnum;
  text: string;
  healthyType?: HealthyTypeEnum;
  timeOfTheDay: TimeOfTheDayEnum;
  friendsToTag: string[];
  photos: string[];
  nutritionDate: Date;
  foods: NutritionFood[];
}

export interface IWeightPost {
  type: PostTypeEnum.WEIGHT;
  weight: number;
  postOnFeed: boolean;
  text: string;
  friendsToTag: string[];
  photos: string[];
}

export interface IBeforeAfterPost {
  type: PostTypeEnum.BEFORE_AFTER;
  text?: string;
  photoBefore?: string;
  photoAfter?: string;
  weightBefore?: number;
  weightAfter?: number;
}

export interface ISharedPost {
  type: PostTypeEnum.SHARED_ARTICLE;
  articleId: string;
}

export type Post =
  | ISimplePost
  | IActivityPost
  | INutritionPost
  | IWeightPost
  | IBeforeAfterPost
  | ISharedPost;
