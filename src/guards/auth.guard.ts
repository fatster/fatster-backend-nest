import { AuthGuard as NestAuthGuard } from '@nestjs/passport';

export const AuthGuard = NestAuthGuard('jwt');
export const AuthGuardFacebook = NestAuthGuard('facebook-token');
export const AuthGuardApple = NestAuthGuard('apple');
