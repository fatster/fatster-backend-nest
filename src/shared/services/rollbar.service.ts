import { Injectable } from '@nestjs/common';
import * as rollbar from 'rollbar';

import { ConfigService } from './config.service';

@Injectable()
export class RollbarService {
  private readonly _rl;
  constructor(public configService: ConfigService) {
    this._rl = new rollbar({
      accessToken: this.configService.get('ROLLBAR_TOKEN'),
      // captureUncaught: true,
      // captureUnhandledRejections: true,
    });
  }

  async sendLog(error, request) {
    return this._rl.error(error, request);
  }
}
