import { Injectable } from '@nestjs/common';

import { PlatformEnum } from '../../common/enum/enum';
import { ContactUsEntity } from '../../entities/contactUs.entity';
import { UserEntity } from '../../entities/user.entity';
import { AppVersionRepository } from '../../repositories/appVersion.repository';
import { ContactUsRepository } from '../../repositories/contactUs.repository';
import { NextFeaturesRepository } from '../../repositories/nextFeatures.repository';
import { UserRepository } from '../../repositories/user.repository';
import { AwsS3Service } from '../../shared/services/aws-s3.service';
import { MailGunService } from '../../shared/services/mailgun.service';
import { SlackService } from '../../shared/services/slack.service';
import { FoodSuggestionDto } from './dto/foodSuggestion.dto';

@Injectable()
export class CommonService {
  constructor(
    private readonly _awsS3Service: AwsS3Service,
    private readonly _contactUsRepository: ContactUsRepository,
    private readonly _nextFeaturesRepository: NextFeaturesRepository,
    private readonly _userRepository: UserRepository,
    private readonly _appVersionRepository: AppVersionRepository,
    private readonly _slackService: SlackService,
    private readonly _mailgunService: MailGunService,
  ) {}

  async getSignedUrl(contentType, type) {
    return this._awsS3Service.getSignedUrl(contentType, type);
  }

  async sendContactMessage(
    user: UserEntity,
    subject: string,
    text: string,
  ) {
    const message = new ContactUsEntity();
    message.userEmail = user.email;
    message.subject = subject;
    message.text = text;

    this._mailgunService.sendEmail({
      subject,
      from: user.email,
      to: 'support@fatster.freshdesk.com',
      text: `Pseudo: ${user.pseudo}
Message: ${text}`,
    });

    await this._slackService.sendToContactChannel(
      `Pseudo: ${user.pseudo}
Email: ${user.email}
Subject: ${message.subject}
Message: ${message.text}`,
    );

    return this._contactUsRepository.save(message);
  }

  async addSuggestedFood(
    foodSuggestion: FoodSuggestionDto,
    user: UserEntity,
  ) {
    return this._slackService.sendToSlackFoodChannel(
      `(${user.pseudo}) 🧀 🥩 *${foodSuggestion.food}*`,
    );
  }

  async isPseudoAvailable(pseudo: string) {
    const user = await this._userRepository
      .createQueryBuilder('users')
      .where('LOWER(users.pseudo) = LOWER(:pseudo)', { pseudo })
      .getOne();
    if (user) {
      return false;
    }
    return true;
  }

  async getFeature(featureName: string) {
    const feature = await this._nextFeaturesRepository.findOne({
      name: featureName,
    });
    return {
      name: feature.name,
      // likesCount: this._kFormatter(feature.likesCount),
      likesCount: feature.likesCount,
    };
  }

  async likeFeature(name: string) {
    const feature = await this._nextFeaturesRepository.findOne({
      name,
    });
    feature.likesCount += 1;
    await this._nextFeaturesRepository.save(feature);
    return {
      name: feature.name,
      // likesCount: this._kFormatter(feature.likesCount),
      likesCount: feature.likesCount,
    };
  }

  async getAppVersion(platform: PlatformEnum) {
    return this._appVersionRepository.findLastOne(platform);
  }

  private _kFormatter(num): string | number {
    return Math.abs(num) > 999
      ? Math.sign(num) * Number((Math.abs(num) / 1000).toFixed(1)) +
          'k'
      : Math.sign(num) * Math.abs(num);
  }
}
