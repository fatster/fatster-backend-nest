import { IsString } from 'class-validator';
import {
  AfterInsert,
  AfterRemove,
  Column,
  Entity,
  getConnection,
  ManyToOne,
  OneToMany,
} from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { CommentDto } from '../dto/comment.dto';
import { LikeEntity } from './like.entity';
import { NotificationEntity } from './notification.entity';
import { PostEntity } from './post.entity';
import { UserEntity } from './user.entity';

@Entity({ name: 'comment' })
export class CommentEntity extends AbstractEntity<CommentDto> {
  @Column()
  @IsString()
  text: string;

  @ManyToOne(() => PostEntity, (post) => post.comments, {
    onDelete: 'CASCADE',
  })
  post: PostEntity;

  @Column({ nullable: false, name: 'post_id' })
  postId: string;

  @ManyToOne(() => UserEntity, (user) => user.comments, {
    onDelete: 'CASCADE',
  })
  user: UserEntity;

  @Column({ nullable: false, name: 'user_id' })
  userId: string;

  @ManyToOne(() => CommentEntity, (comment) => comment.children, {
    nullable: true,
    onDelete: 'CASCADE',
  })
  parent: CommentEntity;

  @OneToMany(() => CommentEntity, (comment) => comment.parent, {
    nullable: true,
  })
  children: CommentEntity[];

  @OneToMany(
    () => NotificationEntity,
    (notification) => notification.comment,
    {
      onDelete: 'CASCADE',
    },
  )
  notifications?: NotificationEntity[];

  @OneToMany(() => LikeEntity, (like) => like.comment, {
    onDelete: 'CASCADE',
  })
  likes: LikeEntity[];

  @Column({
    default: 0,
    name: 'likes_count',
  })
  likesCount?: number;

  @Column({ type: 'text', array: true, nullable: true })
  mentionedUsersPseudo?: string[];

  @AfterInsert()
  async increaseCommentsCounter() {
    // To avoid updated_at to be changed
    await getConnection().query(
      'UPDATE "post" SET "comments_count" = comments_count + 1 WHERE "id" = $1',
      [this.postId],
    );
  }

  @AfterRemove()
  async decreaseCommentsCounter() {
    // To avoid updated_at to be changed
    await getConnection().query(
      'UPDATE "post" SET "comments_count" = comments_count - 1 WHERE "id" = $1',
      [this.postId],
    );
  }
  dtoClass = CommentDto;
}
