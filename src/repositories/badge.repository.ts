import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { BadgeEntity } from '../entities/badge.entity';

@EntityRepository(BadgeEntity)
export class BadgeRepository extends Repository<BadgeEntity> {}
