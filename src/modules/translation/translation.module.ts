import { Module } from '@nestjs/common';

import { TranslationController } from './translation.controller';
import { TranslationEndpointService } from './translation.service';

@Module({
  controllers: [TranslationController],
  exports: [TranslationEndpointService],
  providers: [TranslationEndpointService],
})
export class TranslationModule {}
