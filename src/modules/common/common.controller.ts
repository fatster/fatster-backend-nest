import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';

import { PayloadSuccessDto } from '../../common/dto/PayloadSuccessDto';
import { PlatformEnum } from '../../common/enum/enum';
import { AuthUser } from '../../decorators/auth-user.decorator';
import { UserEntity } from '../../entities/user.entity';
import { AuthGuard } from '../../guards/auth.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
import { CommonService } from './common.service';
import { ContactUsMessageDto } from './dto/contactUsMessage.dto';
import { FoodSuggestionDto } from './dto/foodSuggestion.dto';
import { SignedUrlRequestDto } from './dto/signedUrlRequest.dto';

@Controller('common')
@ApiTags('Common')
export class CommonController {
  constructor(private _commonService: CommonService) {}

  @Get('signedUrl')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async getSignedUrl(
    @Query()
    query: SignedUrlRequestDto,
  ): Promise<PayloadSuccessDto> {
    const signedUrl = await this._commonService.getSignedUrl(
      query.mimeType,
      query.type,
    );
    return { message: 'signed url', data: signedUrl };
  }

  @Post('foodSuggestion')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'sends message to fatster team' })
  async foodSuggestion(
    @AuthUser() user: UserEntity,
    @Body() foodSuggestionDto: FoodSuggestionDto,
  ): Promise<PayloadSuccessDto> {
    const response = await this._commonService.addSuggestedFood(
      foodSuggestionDto,
      user,
    );

    return { message: 'message send', data: response };
  }

  @Post('contactUs')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'sends message to fatster team' })
  async sendContactMessage(
    @AuthUser() user: UserEntity,
    @Body() contactUsMessageDto: ContactUsMessageDto,
  ): Promise<PayloadSuccessDto> {
    const response = await this._commonService.sendContactMessage(
      user,
      contactUsMessageDto.subject,
      contactUsMessageDto.text,
    );
    return { message: 'message send', data: response };
  }

  @Get('feature/:name')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Get a feature by name' })
  @ApiParam({ name: 'name', type: String })
  async getFeature(
    @Param('name') name: string,
  ): Promise<PayloadSuccessDto> {
    const response = await this._commonService.getFeature(name);
    return { message: 'feature', data: response };
  }

  @Post('feature/:name/like')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'Like a feature' })
  @ApiParam({ name: 'name', type: String })
  async likeFeature(
    @Param('name') name: string,
  ): Promise<PayloadSuccessDto> {
    const response = await this._commonService.likeFeature(name);
    return { message: 'feature liked', data: response };
  }

  @Get('/:pseudo/available')
  @HttpCode(HttpStatus.OK)
  @ApiParam({ name: 'pseudo', type: String })
  @ApiOkResponse({
    description: 'Checks if the chosen pseudo is available',
  })
  async isPseudoAvailable(
    @Param('pseudo') pseudo: string,
  ): Promise<PayloadSuccessDto> {
    const isAvailable = await this._commonService.isPseudoAvailable(
      pseudo,
    );
    return {
      message: 'is available',
      data: isAvailable,
    };
  }

  @Get('appVersion/:platform')
  @HttpCode(HttpStatus.OK)
  @ApiParam({ name: 'platform', type: String, enum: PlatformEnum })
  async getAppVersion(
    @Param('platform') platform: PlatformEnum,
  ): Promise<PayloadSuccessDto> {
    const appVersion = await this._commonService.getAppVersion(
      platform,
    );

    return {
      message: 'App Version.',
      data: appVersion,
    };
  }
}
