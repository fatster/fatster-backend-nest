import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiParam, ApiTags } from '@nestjs/swagger';

import { PayloadSuccessDto } from '../../common/dto/PayloadSuccessDto';
import { AuthUser } from '../../decorators/auth-user.decorator';
import { UserEntity } from '../../entities/user.entity';
import { AuthGuard } from '../../guards/auth.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
import { GetMessageDto } from './dto/getMessage.dto';
import { PostMessageDto } from './dto/postMessage.dto';
import { MessengerService } from './messenger.service';

@Controller('messenger')
@ApiTags('Messenger')
@UseInterceptors(AuthUserInterceptor)
export class MessengerController {
  constructor(private _messengerService: MessengerService) {}

  @HttpCode(HttpStatus.OK)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @UseGuards(AuthGuard)
  @Get('/chatroom/:id/messages')
  async getMessageFromRoomId(
    @AuthUser() user: UserEntity,
    @Param('id') id: string,
  ): Promise<PayloadSuccessDto> {
    const getMessageDto = new GetMessageDto(id, user.id, 'ddd');
    const messages = await this._messengerService.getMessageByRoomId(
      getMessageDto,
    );
    return { message: "room's messages", data: messages };
  }

  @HttpCode(HttpStatus.OK)
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @Post('/chatroom')
  async postMessage(
    @AuthUser() user: UserEntity,
    @Body() postMessageDto: PostMessageDto,
  ): Promise<PayloadSuccessDto> {
    postMessageDto.userId = user.id;
    const message = await this._messengerService.postMessage(
      postMessageDto,
    );
    return { message: 'the message has been sent', data: message };
  }
}
