import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { CoachTypeEnum } from '../common/enum/enum';
import { CoachDto } from '../dto/coach.dto';
import { CoachCategoryEntity } from './coachCategory.entity';
import { UserEntity } from './user.entity';

@Entity({ name: 'coach' })
export class CoachEntity extends AbstractEntity<CoachDto> {
  @Column()
  nameEn: string;

  @Column()
  descriptionEn: string;

  @Column()
  nameFr: string;

  @Column()
  descriptionFr: string;

  @Column({ type: 'enum', enum: CoachTypeEnum, default: 'challenge' })
  type: CoachTypeEnum;

  @Column({ nullable: true })
  imageId: string;

  @Column({
    default: 0,
  })
  likesCount?: number;

  @Column({
    default: 0,
  })
  readersCount?: number;

  @OneToMany(() => UserEntity, (user) => user.id, {
    onDelete: 'CASCADE',
  })
  readers: UserEntity[];

  @ManyToOne(() => CoachCategoryEntity, (category) => category.coach)
  coachCategory: CoachCategoryEntity;

  dtoClass = CoachDto;
}
