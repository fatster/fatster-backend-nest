import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { MessengerRoomEntity } from '../entities/messengerRoom.entity';

@EntityRepository(MessengerRoomEntity)
export class MessengerRoomRepository extends Repository<
  MessengerRoomEntity
> {}
