import {
  ApiHideProperty,
  ApiProperty,
  ApiPropertyOptional,
} from '@nestjs/swagger';
import {
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

import { PostStatusEnum } from '../../../common/enum/enum';

export class PostActivityDto {
  @ApiPropertyOptional()
  @IsOptional()
  readonly text: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsEnum(PostStatusEnum)
  readonly status: PostStatusEnum = PostStatusEnum.PUBLIC;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  readonly activityId: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  readonly duration: number;

  @ApiPropertyOptional()
  @IsOptional()
  readonly friendsToTag: string[];

  @ApiPropertyOptional()
  @IsOptional()
  photos: string[];

  @ApiHideProperty()
  userId: string;
}
