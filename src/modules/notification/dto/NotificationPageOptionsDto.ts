import { PageOptionsDto } from '../../../common/dto/PageOptionsDto';

export class NotificationPageOptionsDto extends PageOptionsDto {
  userId: string;
}
