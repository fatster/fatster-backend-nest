export enum GenderEnum {
  MALE = 'm',
  FEMALE = 'f',
}

export enum HashtagEntityType {
  POST = 'post',
  USER = 'user',
}

export enum UserRoleEnum {
  ADMIN = 'ADMIN',
}

export enum StatusFriendRequestEnum {
  ACCEPTED = 'accepted',
  REFUSED = 'refused',
  PENDING = 'pending',
  CANCELED = 'canceled',
}

export enum MediaTypeEnum {
  PHOTO = 'photo',
  VIDEO = 'video',
  BEFORE = 'before',
  AFTER = 'after',
}

export enum NutritionFoodType {
  Calories = 'calories',
  Portions = 'portions',
  Grams = 'grams',
}

export enum PostTypeEnum {
  ACTIVITY = 'activity',
  FOOD = 'food',
  WEIGHT = 'weight',
  SIMPLE = 'simple',
  SHARED_ARTICLE = 'shared_article',
  BEFORE_AFTER = 'before_after',
}

export enum PostStatusEnum {
  PUBLIC = 'public',
  PRIVATE = 'private',
  FRIENDS = 'friends',
}

export enum LikeTypeEnum {
  POST = 'post',
  COMMENT = 'comment',
}

export enum MeasureSystem {
  Metric = 'metric',
  Imperial = 'imperial',
}

export enum TypeUploadEnum {
  FEED = 'feed',
  PROFILE = 'profile',
}

export enum BadgeEnum {
  PRO = 'Pro',
  AMBASSADOR = 'Ambassador',
}

export enum HealthyTypeEnum {
  NO = 'no',
  AVERAGE = 'average',
  YES = 'yes',
}

export enum CoachTypeEnum {
  TIP = 'tip',
  CHALLENGE = 'challenge',
}

export enum AuthMethodEnum {
  EMAIL = 'email',
  FACEBOOK = 'facebook',
  APPLE = 'apple',
  INSTAGRAM = 'instagram',
  GOOGLE = 'google',
}

export enum NotificationEnum {
  WELCOME = 'welcome',
  LIKE_POST = 'likePost',
  LIKE_COMMENT = 'likeComment',
  COMMENT_POST = 'commentPost',
  NEW_FRIEND_REQUEST = 'newFriendRequest',
  NEW_FRIEND = 'newFriend',
  TAG_FRIEND = 'tagFriend',
  COMMENT_COMMENT = 'commentComment',
  FRIEND_LOST_WEIGHT = 'friendLostWeight',
  MENTION_USER_IN_COMMENT = 'mentionUserInComment',
  MENTION_USER_IN_POST = 'mentionUserInPost',
}

export enum TimeOfTheDayEnum {
  BREAKFAST = 'breakfast',
  LUNCH_TIME = 'lunchTime',
  DINNER_TIME = 'dinnerTime',
  SNACK = 'snack',
}

export enum FriendRequestEnum {
  RECEIVED = 'received',
  SENT = 'sent',
}

export enum PlatformEnum {
  IOS = 'ios',
  ANDROID = 'android',
}

export enum ArticleCategoryTypeEnum {
  MAIN = 'main',
  FEATURED = 'featured',
  TIPS = 'coach',
}
