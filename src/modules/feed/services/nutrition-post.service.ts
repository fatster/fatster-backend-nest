import { Injectable } from '@nestjs/common';

import {
  NotificationEnum,
  NutritionFoodType,
} from '../../../common/enum/enum';
import { INutritionPost } from '../../../interfaces/Post';
import { PhotoRepository } from '../../../repositories/photo.repository';
import { PostRepository } from '../../../repositories/post.repository';
import { UserNutritionRepository } from '../../../repositories/userNutrition.repository';
import { UserNutritionFoodRepository } from '../../../repositories/userNutritionFood.repository';
import { createUTCDate, formatDate } from '../../../utils/date';
import { NotificationService } from '../../notification/notification.service';
import { PostService } from './post.service';

@Injectable()
export class NutritionPostService {
  constructor(
    private readonly _postRepository: PostRepository,
    private readonly _userNutritionRepository: UserNutritionRepository,
    private readonly _userNutritionFoodRepository: UserNutritionFoodRepository,
    private readonly _photoRepository: PhotoRepository,
    private readonly _postService: PostService,
    private readonly _notificationService: NotificationService,
  ) {}

  public async post(postPayload: INutritionPost, userId: string) {
    const {
      text,
      type,
      foods,
      photos,
      status,
      healthyType,
      timeOfTheDay,
      friendsToTag,
      nutritionDate,
    } = postPayload;

    const date = nutritionDate
      ? new Date(nutritionDate)
      : createUTCDate();
    const postDate = formatDate('YYYY-MM-DD', date);

    const alreadyCreatedNutrition = await this._userNutritionRepository
      .createQueryBuilder('nutrition')
      .leftJoinAndSelect('nutrition.post', 'post')
      .where(
        `nutrition.user_id = '${userId}' and nutrition.time_of_the_day = '${timeOfTheDay}' and post.created_at::timestamp::date = '${postDate}'`,
      )
      .getOne();

    if (alreadyCreatedNutrition) {
      const [, newNutrition] = await Promise.all([
        this._userNutritionRepository.remove(alreadyCreatedNutrition),
        this._userNutritionRepository.save({
          timeOfTheDay,
          healthyType,
          createdAt: date,
          user: <any>userId,
        }),
      ]);

      const newFoodsToSave = foods.map((f) => {
        const common = {
          userNutrition: newNutrition,
          portionType: f.portionType,
          food: <any>f.id,
        };

        switch (f.portionType) {
          case NutritionFoodType.Portions:
            return this._userNutritionFoodRepository.create({
              ...common,
              portion: f.portion,
            });
          case NutritionFoodType.Grams:
            return this._userNutritionFoodRepository.create({
              ...common,
              grams: f.grams,
            });
          case NutritionFoodType.Calories:
            return this._userNutritionFoodRepository.create({
              ...common,
              calories: f.calories,
            });
        }
      });

      const newPostMeta = Promise.all([
        this._postService.createPostHashTags(text, type, userId),
        this._postService.createPostMentions(text),
        this._postService.verifyFriendsToTag(friendsToTag, userId),
        this._photoRepository.savePhotos(photos),
        this._userNutritionFoodRepository.save(newFoodsToSave),
      ]);

      const [
        newHashtags,
        newMentions,
        newTags,
        newSavedPhotos,
      ] = await newPostMeta;

      await this._postRepository.save({
        id: alreadyCreatedNutrition.post.id,
        text,
        type,
        tags: newTags,
        hashtags: newHashtags,
        status,
        photos: newSavedPhotos,
        userNutrition: newNutrition,
        mentionedUsersPseudo: newMentions.map(({ pseudo }) => pseudo),
      });

      const temp = await this._postRepository
        .createQueryBuilder('post')
        .leftJoinAndSelect('post.userNutrition', 'userNutrition')
        .leftJoinAndSelect('userNutrition.nutritionFood', 'foods')
        .leftJoinAndSelect('foods.food', 'foodsData')
        .where({ id: alreadyCreatedNutrition.post.id })
        .getOne();

      return temp.toDto();
    }
    const postMeta = Promise.all([
      this._postService.createPostHashTags(text, type, userId),
      this._postService.createPostMentions(text),
      this._postService.verifyFriendsToTag(friendsToTag, userId),
      this._photoRepository.savePhotos(photos),
    ]);

    const userNutrition = await this._userNutritionRepository.save({
      timeOfTheDay,
      healthyType,
      createdAt: date,
      user: <any>userId,
    });

    const foodsToSave = foods.map((f) => {
      const common = {
        userNutrition,
        portionType: f.portionType,
        food: <any>f.id,
      };

      switch (f.portionType) {
        case NutritionFoodType.Portions:
          return this._userNutritionFoodRepository.create({
            ...common,
            portion: f.portion,
          });
        case NutritionFoodType.Grams:
          return this._userNutritionFoodRepository.create({
            ...common,
            grams: f.grams,
          });
        case NutritionFoodType.Calories:
          return this._userNutritionFoodRepository.create({
            ...common,
            calories: f.calories,
          });
      }
    });

    this._userNutritionFoodRepository.save(foodsToSave);

    const [hashtags, mentions, tags, savedPhotos] = await postMeta;

    const post = await this._postRepository.save({
      text,
      type,
      userNutrition,
      tags,
      hashtags,
      status,
      createdAt: date,
      user: <any>userId,
      photos: savedPhotos,
      mentionedUsersPseudo: mentions.map(({ pseudo }) => pseudo),
    });

    tags
      .map(({ id }) => id)
      .map((id, _, taggedFriend) => {
        this._notificationService.addNotification({
          taggedFriend,
          from: userId,
          postId: post.id,
          userId: id,
          type: NotificationEnum.TAG_FRIEND,
        });
      });

    mentions.map((user) => {
      this._notificationService.addNotification({
        type: NotificationEnum.MENTION_USER_IN_POST,
        postId: post.id,
        userId: user.id,
        from: userId,
      });
    });

    const res = await this._postRepository
      .createQueryBuilder('post')
      .leftJoinAndSelect('post.userNutrition', 'userNutrition')
      .leftJoinAndSelect('userNutrition.nutritionFood', 'foods')
      .leftJoinAndSelect('foods.food', 'foodsData')
      .where({ id: post.id })
      .getOne();

    return res.toDto();
  }
}
