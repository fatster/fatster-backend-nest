import { IsIn } from 'class-validator';
import {
  AfterInsert,
  AfterRemove,
  Column,
  Entity,
  getConnection,
  ManyToOne,
} from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { LikeTypeEnum } from '../common/enum/enum';
import { LikeDto } from '../dto/like.dto';
import { CommentEntity } from './comment.entity';
import { PostEntity } from './post.entity';
import { UserEntity } from './user.entity';

@Entity({ name: 'like' })
export class LikeEntity extends AbstractEntity<LikeDto> {
  @ManyToOne(() => UserEntity, (user) => user.likes, {
    primary: true,
    nullable: false,
    onDelete: 'CASCADE',
  })
  user: UserEntity;

  @ManyToOne(() => PostEntity, (post) => post.likes, {
    primary: true,
    nullable: true,
    onDelete: 'CASCADE',
  })
  post: PostEntity;

  @ManyToOne(() => CommentEntity, (comment) => comment.likes, {
    primary: true,
    nullable: true,
    onDelete: 'CASCADE',
  })
  comment: CommentEntity;

  @Column({ nullable: false, name: 'user_id' })
  userId: string;

  @Column({ type: 'enum', enum: LikeTypeEnum })
  @IsIn(['post', 'comment'])
  type: LikeTypeEnum;

  @Column({ nullable: true, name: 'post_id' })
  postId: string;

  @Column({ nullable: true, name: 'comment_id' })
  commentId: string;

  @AfterInsert()
  async increaseLikesCounter() {
    const id =
      this.postId != null
        ? this.postId
        : this.commentId != null
        ? this.commentId
        : 0;
    // To avoid updated_at to be changed
    await getConnection().query(
      'UPDATE "' +
        this.type +
        '" SET "likes_count" = likes_count + 1 WHERE "id" = $1',
      [id],
    );
  }

  @AfterRemove()
  async decreaseLikesCounter() {
    const id =
      this.postId != null
        ? this.postId
        : this.commentId != null
        ? this.commentId
        : 0;
    // To avoid updated_at to be changed
    await getConnection().query(
      'UPDATE "' +
        this.type +
        '" SET "likes_count" = likes_count - 1 WHERE "id" = $1',
      [id],
    );
  }

  dtoClass = LikeDto;
}
