import { ApiProperty } from '@nestjs/swagger';

import { PageMetaDto } from './PageMetaDto';

export class ResultPageDto {
  @ApiProperty({})
  readonly response: any[];

  @ApiProperty()
  readonly meta: PageMetaDto;

  constructor(response: any[], meta: PageMetaDto) {
    this.response = response;
    this.meta = meta;
  }
}
