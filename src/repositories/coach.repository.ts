import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { CoachEntity } from '../entities/coach.entity';

@EntityRepository(CoachEntity)
export class CoachRepository extends Repository<CoachEntity> {}
