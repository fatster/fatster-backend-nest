import { IsNotEmpty, IsString } from 'class-validator';

export class ContactUsMessageDto {
  @IsString()
  @IsNotEmpty()
  readonly subject: string;

  @IsString()
  @IsNotEmpty()
  readonly text: string;
}
