import {
  Body,
  Controller,
  Get,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';

import { PayloadSuccessDto } from '../../common/dto/PayloadSuccessDto';
import { AuthUser } from '../../decorators/auth-user.decorator';
import { UserEntity } from '../../entities/user.entity';
import { AuthGuard } from '../../guards/auth.guard';
import { CalendarDto, CreateCalendarDto } from './calendar.dto';
import { CalendarService } from './calendar.service';

@Controller('calendar')
@ApiTags('Calendar')
export class CalendarController {
  constructor(private readonly _calendarService: CalendarService) {}

  @Get('/events')
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({
    type: CalendarDto,
    isArray: true,
    description: 'User events list',
  })
  public async getAll(
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const { id } = user;
    const data = await this._calendarService.getAll(id);

    return {
      data: data.toDtos(),
      message: 'Events list',
    };
  }

  @Post('/events')
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({
    type: CalendarDto,
    description: 'Successfully created event',
  })
  public async calendarSettings(
    @AuthUser() user: UserEntity,
    @Body() createCalendarDto: CreateCalendarDto,
  ): Promise<PayloadSuccessDto> {
    const { id } = user;
    const data = await this._calendarService.saveEvent(
      createCalendarDto,
      id,
    );

    return {
      data,
      message: 'Successfully created event',
    };
  }
}
