import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { LikeEntity } from '../entities/like.entity';

@EntityRepository(LikeEntity)
export class LikeRepository extends Repository<LikeEntity> {
  public async findByUserComment(userId: string, commentId: string) {
    return this.findOne({
      where: {
        userId,
        commentId,
      },
      loadRelationIds: true,
    });
  }

  public async findByUserPost(userId: string, postId: string) {
    return this.findOne({
      where: {
        userId,
        postId,
      },
    });
  }
}
