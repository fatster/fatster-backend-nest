import { Body, Controller, Post } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';

import { PayloadSuccessDto } from '../../../common/dto/PayloadSuccessDto';
import { UserDto } from '../../../dto/user.dto';
import { AuthService } from '../../../modules/auth/auth.service';
import { ProLoginDto } from '../dto/pro-login.dto';
import { ProRegisterDto } from '../dto/pro-register.dto';
import { AUTH_CONTROLLER_PATH } from '../pro.constants';
import { ProAuthService } from '../services/pro-auth.service';

@Controller(AUTH_CONTROLLER_PATH)
@ApiTags('Pro auth')
export class AuthController {
  constructor(
    private readonly _authService: AuthService,
    private readonly _proAuthService: ProAuthService,
  ) {}

  @Post('register')
  @ApiOkResponse({
    type: UserDto,
    description: 'Successfully Registered',
  })
  public async registerPro(
    @Body() proRegisterDto: ProRegisterDto,
  ): Promise<PayloadSuccessDto> {
    const user = await this._proAuthService.registerPro(
      proRegisterDto,
    );
    const token = await this._authService.createToken(user);

    return {
      message: 'Successfully registered',
      data: { token, user },
    };
  }

  @Post('login')
  @ApiOkResponse({
    type: UserDto,
    description: 'Successfully logged in',
  })
  public async loginPro(
    @Body() proLoginDto: ProLoginDto,
  ): Promise<PayloadSuccessDto> {
    const user = await this._proAuthService.loginPro(proLoginDto);
    const token = await this._authService.createToken(user);

    return {
      message: 'Successfully logged in',
      data: { token, user },
    };
  }
}
