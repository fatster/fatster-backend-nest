import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { FriendRequestRepository } from '../../repositories/friendRequest.repository';
import { UserRepository } from '../../repositories/user.repository';
import { AuthModule } from '../auth/auth.module';
import { SearchController } from './search.controller';
import { SearchService } from './search.service';

@Module({
  imports: [
    forwardRef(() => AuthModule),
    TypeOrmModule.forFeature([
      UserRepository,
      FriendRequestRepository,
    ]),
  ],
  controllers: [SearchController],
  exports: [SearchService],
  providers: [SearchService],
})
export class SearchModule {}
