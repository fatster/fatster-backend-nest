import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { FirebaseAuthDto } from '../dto/firebaseAuth.dto';
import { UserEntity } from './user.entity';

@Entity({ name: 'firebase_auth' })
export class FirebaseAuthEntity extends AbstractEntity<
  FirebaseAuthDto
> {
  @Column()
  hash: string;

  @Column()
  salt: string;

  @OneToOne(() => UserEntity, (user) => user.firebaseAuth, {
    nullable: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  user: UserEntity;

  dtoClass = FirebaseAuthDto;
}
