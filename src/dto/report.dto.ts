import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { PostDto } from './post.dto';
import { UserDto } from './user.dto';

export class ReportDto extends AbstractDto {
  @ApiPropertyOptional()
  post: PostDto;

  @ApiPropertyOptional()
  user: UserDto;
}
