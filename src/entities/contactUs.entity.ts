import { Column, Entity } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { ContactUsDto } from '../dto/contactUs.dto';

@Entity({ name: 'contact_us' })
export class ContactUsEntity extends AbstractEntity<ContactUsDto> {
  @Column()
  userEmail: string;

  @Column()
  subject: string;

  @Column()
  text: string;

  dtoClass = ContactUsDto;
}
