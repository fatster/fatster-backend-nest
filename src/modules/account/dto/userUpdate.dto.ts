'use strict';

import { ApiProperty } from '@nestjs/swagger';
import {
  IsIn,
  IsNotEmpty,
  IsNumber,
  IsString,
  Max,
  Min,
} from 'class-validator';

import { GenderEnum } from '../../../common/enum/enum';
import { Language } from '../../../shared/services/translation.service';

export class UserUpdateDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly pseudo: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly language: Language;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty()
  readonly originalWeight: number;

  @IsNumber()
  @Min(30)
  @Max(200)
  @IsNotEmpty()
  @ApiProperty()
  readonly targetWeight: number;

  @IsNumber()
  @Min(100)
  @Max(280)
  @IsNotEmpty()
  @ApiProperty()
  readonly height: number;

  @IsString()
  @IsNotEmpty()
  @IsIn(['m', 'f'])
  @ApiProperty()
  readonly gender: GenderEnum;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly birthDate: string;
}
