import {
  AfterLoad,
  Column,
  Entity,
  JoinColumn,
  OneToOne,
} from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { NavigationMainPictureDto } from '../dto/navigationMainPicture.dto';
import { ArticleEntity } from './article.entity';

@Entity({ name: 'navigation_main_picture' })
export class NavigationMainPictureEntity extends AbstractEntity<
  NavigationMainPictureDto
> {
  @Column()
  nameEn: string;

  @Column()
  ctaEn: string;

  @Column()
  nameFr: string;

  @Column()
  ctaFr: string;

  @Column()
  backgroundPic: string;

  @Column()
  url: string;

  @Column({ default: true })
  isActive: boolean;

  @OneToOne(
    () => ArticleEntity,
    (article) => article.navigationMainPicture,
  )
  @JoinColumn()
  article: ArticleEntity;

  @AfterLoad()
  prefixWithUrl() {
    if (this.backgroundPic) {
      this.backgroundPic = `${process.env.S3_BASE_URL}${this.backgroundPic}?auto=compress&q=20`;
    }
  }

  dtoClass = NavigationMainPictureDto;
}
