import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { UserWeightEntity } from '../entities/userWeight.entity';

@EntityRepository(UserWeightEntity)
export class UserWeightRepository extends Repository<
  UserWeightEntity
> {}
