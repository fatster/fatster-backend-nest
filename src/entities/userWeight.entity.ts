import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
} from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { UserWeightDto } from '../dto/userWeight.dto';
import { PostEntity } from './post.entity';
import { UserEntity } from './user.entity';

@Entity({ name: 'user_weight' })
export class UserWeightEntity extends AbstractEntity<UserWeightDto> {
  @Column({ type: 'float' })
  weight: number;

  @Column({ type: 'float', nullable: true })
  weightDifference: number;

  @ManyToOne(() => UserEntity, (post) => post.userWeights, {
    onDelete: 'CASCADE',
  })
  user: UserEntity;

  @OneToOne(() => PostEntity, (post) => post.userWeight)
  @JoinColumn()
  post: PostEntity;

  dtoClass = UserWeightDto;
}
