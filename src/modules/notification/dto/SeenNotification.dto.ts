'use strict';

export class SeenNotificationDto {
  notificationId: string;

  userId: string;
}
