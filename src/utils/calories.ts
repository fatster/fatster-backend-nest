import { GenderEnum } from '../common/enum/enum';

function keepInAllowedRange(v: number) {
  if (v < 1400) {
    return 1400;
  }
  if (v > 2500) {
    return 2500;
  }
  return v;
}

export function getLoseWeightCalories(
  kg: number,
  metrs: number,
  years: number,
  gender: GenderEnum,
) {
  const P = Math.pow(kg, 0.48);
  const T = Math.pow(metrs, 0.5);
  const A = Math.pow(years, -0.13);
  const isAdult = years >= 18;
  switch (gender) {
    case GenderEnum.FEMALE: {
      if (isAdult) {
        return keepInAllowedRange(
          ((0.963 * P * T * A) / 4.18) * 1000,
        );
      }
      return keepInAllowedRange(
        ((30.9 * kg + 2016.6 * metrs + 907) / 4.18) * 1.3 * 1000,
      );
    }
    case GenderEnum.MALE:
      if (isAdult) {
        return keepInAllowedRange(
          ((1.083 * P * T * A) / 4.18) * 1000,
        );
      }
      return keepInAllowedRange(
        ((69.4 * kg + 322 * metrs + 2392) / 4.18) * 1.3 * 1000,
      );
  }
}

export function getMaintainWeightCalories(
  loseWeightCalories: number,
  years: number,
) {
  const isAdult = years >= 18;

  if (isAdult) {
    return loseWeightCalories * 1.25;
  }
  return loseWeightCalories * 1.6;
}

interface ICalories {
  loseCalories: number;
  maintainCalories: number;
}

export function getCalories(
  kg: number,
  metrs: number,
  years: number,
  gender: GenderEnum,
): ICalories {
  if (!kg || !metrs || !years || !gender) {
    return {
      loseCalories: 0,
      maintainCalories: 0,
    };
  }

  const loseCalories = getLoseWeightCalories(
    kg,
    metrs,
    years,
    gender,
  );
  const maintainCalories = getMaintainWeightCalories(
    loseCalories,
    years,
  );
  return {
    loseCalories: Math.round(loseCalories),
    maintainCalories: Math.round(maintainCalories),
  };
}
