'use strict';

import { BadRequestException } from '@nestjs/common';

export class SimpleTextMustHaveAtLeastATextOrAPhoto extends BadRequestException {
  constructor(error?: string) {
    super(
      'error.simple-text-must-have-at-leat-a-text-or-a-photo',
      error,
    );
  }
}
