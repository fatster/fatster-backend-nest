'use strict';

import { BadRequestException } from '@nestjs/common';

export class FriendRequestAlreadyExistException extends BadRequestException {
  constructor(error?: string) {
    super('error.friend_request_already_exist', error);
  }
}
