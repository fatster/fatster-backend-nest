'use strict';

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class SendFriendRequestDto {
  @IsNotEmpty()
  @ApiProperty()
  readonly userId: string;
}
