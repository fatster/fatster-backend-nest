import { IsIn } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import {
  HealthyTypeEnum,
  TimeOfTheDayEnum,
} from '../common/enum/enum';
import { UserNutritionDto } from '../dto/userNutrition.dto';
import { PostEntity } from './post.entity';
import { UserEntity } from './user.entity';
import { UserNutritionFoodEntity } from './userNutritionFood.entity';

@Entity({ name: 'user_nutrition' })
export class UserNutritionEntity extends AbstractEntity<
  UserNutritionDto
> {
  @OneToMany(
    () => UserNutritionFoodEntity,
    (userNutrition) => userNutrition.userNutrition,
  )
  nutritionFood: UserNutritionFoodEntity[];

  @OneToOne(() => PostEntity, (post) => post.userNutrition, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  post: PostEntity;

  @Column({
    type: 'enum',
    enum: HealthyTypeEnum,
  })
  @IsIn(['no', 'average', 'yes'])
  healthyType?: HealthyTypeEnum;

  /* @Column({ type: 'float', nullable: true })
  grandTotalCalories: number; */

  @Column({
    type: 'enum',
    enum: TimeOfTheDayEnum,
    default: 'lunchTime',
  })
  @IsIn(['breakfast', 'lunchTime', 'dinnerTime', 'snack'])
  timeOfTheDay?: TimeOfTheDayEnum;

  @ManyToOne(() => UserEntity, (user) => user.userNutritions, {
    onDelete: 'CASCADE',
  })
  user: UserEntity;

  summaryCalories: number;

  dtoClass = UserNutritionDto;
}
