import { EntityRepository, Repository } from 'typeorm';

import { FirebaseAuthEntity } from '../entities/firebaseAuth.entity';

@EntityRepository(FirebaseAuthEntity)
export class FirebaseAuthRepository extends Repository<
  FirebaseAuthEntity
> {}
