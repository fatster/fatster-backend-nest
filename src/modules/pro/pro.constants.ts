import { createProPath } from './pro.helper';

export const ROOT_CONTROLLER_PATH = createProPath();
export const AUTH_CONTROLLER_PATH = createProPath('auth');
export const ONBOARD_CONTROLLER_PATH = createProPath('onboard');
