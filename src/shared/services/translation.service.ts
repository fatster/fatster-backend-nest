import { Injectable } from '@nestjs/common';
import { GoogleSpreadsheet } from 'google-spreadsheet';
import { keys } from 'lodash';

import { ConfigService } from './config.service';

export enum Term {
  PUSH_BUDDY_LIKED_POST = 'PUSH_BUDDY_LIKED_POST',
  PUSH_BUDDY_LIKED_COMMENTS = 'PUSH_BUDDY_LIKED_COMMENTS',
  PUSH_BUDDY_COMMENTED = 'PUSH_BUDDY_COMMENTED',
  PUSH_BUDDY_REQUEST_SENT = 'PUSH_BUDDY_REQUEST_SENT',
  PUSH_BUDDY_REQUEST_ACCEPTED = 'PUSH_BUDDY_REQUEST_ACCEPTED',
  NOTIFY_MENTION_COMMENT = 'NOTIFY_MENTION_COMMENT',
  NOTIFY_MENTION_POST = 'NOTIFY_MENTION_POST',
  NOTIFY_TAG_POST = 'NOTIFY_TAG_POST',
  NOTIFY_FRIEND_LOST_WEIGHT = 'NOTIFY_FRIEND_LOST_WEIGHT',
  NOTIFY_COMMENT_COMMENT = 'NOTIFY_COMMENT_COMMENT',
}

export interface ITermVariables {
  [Term.PUSH_BUDDY_LIKED_POST]: { ['{{name}}']: string };
  [Term.PUSH_BUDDY_LIKED_COMMENTS]: { ['{{name}}']: string };
  [Term.PUSH_BUDDY_COMMENTED]: { ['{{name}}']: string };
  [Term.PUSH_BUDDY_REQUEST_SENT]: { ['{{name}}']: string };
  [Term.PUSH_BUDDY_REQUEST_ACCEPTED]: { ['{{name}}']: string };
  [Term.NOTIFY_MENTION_COMMENT]: { ['{{name}}']: string };
  [Term.NOTIFY_MENTION_POST]: { ['{{name}}']: string };
  [Term.NOTIFY_TAG_POST]: { ['{{name}}']: string };
  [Term.NOTIFY_FRIEND_LOST_WEIGHT]: { ['{{name}}']: string };
  [Term.NOTIFY_COMMENT_COMMENT]: { ['{{name}}']: string };
}

export enum LanguageEnum {
  EN = 'EN',
  FR = 'FR',
  PL = 'PL',
  TH = 'TH',
  ES = 'ES',
  PT = 'PT',
}

export enum LangVersion {
  V2 = 'V2',
  V3 = 'V3',
}

export type Language = keyof typeof LanguageEnum;
export type Version = keyof typeof LangVersion;
export type TranslationsObj = Partial<{ [key in Term]: string }>;
export type LangMap = Record<Language, TranslationsObj>;
export type LangVersionMap = Record<Version, LangMap>;
export type LastFetchMap = Record<Version, number>;

@Injectable()
export class TranslationService {
  private _localeMap: Partial<LangVersionMap> = {};
  private _lastFetch: Partial<LastFetchMap> = {};
  private readonly _ttlInMS = 2.16e7; // 1000 * 60 * 60 * 6 (6hours)
  private readonly _config: Record<Language, number> = {
    EN: 1616568333,
    FR: 1184042870,
    PL: 1813540939,
    TH: 1631573817,
    ES: 1579054609,
    PT: 1307413406,
  };

  constructor(private readonly _configService: ConfigService) {}

  public async t<T extends Term>(
    lang: Language,
    term: T,
    variables: ITermVariables[T],
    version: Version = 'V2',
  ) {
    const localeMap = await this._getLocaleMap(version);
    const translate = localeMap[lang][term];
    if (!translate) {
      return `${
        keys(localeMap || {}).length
      } missed ${term} key for ${lang}`;
    }

    return keys(variables)
      .reduce((acc, v) => acc.replace(v, variables[v]), translate)
      .trim();
  }

  public async createNotificationContent<T extends Term>(
    term: T,
    variables: ITermVariables[T],
  ) {
    const en = await this.t('EN', term, variables);
    const fr = await this.t('FR', term, variables);
    const pl = await this.t('PL', term, variables);
    const es = await this.t('ES', term, variables);
    const pt = await this.t('PT', term, variables);
    const th = await this.t('TH', term, variables);

    return {
      en,
      fr,
      pl,
      es,
      pt,
      th,
    };
  }

  public async getTranslationsForLang(
    language: Language,
    disableCache: boolean,
    version: Version = 'V2',
  ): Promise<TranslationsObj> {
    const localeMap = await this._getLocaleMap(version, disableCache);

    const langLocale = localeMap[language];

    if (!langLocale) {
      throw new Error(`Language ${language} doesn't exist in config`);
    }

    return langLocale;
  }

  private async _fetchDoc(
    version: Version,
  ): Promise<GoogleSpreadsheet> {
    const sheetId =
      version === 'V2'
        ? 'GOOGLE_SPREADSHEET_SHEET_ID_V2'
        : 'GOOGLE_SPREADSHEET_SHEET_ID_V3';

    const doc = new GoogleSpreadsheet(
      this._configService.get(sheetId),
    );

    doc.useApiKey(
      this._configService.get('GOOGLE_SPREADSHEET_API_KEY'),
    );
    await doc.loadInfo();

    return doc;
  }

  private async _createLocaleMap(version: Version) {
    try {
      const localeMap: LangMap = {
        EN: {},
        FR: {},
        PL: {},
        TH: {},
        ES: {},
        PT: {},
      };
      const doc = await this._fetchDoc(version);

      const langKeys = <Language[]>keys(localeMap);
      for (const lang of langKeys) {
        const sheet = doc.sheetsById[this._config[lang]];
        const rows = await sheet.getRows();
        rows.map((row) => {
          localeMap[lang][row.key] = row.translation;
        });
      }

      this._lastFetch[version] = Date.now();
      this._localeMap[version] = localeMap;
    } catch (error) {
      // console.log(error);
    }
    return this._localeMap[version];
  }

  private async _getLocaleMap(
    version: Version,
    disableCache?: boolean,
  ) {
    const localeMap = this._localeMap[version];
    if (localeMap && !disableCache) {
      const fetchTimeDiff = Date.now() - this._lastFetch[version];
      const isOutDated = this._ttlInMS - fetchTimeDiff < 0;

      if (isOutDated) {
        return this._createLocaleMap(version);
      }
      return Promise.resolve(localeMap);
    }
    return this._createLocaleMap(version);
  }
}
