'use strict';

import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsUUID } from 'class-validator';

export class PostMessageDto {
  @IsUUID()
  @IsNotEmpty()
  @ApiProperty()
  messengerRoomId: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  text: string;

  @ApiHideProperty()
  userId: string;

  constructor(messengerRoomId: string, userId: string) {
    this.messengerRoomId = messengerRoomId;
    this.userId = userId;
  }
}
