import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import {
  HealthyTypeEnum,
  TimeOfTheDayEnum,
} from '../common/enum/enum';
import { isDto } from '../common/utils/entities';
import { UserNutritionEntity } from '../entities/userNutrition.entity';
import { UserNutritionFoodEntity } from '../entities/userNutritionFood.entity';
import { UserDto } from './user.dto';

export class UserNutritionDto extends AbstractDto {
  @ApiPropertyOptional()
  foods?: UserNutritionFoodEntity[];

  @ApiPropertyOptional()
  user: UserDto;

  @ApiPropertyOptional()
  post: any;

  @ApiProperty({ enum: ['no', 'average', 'yes'] })
  healthyType?: HealthyTypeEnum;

  @ApiProperty({
    enum: ['breakfast', 'lunchTime', 'dinnerTime', 'snack'],
  })
  timeOfTheDay?: TimeOfTheDayEnum;

  summaryCalories: number;

  constructor(userNutrition: UserNutritionEntity) {
    super(userNutrition);
    this.healthyType = userNutrition.healthyType;
    this.timeOfTheDay = userNutrition.timeOfTheDay;

    this.foods = (userNutrition.nutritionFood || []).map((f) =>
      isDto(f) ? f.toDto() : f,
    );
    this.post = isDto(userNutrition.post)
      ? userNutrition.post.toDto()
      : <any>userNutrition.post;
    this.user = isDto(userNutrition.user)
      ? userNutrition.user.toDto()
      : <any>userNutrition.user;
    this.summaryCalories =
      Math.round(
        (this.foods || []).reduce(
          (sum, { summaryCalories }) => sum + (summaryCalories || 0),
          0,
        ) * 10,
      ) / 10;
  }
}
