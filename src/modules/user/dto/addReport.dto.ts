import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';

export class AddReportDto {
  @IsUUID()
  @IsNotEmpty()
  @ApiProperty()
  readonly postId: string;

  @ApiHideProperty()
  userId: string;
}
