import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { request } from 'https';
import { CreateNotificationBody } from 'onesignal-node/lib/types';

import { NotificationEnum } from '../../common/enum/enum';
import { UserPreferencesDto } from '../../dto/userPreferences.dto';
import { NotificationRepository } from '../../repositories/notification.repository';
import { UserRepository } from '../../repositories/user.repository';
import { UserPreferencesRepository } from '../../repositories/userPreferences.repository';
import { ConfigService } from '../../shared/services/config.service';
import {
  Term,
  TranslationService,
} from '../../shared/services/translation.service';
import { AddNotificationDto } from './dto/AddNotificationDto';
import { DeleteNotificationDto } from './dto/DeleteNotification.dto';
import { NotificationPageOptionsDto } from './dto/NotificationPageOptionsDto';
import { SeenNotificationDto } from './dto/SeenNotification.dto';

function noop() {
  // noop fn
}
/* eslint-disable */
function sendNotification(data: any) {
  const headers = {
    "Content-Type": "application/json; charset=utf-8",
    "Authorization": "Basic OGJjMDUwMGUtZWI1YS00OWRhLTg0OWQtOTUwZjA5MzRkMDZm"
  };
  
  
  const options = {
    host: "onesignal.com",
    port: 443,
    path: "/api/v1/notifications",
    method: "POST",
    headers: headers
  };

  const req = request(options, function(res) {  
    res.on('data', noop);
  });
  
  req.on('error', noop);
  
  req.write(JSON.stringify(data));
  req.end();
};
/* eslint-enable */
@Injectable()
export class NotificationService {
  private readonly _notifyKey: Record<
    NotificationEnum,
    keyof UserPreferencesDto
  > = {
    [NotificationEnum.LIKE_POST]: 'pushNotificationLikes',
    [NotificationEnum.WELCOME]: null,
    [NotificationEnum.LIKE_COMMENT]: 'pushNotificationLikes',
    [NotificationEnum.COMMENT_POST]: 'pushNotificationComments',
    [NotificationEnum.NEW_FRIEND_REQUEST]:
      'pushNotificationFriendRequest',
    [NotificationEnum.NEW_FRIEND]: 'pushNotificationNewFriend',
    [NotificationEnum.TAG_FRIEND]: null,
    [NotificationEnum.COMMENT_COMMENT]: 'pushNotificationComments',
    [NotificationEnum.FRIEND_LOST_WEIGHT]: null,
    [NotificationEnum.MENTION_USER_IN_COMMENT]:
      'pushNotificationMentionMessages',
    [NotificationEnum.MENTION_USER_IN_POST]:
      'pushNotificationMentionMessages',
  };

  constructor(
    private readonly _userRepository: UserRepository,
    private readonly _notificationRepository: NotificationRepository,
    private readonly _configService: ConfigService,
    private readonly _translationService: TranslationService,
    private readonly _userPreferencesRepository: UserPreferencesRepository,
  ) {}

  async countUserNotification(userId: string) {
    try {
      return this._notificationRepository
        .createQueryBuilder('notification')
        .where('notification.user_id = :userID', {
          userID: userId,
        })
        .andWhere('notification.seen = FALSE')
        .getCount();
    } catch (err) {
      throw new Error(err);
    }
  }

  async getUserNotification(
    notificationPageOption: NotificationPageOptionsDto,
  ) {
    try {
      return this._notificationRepository
        .createQueryBuilder('notification')
        .leftJoinAndSelect('notification.comment', 'comment')
        .leftJoinAndSelect('notification.like', 'like')
        .leftJoinAndSelect('notification.post', 'post')
        .leftJoinAndSelect(
          'notification.friendRequest',
          'friendRequest',
        )
        .leftJoinAndSelect('notification.from', 'from')
        .where('notification.user_id = :userID', {
          userID: notificationPageOption.userId,
        })
        .orderBy(
          'notification.createdAt',
          notificationPageOption.order,
        )
        .take(notificationPageOption.take)
        .skip(notificationPageOption.skip)
        .getMany();
    } catch (err) {
      throw new Error(err);
    }
  }

  async createNotification(addNotificationDto: AddNotificationDto) {
    const notification = this._notificationRepository.create();
    notification.type = addNotificationDto.type;
    notification.from = <any>addNotificationDto.from;
    notification.user = <any>addNotificationDto.userId;
    notification.post = <any>addNotificationDto.postId;
    notification.comment = <any>addNotificationDto.commentId;
    notification.friendRequest = <any>(
      addNotificationDto.friendRequestId
    );
    notification.like = <any>addNotificationDto.likeId;

    const notify = await this._needPushNotification(
      addNotificationDto,
    );

    if (notify) {
      await this.sendPushNotification(
        addNotificationDto.from,
        addNotificationDto.userId,
        addNotificationDto.type,
        addNotificationDto.postId ||
          addNotificationDto.friendRequestId,
      );
    }

    if (
      addNotificationDto.type !==
        NotificationEnum.FRIEND_LOST_WEIGHT &&
      addNotificationDto.type !== NotificationEnum.COMMENT_COMMENT
    ) {
      return this._notificationRepository.save(notification);
    }
    return Promise.resolve();
  }

  async addNotification(addNotificationDto: AddNotificationDto) {
    // Don't send the notification to myself
    if (
      addNotificationDto.from !== addNotificationDto.userId // &&
      // addNotificationDto.type !== NotificationEnum.TAG_FRIEND
    ) {
      this.createNotification(addNotificationDto);

      /*switch (addNotificationDto.type) {
        case NotificationEnum.TAG_FRIEND:
          // Send a notification to every tagged friend
          addNotificationDto.taggedFriend.map(
            async (taggedFriendId) => {
              addNotificationDto.userId = taggedFriendId;
              this.createNotification(addNotificationDto);
            },
          );
          break;
        default:
          this.createNotification(addNotificationDto);
      }*/
    }
    return Promise.resolve();
  }

  async notificationSeen(seenNotificationDto: SeenNotificationDto) {
    const notification = await this._getNotification(
      seenNotificationDto.notificationId,
      seenNotificationDto.userId,
    );

    notification.seen = true;
    return this._notificationRepository.save(notification);
  }

  async deleteNotification(
    deleteNotificationDto: DeleteNotificationDto,
  ) {
    const notification = await this._getNotification(
      deleteNotificationDto.notificationId,
      deleteNotificationDto.userId,
    );

    return this._notificationRepository.delete({
      id: notification.id,
    });
  }

  private async _needPushNotification(
    notification: AddNotificationDto,
  ) {
    switch (notification.type) {
      case NotificationEnum.LIKE_POST:
      case NotificationEnum.WELCOME:
      case NotificationEnum.LIKE_COMMENT:
      case NotificationEnum.COMMENT_POST:
      case NotificationEnum.NEW_FRIEND_REQUEST:
      case NotificationEnum.NEW_FRIEND:
      case NotificationEnum.TAG_FRIEND:
      case NotificationEnum.COMMENT_COMMENT:
      case NotificationEnum.FRIEND_LOST_WEIGHT:
      case NotificationEnum.MENTION_USER_IN_COMMENT:
      case NotificationEnum.MENTION_USER_IN_POST: {
        const key = this._notifyKey[notification.type];
        if (key) {
          const count = await this._userPreferencesRepository.count({
            where: {
              user: notification.userId,
              [key]: true,
            },
          });
          return Boolean(count);
        }
        return true;
      }
    }
  }

  private async _getNotification(
    notificationId: string,
    userId: string,
  ) {
    const notification = await this._notificationRepository.findOne({
      where: { id: notificationId },
      loadRelationIds: true,
    });

    if (!notification) {
      throw new NotFoundException();
    }

    if (notification.user !== <any>userId) {
      throw new BadRequestException('unauthorized');
    }

    return notification;
  }

  async sendPushNotification(
    fromUserId: string,
    toUserId: string,
    typePushNotification: NotificationEnum,
    data?: string,
  ) {
    try {
      const [toUser, fromUser] = await Promise.all([
        this._userRepository.findOne(toUserId),
        this._userRepository.findOne(fromUserId),
      ]);

      let notification: CreateNotificationBody;

      if (toUser.oneSignalPlayerId) {
        switch (typePushNotification) {
          case NotificationEnum.LIKE_COMMENT: {
            const contents = await this._translationService.createNotificationContent(
              Term.PUSH_BUDDY_LIKED_COMMENTS,
              { ['{{name}}']: fromUser.pseudo },
            );

            notification = {
              contents,
              data: { postId: data },
            };
            break;
          }
          case NotificationEnum.LIKE_POST: {
            const contents = await this._translationService.createNotificationContent(
              Term.PUSH_BUDDY_LIKED_POST,
              { ['{{name}}']: fromUser.pseudo },
            );

            notification = {
              contents,
              data: { postId: data, type: 'like' },
            };
            break;
          }
          case NotificationEnum.MENTION_USER_IN_POST: {
            const contents = await this._translationService.createNotificationContent(
              Term.NOTIFY_MENTION_POST,
              { ['{{name}}']: fromUser.pseudo },
            );

            notification = {
              contents,
              data: { postId: data },
            };
            break;
          }
          case NotificationEnum.MENTION_USER_IN_COMMENT: {
            const contents = await this._translationService.createNotificationContent(
              Term.NOTIFY_MENTION_COMMENT,
              { ['{{name}}']: fromUser.pseudo },
            );

            notification = {
              contents,
              data: { postId: data },
            };
            break;
          }
          case NotificationEnum.NEW_FRIEND_REQUEST: {
            const contents = await this._translationService.createNotificationContent(
              Term.PUSH_BUDDY_REQUEST_SENT,
              { ['{{name}}']: fromUser.pseudo },
            );

            notification = {
              contents,
              data: { friendRequestId: data },
            };
            break;
          }
          case NotificationEnum.NEW_FRIEND: {
            const contents = await this._translationService.createNotificationContent(
              Term.PUSH_BUDDY_REQUEST_ACCEPTED,
              { ['{{name}}']: fromUser.pseudo },
            );

            notification = {
              contents,
              data: { friendId: data },
            };
            break;
          }
          case NotificationEnum.COMMENT_POST: {
            const contents = await this._translationService.createNotificationContent(
              Term.PUSH_BUDDY_COMMENTED,
              { ['{{name}}']: fromUser.pseudo },
            );

            notification = {
              contents,
              data: { postId: data, type: 'comment' },
            };
            break;
          }
          case NotificationEnum.TAG_FRIEND: {
            const contents = await this._translationService.createNotificationContent(
              Term.NOTIFY_TAG_POST,
              { ['{{name}}']: fromUser.pseudo },
            );

            notification = {
              contents,
              data: { postId: data, type: 'tag' },
            };
            break;
          }
          case NotificationEnum.COMMENT_COMMENT: {
            const contents = await this._translationService.createNotificationContent(
              Term.NOTIFY_COMMENT_COMMENT,
              { ['{{name}}']: fromUser.pseudo },
            );

            notification = {
              contents,
              data: { postId: data, type: 'comment' },
            };
            break;
          }
          case NotificationEnum.FRIEND_LOST_WEIGHT: {
            const contents = await this._translationService.createNotificationContent(
              Term.NOTIFY_FRIEND_LOST_WEIGHT,
              { ['{{name}}']: fromUser.pseudo },
            );

            notification = {
              contents,
              data: { postId: data },
            };
            break;
          }
          default:
        }

        const message = {
          app_id: '132dc2ce-7313-435b-b82f-b3b2c6840da2',
          ...notification,
          channel_for_external_user_ids: 'push',
          include_external_user_ids: [toUserId],
        };

        sendNotification(message);
        return Promise.resolve();
      }
      return Promise.resolve();
    } catch (err) {
      // console.log(JSON.stringify(err));
    }
  }
}
