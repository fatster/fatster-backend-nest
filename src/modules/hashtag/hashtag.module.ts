import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { HashtagRepository } from '../../repositories/hashtag.repository';
import { PostRepository } from '../../repositories/post.repository';
import { UserRepository } from '../../repositories/user.repository';
import { AuthModule } from '../auth/auth.module';
import { HashtagController } from './hashtag.controller';
import { HashtagService } from './hashtag.service';

@Module({
  imports: [
    forwardRef(() => AuthModule),
    TypeOrmModule.forFeature([
      HashtagRepository,
      PostRepository,
      UserRepository,
    ]),
  ],
  controllers: [HashtagController],
  exports: [HashtagService],
  providers: [HashtagService],
})
export class HashtagModule {}
