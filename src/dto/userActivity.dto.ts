import {
  ApiHideProperty,
  ApiPropertyOptional,
} from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { isDto } from '../common/utils/entities';
import { UserActivityEntity } from '../entities/userActivity.entity';
import { ActivityDto } from './activity.dto';
import { PostDto } from './post.dto';

export class UserActivityDto extends AbstractDto {
  @ApiPropertyOptional()
  duration: number;

  @ApiHideProperty()
  calorieBurned: number;

  @ApiPropertyOptional()
  post: PostDto[];

  @ApiPropertyOptional()
  activity: ActivityDto;

  constructor(userActivity: UserActivityEntity) {
    super(userActivity);
    this.duration = userActivity.duration;
    this.calorieBurned = userActivity.calorieBurned;
    this.activity = isDto(userActivity.activity)
      ? userActivity.activity.toDto()
      : <any>userActivity.activity;
    this.post = isDto(userActivity.post)
      ? userActivity.post.toDto()
      : <any>userActivity.post;
  }
}
