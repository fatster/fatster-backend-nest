import { Entity, ManyToOne } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { ReportDto } from '../dto/report.dto';
import { PostEntity } from './post.entity';
import { UserEntity } from './user.entity';

@Entity({ name: 'report' })
export class ReportEntity extends AbstractEntity<ReportDto> {
  @ManyToOne(() => PostEntity, (post) => post.reports, {
    onDelete: 'CASCADE',
  })
  post: PostEntity;

  @ManyToOne(() => UserEntity, (user) => user.reports, {
    onDelete: 'CASCADE',
  })
  user: UserEntity;

  dtoClass = ReportDto;
}
