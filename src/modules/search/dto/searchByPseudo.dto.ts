// eslint-disable-next-line import/named
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

import { PageOptionsDto } from '../../../common/dto/PageOptionsDto';

export class SearchByPseudoDto extends PageOptionsDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly pseudo: string;

  @ApiProperty()
  readonly withMe?: boolean;
}
