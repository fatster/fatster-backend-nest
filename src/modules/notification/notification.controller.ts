import {
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Put,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';

import { PayloadSuccessDto } from '../../common/dto/PayloadSuccessDto';
import { AuthUser } from '../../decorators/auth-user.decorator';
import { UserEntity } from '../../entities/user.entity';
import { AuthGuard } from '../../guards/auth.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
import { DeleteNotificationDto } from './dto/DeleteNotification.dto';
import { NotificationPageOptionsDto } from './dto/NotificationPageOptionsDto';
import { SeenNotificationDto } from './dto/SeenNotification.dto';
import { NotificationService } from './notification.service';

@Controller('notification')
@ApiTags('Notification')
export class NotificationController {
  constructor(private _notificationService: NotificationService) {}

  @Get('/count')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async countUserNotification(
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const nbUserNotification = await this._notificationService.countUserNotification(
      user.id,
    );
    return {
      message: 'count user notification',
      data: nbUserNotification,
    };
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async getUserNotification(
    @Query()
    notificationPageOption: NotificationPageOptionsDto,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    notificationPageOption.userId = user.id;
    const notifs = await this._notificationService.getUserNotification(
      notificationPageOption,
    );
    return {
      message: 'notification list',
      data: notifs.toDtos(),
    };
  }

  @Delete('/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @ApiOkResponse({
    description: 'notification has been removed',
  })
  async deleteNotification(
    @AuthUser() user: UserEntity,
    @Param('id') notificationId,
  ): Promise<PayloadSuccessDto> {
    const deleteNotificationDto = new DeleteNotificationDto();
    deleteNotificationDto.userId = user.id;
    deleteNotificationDto.notificationId = notificationId;
    await this._notificationService.deleteNotification(
      deleteNotificationDto,
    );
    return { message: 'friend has been deleted' };
  }

  @Put(':id/seen')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @ApiOkResponse({
    description: 'notification has been seen',
  })
  async notificationSeen(
    @AuthUser() user: UserEntity,
    @Param('id') notificationId,
  ): Promise<PayloadSuccessDto> {
    const seenNotificationDto = new SeenNotificationDto();
    seenNotificationDto.userId = user.id;
    seenNotificationDto.notificationId = notificationId;
    await this._notificationService.notificationSeen(
      seenNotificationDto,
    );
    return { message: 'notification seen' };
  }
}
