import { forwardRef, Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';

import { FirebaseAuthRepository } from '../../repositories/firebaseAuth.repository';
import { UserRepository } from '../../repositories/user.repository';
import { VerificationCodeRepository } from '../../repositories/verificationCode.repository';
import { UserModule } from '../user/user.module';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { FacebookStrategy } from './strategy/facebook.strategy';
import { JwtStrategy } from './strategy/jwt.strategy';

@Module({
  imports: [
    forwardRef(() => UserModule),
    TypeOrmModule.forFeature([
      VerificationCodeRepository,
      UserRepository,
      FirebaseAuthRepository,
    ]),
    PassportModule.register({
      defaultStrategy: ['jwt', 'facebook-token'],
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, FacebookStrategy],
  exports: [
    PassportModule.register({
      defaultStrategy: ['jwt', 'facebook-token'],
    }),
    AuthService,
  ],
})
export class AuthModule {}
