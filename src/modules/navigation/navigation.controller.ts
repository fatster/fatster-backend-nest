import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiParam,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

import { PayloadSuccessDto } from '../../common/dto/PayloadSuccessDto';
import { CoachTypeEnum } from '../../common/enum/enum';
import { AuthUser } from '../../decorators/auth-user.decorator';
import { UserEntity } from '../../entities/user.entity';
import { AuthGuard } from '../../guards/auth.guard';
import { RolesGuard } from '../../guards/roles.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
import { GetFriendsSuggestionsDto } from './dto/getFriendsSuggestions.dto';
import { RequestCatDto } from './dto/requestCat.dto';
import { NavigationService } from './navigation.service';

@Controller('navigation')
@ApiTags('Navigation')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(AuthUserInterceptor)
@ApiBearerAuth()
export class NavigationController {
  constructor(private _navigationService: NavigationService) {}

  // OK
  @Get('coach/categories')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiResponse({
    description: 'Get coach categories',
  })
  async getCoachCategories() {
    const result = await this._navigationService.getCategories(
      'coach',
    );
    return { message: 'Coach categories', data: result };
  }

  // OK
  @Get('article/categories')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiResponse({
    description: 'Get article categories',
  })
  async getArticleCategories() {
    const result = await this._navigationService.getCategories(
      'article',
    );
    return { message: 'Article categories', data: result };
  }

  @Get('mainPictures/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiParam({ name: 'id', type: String })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get main picture by id',
  })
  async getMainPictureById(@Param('id') id: string) {
    const result = await this._navigationService.getMainPictureById(
      id,
    );
    return { message: 'Main pictures', data: result };
  }

  @Get('tip/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @ApiResponse({
    description: 'Get Coach tips by id',
  })
  async getTipById(@Param('id') id: string) {
    const result = await this._navigationService.getTipById(id);
    return { message: 'Coach articles', data: result };
  }

  @Get('article/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get article by id',
  })
  async getArticleById(@Param('id') articleId: string) {
    const result = await this._navigationService.getArticleById(
      articleId,
    );
    return { message: 'Article', data: result };
  }

  // OK
  @Get('tipByType')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get Coach tips by type',
  })
  async getTipByType(): Promise<PayloadSuccessDto> {
    const result = await this._navigationService.getByType(
      CoachTypeEnum.TIP,
    );
    return { message: 'Coach tips', data: result };
  }

  // OK
  @Get('articleByType')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get featured articles by type',
  })
  async getArticlesByType() {
    const result = await this._navigationService.getArticlesByType();
    return { message: 'Featured articles', data: result };
  }

  // OK
  @Get('tipByCategory')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get tips by type',
  })
  async getTipByCategory(
    @Query()
    request: RequestCatDto,
  ) {
    const result = await this._navigationService.getByCategory(
      CoachTypeEnum.TIP,
      request,
    );
    return { message: 'tips by type', data: result };
  }

  // OK
  @Get('articles/category')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get articles by type',
  })
  async getArticlesByCategory(
    @Query()
    request: RequestCatDto,
  ) {
    const result = await this._navigationService.getArticleByCategory(
      request,
    );
    return { message: 'Articles', data: result };
  }

  // OK
  @Post('tip/:id/like')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Like a tip',
  })
  async likeTip(
    @Param('id') articleId: string,
  ): Promise<PayloadSuccessDto> {
    const result = await this._navigationService.likeTip(articleId);
    return { message: 'tip liked', data: result };
  }

  @Post('article/:id/like')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Like an article',
  })
  async likeArticle(
    @Param('id') articleId: string,
  ): Promise<PayloadSuccessDto> {
    const result = await this._navigationService.likeArticle(
      articleId,
    );
    return { message: 'Article liked', data: result };
  }

  // OK
  @Get('mainPictures')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get main pictures',
  })
  async getMainPictures() {
    const result = await this._navigationService.getMainPictures();
    return { message: 'Main pictures', data: result };
  }

  @Get('/friendsSuggestions')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get friends suggestions.',
  })
  async getFriendsSuggestion(
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const getFriendsSuggestionsDto = new GetFriendsSuggestionsDto(
      user,
    );

    const friendsSuggestions = await this._navigationService.getFriendsSuggestions(
      getFriendsSuggestionsDto,
    );
    return {
      message: 'Friends suggestions',
      data: { friendsSuggestions },
    };
  }
}
