import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { FoodEntity } from '../entities/food.entity';
import { FoodCategoryDto } from './foodCategory.dto';
import { UserNutritionDto } from './userNutrition.dto';

export class FoodDto extends AbstractDto {
  @ApiPropertyOptional()
  nameFr: string;

  @ApiPropertyOptional()
  nameEn: string;

  @ApiPropertyOptional()
  userNutrition: UserNutritionDto;

  @ApiPropertyOptional()
  foodCategory: FoodCategoryDto;

  @ApiPropertyOptional()
  energyEuRegulation: number;

  @ApiPropertyOptional()
  energyNJonesFactorWithFibres: number;

  @ApiPropertyOptional()
  energyNJonesFactorWithFibres2: number;

  @ApiPropertyOptional()
  water: number;

  @ApiPropertyOptional()
  protein: number;

  @ApiPropertyOptional()
  crudeProtein: number;

  @ApiPropertyOptional()
  carbohydrates: number;

  @ApiPropertyOptional()
  fat: number;

  @ApiPropertyOptional()
  sugars: number;

  @ApiPropertyOptional()
  starch: number;

  @ApiPropertyOptional()
  dietaryFibre: number;

  @ApiPropertyOptional()
  totalPolyols: number;

  @ApiPropertyOptional()
  ash: number;

  @ApiPropertyOptional()
  alcohol: number;

  @ApiPropertyOptional()
  organicAcids: number;

  @ApiPropertyOptional()
  saturated: number;

  @ApiPropertyOptional()
  monounsaturated: number;

  @ApiPropertyOptional()
  polyunsaturated: number;

  @ApiPropertyOptional()
  butyric: number;

  @ApiPropertyOptional()
  caproic: number;

  @ApiPropertyOptional()
  caprylic: number;

  @ApiPropertyOptional()
  capric: number;

  @ApiPropertyOptional()
  lauric: number;

  @ApiPropertyOptional()
  myristic: number;

  @ApiPropertyOptional()
  palmitic: number;

  @ApiPropertyOptional()
  stearic: number;

  @ApiPropertyOptional()
  oleic: number;

  @ApiPropertyOptional()
  linoleic: number;

  @ApiPropertyOptional()
  alphaLinolenic: number;

  @ApiPropertyOptional()
  arachidonic: number;

  @ApiPropertyOptional()
  epa: number;

  @ApiPropertyOptional()
  dha: number;

  @ApiPropertyOptional()
  cholesterol: number;

  @ApiPropertyOptional()
  sodiumChlorideSalt: number;

  @ApiPropertyOptional()
  calcium: number;

  @ApiPropertyOptional()
  chloride: number;

  @ApiPropertyOptional()
  copper: number;

  @ApiPropertyOptional()
  iron: number;

  @ApiPropertyOptional()
  iodine: number;

  @ApiPropertyOptional()
  magnesium: number;

  @ApiPropertyOptional()
  manganese: number;

  @ApiPropertyOptional()
  manganese2: number;

  @ApiPropertyOptional()
  potassium: number;

  @ApiPropertyOptional()
  selenium: number;

  @ApiPropertyOptional()
  sodium: number;

  @ApiPropertyOptional()
  zinc: number;

  @ApiPropertyOptional()
  retinol: number;

  @ApiPropertyOptional()
  betaCarotene: number;

  @ApiPropertyOptional()
  vitaminD: number;

  @ApiPropertyOptional()
  vitaminE: number;

  @ApiPropertyOptional()
  vitaminK1: number;

  @ApiPropertyOptional()
  vitaminK2: number;

  @ApiPropertyOptional()
  vitaminC: number;

  @ApiPropertyOptional()
  vitaminB1: number;

  @ApiPropertyOptional()
  vitaminB2: number;

  @ApiPropertyOptional()
  vitaminB3: number;

  @ApiPropertyOptional()
  vitaminB5: number;

  @ApiPropertyOptional()
  vitaminB6: number;

  @ApiPropertyOptional()
  vitaminB9: number;

  @ApiPropertyOptional()
  vitaminB12: number;

  constructor(food: FoodEntity) {
    super(food);
    Object.assign(this, food);
  }
}
