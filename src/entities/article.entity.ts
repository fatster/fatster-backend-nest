import {
  AfterLoad,
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { ArticleDto } from '../dto/article.dto';
import { ArticleCategoryEntity } from './articleCategory.entity';
import { NavigationMainPictureEntity } from './navigationMainPicture.entity';
import { PostEntity } from './post.entity';
import { UserEntity } from './user.entity';

@Entity({ name: 'article' })
export class ArticleEntity extends AbstractEntity<ArticleDto> {
  @Column()
  nameEn: string;

  @Column()
  descriptionEn: string;

  @Column()
  nameFr: string;

  @Column()
  descriptionFr: string;

  @Column({ nullable: true })
  imageId: string;

  @Column()
  url: string;

  @Column({
    default: 0,
  })
  likesCount?: number;

  @Column({
    default: 0,
  })
  readersCount?: number;

  @OneToMany(() => UserEntity, (user) => user.id, {
    onDelete: 'CASCADE',
  })
  readers: UserEntity[];

  @ManyToOne(() => PostEntity, (post) => post.article)
  post: PostEntity;

  @ManyToOne(
    () => ArticleCategoryEntity,
    (category) => category.article,
  )
  articleCategory: ArticleCategoryEntity;

  @OneToOne(
    () => NavigationMainPictureEntity,
    (navigationMainPicture) => navigationMainPicture.article,
  )
  navigationMainPicture: NavigationMainPictureEntity;

  @AfterLoad()
  prefixWithUrl() {
    if (this.imageId) {
      this.imageId = `${process.env.S3_BASE_URL}${this.imageId}?auto=compress&q=20`;
    }
  }

  dtoClass = ArticleDto;
}
