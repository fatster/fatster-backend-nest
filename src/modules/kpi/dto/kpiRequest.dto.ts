'use strict';

import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class KpirequestDto {
  @IsString()
  @IsNotEmpty()
  @ApiPropertyOptional()
  startDate: Date;

  @IsString()
  @IsNotEmpty()
  @ApiPropertyOptional()
  endDate: Date;

  constructor(startDate: Date, endDate: Date) {
    this.startDate = startDate;
    this.endDate = endDate;
  }
}
