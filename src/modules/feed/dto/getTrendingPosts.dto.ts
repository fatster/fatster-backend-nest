import {
  ApiHideProperty,
  ApiPropertyOptional,
} from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

import { PageOptionsDto } from '../../../common/dto/PageOptionsDto';
import { UserEntity } from '../../../entities/user.entity';

export class GetTrendingPostsDto extends PageOptionsDto {
  @ApiHideProperty()
  user: UserEntity;

  @ApiPropertyOptional()
  @IsOptional()
  withPrivacy?: boolean;
}
