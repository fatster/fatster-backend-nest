import { Injectable } from '@nestjs/common';
import { isEmpty } from 'lodash';

import { PageMetaDto } from '../../common/dto/PageMetaDto';
import { ResultPageDto } from '../../common/dto/ResultPage.dto';
import {
  CoachTypeEnum,
  StatusFriendRequestEnum,
} from '../../common/enum/enum';
import { ArticleRepository } from '../../repositories/article.repository';
import { ArticleCategoryRepository } from '../../repositories/articleCategory.repository';
import { CoachRepository } from '../../repositories/coach.repository';
import { CoachCategoryRepository } from '../../repositories/coachCategory.repository';
import { FriendRepository } from '../../repositories/friend.repository';
import { FriendRequestRepository } from '../../repositories/friendRequest.repository';
import { NavigationMainPictureRepository } from '../../repositories/NavigationMainPicture.repository';
import { UserRepository } from '../../repositories/user.repository';
import { addDays } from '../../utils/date';
import { GetFriendsSuggestionsDto } from './dto/getFriendsSuggestions.dto';
import { RequestCatDto } from './dto/requestCat.dto';

@Injectable()
export class NavigationService {
  constructor(
    private readonly _articleRepository: ArticleRepository,
    private readonly _articleCategoryRepository: ArticleCategoryRepository,
    private readonly _coachRepository: CoachRepository,
    private readonly _coachCategoryRepository: CoachCategoryRepository,
    private readonly _userRepository: UserRepository,
    private readonly _friendRequestRepository: FriendRequestRepository,
    private readonly _friendRepository: FriendRepository,
    private readonly _navigationMainPictureRepository: NavigationMainPictureRepository,
  ) {}

  async getCategories(type: string) {
    const repository =
      type === 'coach'
        ? this._coachCategoryRepository
        : this._articleCategoryRepository;

    return repository
      .createQueryBuilder('categories')
      .select([
        'categories.id',
        'categories.typeEn',
        'categories.typeFr',
      ])
      .getMany();
  }

  async getMainPictureById(id: string) {
    return this._navigationMainPictureRepository.findOne(id);
  }

  async getTipById(tipId: string) {
    const tip = await this._coachRepository.findOne(tipId, {
      where: { type: 'tip' },
    });
    tip.readersCount += 1;
    return this._coachRepository.save(tip);
  }

  async getArticleById(articleId: string) {
    const article = await this._articleRepository.findOne(articleId);
    article.readersCount += 1;
    return this._articleRepository.save(article);
  }

  async likeTip(tipId: string) {
    const tip = await this._coachRepository.findOne(tipId, {
      where: { type: 'tip' },
    });
    tip.likesCount += 1;
    return this._coachRepository.save(tip);
  }

  async likeChallenge(challengeId: string) {
    const challenge = await this._coachRepository.findOne(
      challengeId,
      { where: { type: 'challenge' } },
    );
    challenge.likesCount += 1;
    return this._coachRepository.save(challenge);
  }

  async likeArticle(articleId: string) {
    const article = await this._articleRepository.findOne(articleId);
    article.likesCount += 1;
    return this._articleRepository.save(article);
  }

  async getByType(type: CoachTypeEnum) {
    const result = [];
    const categories = await this._coachCategoryRepository
      .createQueryBuilder('coachCategory')
      .getMany();
    for await (const category of categories) {
      const content = await this._coachRepository
        .createQueryBuilder('coach')
        .leftJoin('coach.coachCategory', 'coachCategory')
        .select([
          'coach.id',
          'coach.nameEn',
          'coach.nameFr',
          'coach.imageId',
        ])
        .where('coach.type = :type', { type })
        .andWhere('coachCategory.id = :id', { id: category.id })
        // .orderBy('(coach.likesCount + coach.readersCount)/2', 'DESC')
        .orderBy('random()')
        .limit(5)
        .getMany();

      if (!isEmpty(content)) {
        result.push({
          id: category.id,
          createdAt: category.createdAt,
          updatedAt: category.updatedAt,
          typeEn: category.typeEn,
          typeFr: category.typeFr,
          data: content,
        });
      }
    }

    return result;
  }

  async getByCategory(type: CoachTypeEnum, request: RequestCatDto) {
    const response = await this._coachRepository
      .createQueryBuilder('coach')
      .leftJoin('coach.coachCategory', 'coachCategory')
      .select([
        'coach.id',
        'coach.nameEn',
        'coach.nameFr',
        'coach.imageId',
      ])
      .where('coach.type = :type', { type })
      .andWhere('coachCategory.id = :categoryId', {
        categoryId: request.categoryId,
      })
      .skip(request.skip)
      .take(request.take)
      .getMany();

    const pageMetaDto = new PageMetaDto({
      pageOptionsDto: request,
      itemCount: response.length,
    });

    return new ResultPageDto(response, pageMetaDto);
  }

  async getArticlesByType() {
    const result = [];
    const categories = await this._articleCategoryRepository
      .createQueryBuilder('articleCategory')
      .getMany();
    for await (const category of categories) {
      const content = await this._articleRepository
        .createQueryBuilder('article')
        .leftJoin('article.articleCategory', 'articleCategory')
        .select([
          'article.id',
          'article.nameEn',
          'article.nameFr',
          'article.imageId',
        ])
        .andWhere('articleCategory.id = :id', { id: category.id })
        .orderBy('random()')
        .limit(5)
        .getMany();

      if (!isEmpty(content)) {
        result.push({
          id: category.id,
          createdAt: category.createdAt,
          updatedAt: category.updatedAt,
          typeEn: category.typeEn,
          typeFr: category.typeFr,
          data: content,
        });
      }
    }

    return result;
  }

  async getArticleByCategory(request: RequestCatDto) {
    const response = await this._articleRepository
      .createQueryBuilder('article')
      .leftJoin('article.articleCategory', 'articleCategory')
      .select([
        'article.id',
        'article.nameEn',
        'article.nameFr',
        'article.imageId',
      ])
      .andWhere('articleCategory.id = :categoryId', {
        categoryId: request.categoryId,
      })
      .skip(request.skip)
      .take(request.take)
      .getMany();

    const pageMetaDto = new PageMetaDto({
      pageOptionsDto: request,
      itemCount: response.length,
    });

    return new ResultPageDto(response, pageMetaDto);
  }

  async getMainPictures() {
    return this._navigationMainPictureRepository
      .createQueryBuilder('article')
      .where('article.isActive = :true', { true: true })
      .loadAllRelationIds()
      .getMany();
  }

  async getFriendsSuggestions(
    getFriendsSuggestionsDto: GetFriendsSuggestionsDto,
  ) {
    const friends = await this._friendRepository
      .createQueryBuilder('relationShip')
      .where(`user_id = '${getFriendsSuggestionsDto.user.id}'`)
      .leftJoinAndSelect('relationShip.friend', 'friend')
      .getMany();

    const friendRequests = await this._friendRequestRepository
      .createQueryBuilder('friendRequests')
      .where([
        { from: getFriendsSuggestionsDto.user },
        { to: getFriendsSuggestionsDto.user },
      ])
      .leftJoinAndSelect('friendRequests.from', 'from')
      .leftJoinAndSelect('friendRequests.to', 'to')
      .getMany();

    const rejectedIds = [
      getFriendsSuggestionsDto.user.id,
      ...friends.map((x) => x.friend.id),
      ...friendRequests
        .filter((x) => x.status !== StatusFriendRequestEnum.ACCEPTED)
        .map((x) =>
          x.from.id === getFriendsSuggestionsDto.user.id
            ? x.to.id
            : x.from.id,
        ),
    ];

    const suggestedUsers = await this._userRepository
      .createQueryBuilder('users')
      .select([
        'users.id',
        'users.pseudo',
        'users.photoId',
        'users.gender',
        'users.targetLooseWeight',
        'users.birthDate',
        'COUNT(posts.id) AS countPost',
      ])
      .where('users.profile_completed =:isCompleted', {
        isCompleted: true,
      })
      .andWhere('users.id NOT IN (:...rejectedIds)', {
        rejectedIds,
      })
      .andWhere('users.updatedAt > :updatedAt', {
        updatedAt: addDays(new Date(), -7),
      })
      .leftJoin('users.posts', 'posts')
      .groupBy('users.id')
      .orderBy('countPost', 'DESC')
      .getMany();

    // Get 60KG EN TROP
    const laGrosseVacheDe60kgEnTrop = await this._userRepository.findOne(
      {
        where: {
          id: '31c8a802-098a-4621-a279-a8a2a3cf76e4',
        },
      },
    );
    // IF SHE'S NOT MY FRIEND
    if (
      laGrosseVacheDe60kgEnTrop &&
      friends.filter(
        (friend) => friend.user === <any>laGrosseVacheDe60kgEnTrop.id,
      ).length === 0
    ) {
      // ADD TO SUGGESTION
      suggestedUsers.unshift(laGrosseVacheDe60kgEnTrop);
    }

    return suggestedUsers.slice(0, 10);
  }
}
