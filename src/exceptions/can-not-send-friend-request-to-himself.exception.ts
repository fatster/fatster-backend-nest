'use strict';

import { BadRequestException } from '@nestjs/common';

export class CanNotSendFriendRequestToHimselfException extends BadRequestException {
  constructor(error?: string) {
    super('error.can_not_send_friend_request_to_himself', error);
  }
}
