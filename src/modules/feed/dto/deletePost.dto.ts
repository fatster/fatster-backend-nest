'use strict';

import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class DeletePostDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly postId: string;

  @ApiHideProperty()
  userId: string;
}
