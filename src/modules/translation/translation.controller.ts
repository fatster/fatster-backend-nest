import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiParam, ApiTags } from '@nestjs/swagger';

import { AuthGuard } from '../../guards/auth.guard';
import { LangVersion } from '../../shared/services/translation.service';
import { GetTranslationDto } from './translation.dto';
import { TranslationEndpointService } from './translation.service';

@Controller('translation')
@ApiTags('Translation')
export class TranslationController {
  constructor(
    private _translationEndpointService: TranslationEndpointService,
  ) {}

  @Get('/')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  async getLocale(@Query() getTranslationDto: GetTranslationDto) {
    const translation = await this._translationEndpointService.getTranslation(
      getTranslationDto.id,
      getTranslationDto.disableCache,
      getTranslationDto.version,
    );

    return { message: 'translation', data: translation };
  }

  @Get('/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  async getTranslation(@Param('id') id) {
    const translation = await this._translationEndpointService.getTranslation(
      id,
      false,
      LangVersion.V2,
    );
    return { message: 'translation', data: translation };
  }
}
