import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsEnum,
  IsIn,
  IsNotEmpty,
  IsNumber,
  IsUUID,
} from 'class-validator';

import { NutritionFoodType } from '../../../common/enum/enum';

const allGroups = Object.values(NutritionFoodType);

export class CreateNutritionFoodDto {
  @IsNotEmpty({ groups: allGroups })
  @IsEnum(NutritionFoodType, { groups: allGroups })
  @IsIn(allGroups)
  @ApiProperty({ enum: allGroups })
  readonly portionType: NutritionFoodType;

  @IsNotEmpty({ groups: allGroups })
  @IsUUID('all', { groups: allGroups })
  @ApiProperty()
  readonly id: string;

  @ApiPropertyOptional()
  @IsNotEmpty({ groups: [NutritionFoodType.Calories] })
  @IsNumber({}, { groups: [NutritionFoodType.Calories] })
  readonly calories: number;

  @ApiPropertyOptional()
  @IsNotEmpty({ groups: [NutritionFoodType.Portions] })
  @IsNumber({}, { groups: [NutritionFoodType.Portions] })
  readonly portion: number;

  @ApiPropertyOptional()
  @IsNotEmpty({ groups: [NutritionFoodType.Grams] })
  @IsNumber({}, { groups: [NutritionFoodType.Grams] })
  readonly grams: number;

  @ApiPropertyOptional()
  @IsNotEmpty({
    groups: [NutritionFoodType.Grams, NutritionFoodType.Portions],
  })
  @IsNumber(
    {},
    { groups: [NutritionFoodType.Grams, NutritionFoodType.Portions] },
  )
  readonly energyEuRegulation: number;
}
