import { Column, Entity, ManyToOne } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { FoodDto } from '../dto/food.dto';
import { FoodCategoryEntity } from './foodCategory.entity';

@Entity({ name: 'food' })
export class FoodEntity extends AbstractEntity<FoodDto> {
  @Column()
  textFr: string;

  @Column()
  textEn: string;

  @ManyToOne(
    () => FoodCategoryEntity,
    (userNutrition) => userNutrition.foods,
  )
  foodCategory: FoodCategoryEntity;

  @Column({ type: 'float', nullable: true })
  energyEuRegulation: number;

  @Column({ type: 'float', nullable: true })
  energyNJonesFactorWithFibres: number;

  @Column({ type: 'float', nullable: true })
  water: number;

  @Column({ type: 'float', nullable: true })
  protein: number;

  @Column({ type: 'float', nullable: true })
  carbohydrates: number;

  @Column({ type: 'float', nullable: true })
  fat: number;

  @Column({ type: 'float', nullable: true })
  sugars: number;

  @Column({ type: 'float', nullable: true })
  starch: number;

  @Column({ type: 'float', nullable: true })
  dietaryFibre: number;

  @Column({ type: 'float', nullable: true })
  alcohol: number;

  @Column({ type: 'float', nullable: true })
  organicAcids: number;

  @Column({ type: 'float', nullable: true })
  saturated: number;

  @Column({ type: 'float', nullable: true })
  monounsaturated: number;

  @Column({ type: 'float', nullable: true })
  polyunsaturated: number;

  @Column({ type: 'float', nullable: true })
  arachidonic: number;

  @Column({ type: 'float', nullable: true })
  epa: number;

  @Column({ type: 'float', nullable: true })
  dha: number;

  @Column({ type: 'float', nullable: true })
  cholesterol: number;

  @Column({ type: 'float', nullable: true })
  sodiumChlorideSalt: number;

  @Column({ type: 'float', nullable: true })
  calcium: number;

  @Column({ type: 'float', nullable: true })
  chloride: number;

  @Column({ type: 'float', nullable: true })
  copper: number;

  @Column({ type: 'float', nullable: true })
  iron: number;

  @Column({ type: 'float', nullable: true })
  iodine: number;

  @Column({ type: 'float', nullable: true })
  magnesium: number;

  @Column({ type: 'float', nullable: true })
  manganese: number;

  @Column({ type: 'float', nullable: true })
  potassium: number;

  @Column({ type: 'float', nullable: true })
  selenium: number;

  @Column({ type: 'float', nullable: true })
  sodium: number;

  @Column({ type: 'float', nullable: true })
  zinc: number;

  @Column({ type: 'float', nullable: true })
  betaCarotene: number;

  @Column({ type: 'float', nullable: true })
  vitaminD: number;

  @Column({ type: 'float', nullable: true })
  vitaminE: number;

  @Column({ type: 'float', nullable: true })
  vitaminK1: number;

  @Column({ type: 'float', nullable: true })
  vitaminK2: number;

  @Column({ type: 'float', nullable: true })
  vitaminC: number;

  @Column({ type: 'float', nullable: true })
  vitaminB1: number;

  @Column({ type: 'float', nullable: true })
  vitaminB2: number;

  @Column({ type: 'float', nullable: true })
  vitaminB3: number;

  @Column({ type: 'float', nullable: true })
  vitaminB5: number;

  @Column({ type: 'float', nullable: true })
  vitaminB6: number;

  @Column({ type: 'float', nullable: true })
  vitaminB9: number;

  @Column({ type: 'float', nullable: true })
  vitaminB12: number;

  dtoClass = FoodDto;
}
