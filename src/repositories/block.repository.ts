import { EntityRepository, Repository } from 'typeorm';

import { BlockEntity } from '../entities/block.entity';

@EntityRepository(BlockEntity)
export class BlockRepository extends Repository<BlockEntity> {}
