'use strict';

import { NotFoundException } from '@nestjs/common';

export class InvalidVerificationCodeException extends NotFoundException {
  constructor(error?: string) {
    super('Invalid or disabled verification code', error);
  }
}
