import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class UpdateProfilePictureDto {
  @IsString()
  @ApiProperty()
  fileName: string;
}
