import { Column, Entity } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { HashtagEntityType } from '../common/enum/enum';
import { HashtagDto } from '../dto/hashtag.dto';

@Entity({ name: 'hashtag' })
export class HashtagEntity extends AbstractEntity<HashtagDto> {
  @Column()
  keyName: string;

  @Column()
  label: string;

  @Column()
  color: string;

  @Column({
    type: 'enum',
    enum: HashtagEntityType,
  })
  entity: HashtagEntityType;

  dtoClass = HashtagDto;
}
