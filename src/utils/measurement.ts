import { MeasureSystem } from '../common/enum/enum';

export function getWeightInKG(weight: number, system: MeasureSystem) {
  switch (system) {
    case MeasureSystem.Imperial: {
      const kg = weight / 2.205;
      return Math.round(kg * 100) / 100;
    }
    case MeasureSystem.Metric:
      return weight;
  }
}
