import { BadRequestException, Injectable } from '@nestjs/common';
import { uniqBy } from 'lodash';

import { BadgeEnum } from '../../common/enum/enum';
import { BadgeRepository } from '../../repositories/badge.repository';
import { PostRepository } from '../../repositories/post.repository';
import { UserRepository } from '../../repositories/user.repository';

const ProENName = 'Pro';
const AmbassadorENName = 'Ambassador';

@Injectable()
export class BadgeService {
  constructor(
    private readonly _userRepository: UserRepository,
    private readonly _postRepository: PostRepository,
    private readonly _badgeRepository: BadgeRepository,
  ) {}

  public async getAll() {
    return this._badgeRepository.find();
  }

  public async addBadgeToUser(userId: string, badgeName: BadgeEnum) {
    const [badge, user] = await Promise.all([
      this._findOneByName(badgeName),
      this._userRepository.findOne(userId, {
        relations: ['badges', 'posts'],
      }),
    ]);

    if (!badge) {
      throw new BadRequestException('badge not found');
    }

    if (!user) {
      throw new BadRequestException('user not found');
    }

    user.badges = uniqBy([...user.badges, badge], (b) => b.id);
    const updateUser = await this._userRepository.save(user);

    await this._postRepository.save(
      user.posts.map((p) => ({
        ...p,
        hashtags: Array.from(
          new Set([...p.hashtags, badge.nameEn.toLowerCase()]),
        ),
      })),
    );

    return updateUser;
  }

  private async _ProBadge() {
    return this._badgeRepository.findOne({ nameEn: ProENName });
  }

  private async _AmbassadorBadge() {
    return this._badgeRepository.findOne({
      nameEn: AmbassadorENName,
    });
  }

  private async _findOneByName(badgeName: BadgeEnum) {
    switch (badgeName) {
      case BadgeEnum.PRO:
        return this._ProBadge();
      case BadgeEnum.AMBASSADOR:
        return this._AmbassadorBadge();
    }
  }
}
