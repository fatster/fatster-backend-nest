export const minutesToHours = (num: number) => {
  if (num) {
    const hours = Math.floor(num / 60);
    const minutes = ('0' + (num % 60)).slice(-2);
    const formatedMinutes = Number(minutes);
    return formatedMinutes > 0 ? `${hours}h${minutes}` : `${hours}`;
  }
  return '0h';
};

export const minutesToHoursFeed = (num: number) => {
  if (num) {
    const hours = Math.floor(num / 60);
    const minutes = ('0' + (num % 60)).slice(-2);
    const formatedMinutes = Number(minutes);
    return formatedMinutes > 0 ? `${hours}h${minutes}` : `${hours}h`;
  }
  return '0h';
};
