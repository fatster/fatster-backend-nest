import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

import { PayloadSuccessDto } from '../../common/dto/PayloadSuccessDto';
import { AuthUser } from '../../decorators/auth-user.decorator';
import { UserEntity } from '../../entities/user.entity';
import { AuthGuard } from '../../guards/auth.guard';
import { RolesGuard } from '../../guards/roles.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
import { RessourceService } from './ressource.service';

@Controller('ressource')
@ApiTags('Ressource')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(AuthUserInterceptor)
@ApiBearerAuth()
export class RessourceController {
  constructor(private _ressourceService: RessourceService) {}

  @Get('food')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  async getFoods(
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const data = await this._ressourceService.getFood(user.id);
    return {
      data,
      message: 'food lists',
    };
  }

  @Get('activity')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  async getActivity(
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const data = await this._ressourceService.getActivity(user.id);
    return {
      data,
      message: 'activity lists',
    };
  }
}
