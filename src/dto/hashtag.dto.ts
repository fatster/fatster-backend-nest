import { ApiProperty } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { HashtagEntityType } from '../common/enum/enum';
import { HashtagEntity } from '../entities/hashtag.entity';

export class HashtagDto extends AbstractDto {
  @ApiProperty({ enum: HashtagEntityType })
  entity: HashtagEntityType;

  @ApiProperty()
  keyName: string;

  @ApiProperty()
  label: string;

  @ApiProperty()
  color: string;

  constructor(hashtag: HashtagEntity) {
    super(hashtag);
    this.entity = hashtag.entity;
    this.keyName = hashtag.keyName;
    this.color = hashtag.color;
    this.label = hashtag.label;
  }
}
