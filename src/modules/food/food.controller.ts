import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';

import { PayloadSuccessDto } from '../../common/dto/PayloadSuccessDto';
import { FoodService } from './food.service';

@Controller('food')
@ApiTags('Food')
export class FoodController {
  constructor(private _foodService: FoodService) {}

  @Get('/')
  @HttpCode(HttpStatus.OK)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get all food',
  })
  async getUsersPosts(): Promise<PayloadSuccessDto> {
    const hashtags = await this._foodService.getAllFood();

    return {
      message: 'All food',
      data: hashtags,
    };
  }

  @Post('/import')
  @HttpCode(HttpStatus.OK)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get all food',
  })
  @UseInterceptors(FileInterceptor('file'))
  async importCSVFood(
    @UploadedFile() file,
  ): Promise<PayloadSuccessDto> {
    const input = file.buffer.toString('utf-8');

    const importedFood = await this._foodService.importFood(input);

    return {
      message: 'All food',
      data: importedFood,
    };
  }
}
