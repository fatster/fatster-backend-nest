import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { CoachDto } from './coach.dto';

export class CoachCategoryDto extends AbstractDto {
  @ApiPropertyOptional()
  typeEn: string;

  @ApiPropertyOptional()
  typeFr: string;

  @ApiPropertyOptional()
  coach: CoachDto[];
}
