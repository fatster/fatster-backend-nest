import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { BeforeAfterEntity } from '../entities/beforeAfter.entity';
// import { isDtos } from '../common/utils/entities';
import { PostDto } from './post.dto';

export class BeforeAfterDto extends AbstractDto {
  @ApiPropertyOptional()
  weightBefore: number;

  @ApiPropertyOptional()
  weightAfter: number;

  @ApiPropertyOptional()
  weightLost: string;

  @ApiPropertyOptional()
  post: PostDto[];

  constructor(beforeAfter: BeforeAfterEntity) {
    super(beforeAfter);
    this.weightBefore = beforeAfter.weightBefore;
    this.weightAfter = beforeAfter.weightAfter;
    this.weightLost = parseFloat(
      (beforeAfter.weightAfter - beforeAfter.weightBefore).toFixed(1),
    ).toString();
    this.weightLost = this.format(this.weightLost);
    /*this.post = isDtos(beforeAfter.post)
      ? post.users.toDtos()
      : <any>badge.users;*/
  }

  format = (n) => (n > 0 ? '+' : '') + n;
}
