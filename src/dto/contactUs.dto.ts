import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';

export class ContactUsDto extends AbstractDto {
  @ApiPropertyOptional()
  userEmail: string;

  @ApiPropertyOptional()
  subject: string;

  @ApiPropertyOptional()
  text: string;
}
