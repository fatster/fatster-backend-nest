import { EntityRepository, Repository } from 'typeorm';

import { PlatformEnum } from '../common/enum/enum';
import { AppVersionEntity } from '../entities/appVersion.entity';

@EntityRepository(AppVersionEntity)
export class AppVersionRepository extends Repository<
  AppVersionEntity
> {
  public async findLastOne(platform: PlatformEnum) {
    return this.findOne({
      where: { platform },
      order: { createdAt: 'DESC' },
    });
  }
}
