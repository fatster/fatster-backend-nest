import { Entity, ManyToOne } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { FriendDto } from '../dto/friend.dto';
import { MessengerRoomEntity } from './messengerRoom.entity';
import { UserEntity } from './user.entity';

@Entity({ name: 'friend' })
export class FriendEntity extends AbstractEntity<FriendDto> {
  @ManyToOne(() => UserEntity, (user) => user.friends, {
    onDelete: 'CASCADE',
  })
  user: UserEntity;

  @ManyToOne(() => UserEntity, (user) => user.friends, {
    onDelete: 'CASCADE',
  })
  friend: UserEntity;

  @ManyToOne(
    () => MessengerRoomEntity,
    (messengerRoom) => messengerRoom.friend,
    { onDelete: 'CASCADE' },
  )
  messengerRoom: MessengerRoomEntity;

  dtoClass = FriendDto;
}
