import { IsNotEmpty, IsString } from 'class-validator';

export class FoodSuggestionDto {
  @IsString()
  @IsNotEmpty()
  readonly food: string;
}
