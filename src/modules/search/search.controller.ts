import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';

import { PayloadSuccessDto } from '../../common/dto/PayloadSuccessDto';
import { ResultPageDto } from '../../common/dto/ResultPage.dto';
import { AuthUser } from '../../decorators/auth-user.decorator';
import { UserEntity } from '../../entities/user.entity';
import { AuthGuard } from '../../guards/auth.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
import { SearchByEmailDto } from './dto/searchByEmail.dto';
import { SearchByPseudoDto } from './dto/searchByPseudo.dto';
import { UsersPageOptionsDto } from './dto/usersPageOptions.dto';
import { SearchService } from './search.service';

@Controller('search')
@ApiTags('Search')
export class SearchController {
  constructor(private _searchService: SearchService) {}

  @Post('byCriteria')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get users list',
  })
  async searchByCriteria(
    @Body() userCriteria: UsersPageOptionsDto,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const result = await this._searchService.searchByCriteria(
      userCriteria,
      user.id,
    );
    return { message: 'users list', data: result };
  }

  @Get('byPseudo')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get users list',
    type: ResultPageDto,
  })
  async searchByPseudo(
    @Query()
    request: SearchByPseudoDto,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const result = await this._searchService.searchByPseudo(
      request,
      user,
    );
    return { message: 'user list', data: result };
  }

  @Get('byEmail')
  @HttpCode(HttpStatus.OK)
  @ApiBearerAuth()
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get user list by email',
    type: ResultPageDto,
  })
  async searchByEmail(
    @Query()
    request: SearchByEmailDto,
  ): Promise<PayloadSuccessDto> {
    const result = await this._searchService.searchByEmail(request);
    return { message: 'user', data: result };
  }
}
