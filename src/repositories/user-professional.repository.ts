import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { ProfessionalInfoEntity } from '../entities/professionalInfo.entity';

@EntityRepository(ProfessionalInfoEntity)
export class UserProfessionalInfoRepository extends Repository<
  ProfessionalInfoEntity
> {}
