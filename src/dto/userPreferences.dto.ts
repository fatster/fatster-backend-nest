import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { isDto } from '../common/utils/entities';
import { UserPreferencesEntity } from '../entities/userPreferences.entity';
import { Language } from '../shared/services/translation.service';

export class UserPreferencesDto extends AbstractDto {
  // MY INFORMAITON
  @ApiPropertyOptional()
  showStatistics: boolean;

  @ApiPropertyOptional()
  showFeed: boolean;

  @ApiPropertyOptional()
  showPostsInPublicFeed: boolean;

  @ApiPropertyOptional()
  showPostsAppLanguageOnly: boolean;

  @ApiPropertyOptional()
  showProfile: boolean;

  @ApiPropertyOptional()
  showPseudoInSearch: boolean;

  @ApiPropertyOptional()
  language: Language;

  // PUSH NOTIFICATION
  @ApiPropertyOptional()
  pushNotificationLikes: boolean;

  @ApiPropertyOptional()
  pushNotificationComments: boolean;

  @ApiPropertyOptional()
  pushNotificationFriendRequest: boolean;

  @ApiPropertyOptional()
  pushNotificationNewFriend: boolean;

  @ApiPropertyOptional()
  pushNotificationMentionMessages: boolean;

  @ApiPropertyOptional()
  pushNotificationAdminMessages: 'all';

  // EMAILS
  @ApiPropertyOptional()
  newsLetter: boolean;

  @ApiPropertyOptional()
  adminEmails: boolean;

  user: any;

  constructor(userPreferences: UserPreferencesEntity) {
    super(userPreferences);
    Object.assign(this, userPreferences);
    this.user = isDto(userPreferences.user)
      ? userPreferences.user.toDto()
      : userPreferences.user;
  }
}
