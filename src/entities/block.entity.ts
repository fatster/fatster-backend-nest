import { Entity, ManyToOne } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { BlockDto } from '../dto/block.dto';
import { UserEntity } from './user.entity';

@Entity({ name: 'block' })
export class BlockEntity extends AbstractEntity<BlockDto> {
  @ManyToOne(() => UserEntity, (user) => user.blockedBy, {
    onDelete: 'CASCADE',
  })
  from: UserEntity;

  @ManyToOne(() => UserEntity, (user) => user.usersBlocked, {
    onDelete: 'CASCADE',
  })
  against: UserEntity;

  dtoClass = BlockDto;
}
