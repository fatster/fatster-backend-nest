'use strict';

import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class UpdateOneSignalPlayerDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly oneSignalPlayerId: string;

  @ApiHideProperty()
  userId: string;
}
