import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { FoodRepository } from '../../repositories/food.repository';
import { AuthModule } from '../auth/auth.module';
import { FoodController } from './food.controller';
import { FoodService } from './food.service';

@Module({
  imports: [
    forwardRef(() => AuthModule),
    TypeOrmModule.forFeature([FoodRepository]),
  ],
  controllers: [FoodController],
  exports: [FoodService],
  providers: [FoodService],
})
export class FoodModule {}
