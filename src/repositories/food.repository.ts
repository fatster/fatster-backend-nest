import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { FoodEntity } from '../entities/food.entity';

@EntityRepository(FoodEntity)
export class FoodRepository extends Repository<FoodEntity> {}
