import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { CalendarEntity } from './calendar.entity';

@EntityRepository(CalendarEntity)
export class CalendarRepository extends Repository<CalendarEntity> {}
