import { BadRequestException, Injectable } from '@nestjs/common';
import { toLower } from 'lodash';
import { Between, In } from 'typeorm';

import {
  MediaTypeEnum,
  NotificationEnum,
  PostStatusEnum,
  PostTypeEnum,
  TypeUploadEnum,
  UserRoleEnum,
} from '../../../common/enum/enum';
import { PostEntity } from '../../../entities/post.entity';
import { UserEntity } from '../../../entities/user.entity';
import { PostNotFoundException } from '../../../exceptions/post-not-found.exception';
import { SimpleTextMustHaveAtLeastATextOrAPhoto } from '../../../exceptions/simple-post-must-have-at-leat-a-text-or-a-photo.exception';
import { IFile } from '../../../interfaces/IFile';
import { IWeightPost } from '../../../interfaces/Post';
import { ActivityRepository } from '../../../repositories/activity.repository';
import { BeforeAfterRepository } from '../../../repositories/beforeAfter.repository';
import { CommentRepository } from '../../../repositories/comment.repository';
import { FoodRepository } from '../../../repositories/food.repository';
import { FriendRepository } from '../../../repositories/friend.repository';
import { HashtagRepository } from '../../../repositories/hashtag.repository';
import { PhotoRepository } from '../../../repositories/photo.repository';
import { PostRepository } from '../../../repositories/post.repository';
import { UserRepository } from '../../../repositories/user.repository';
import { UserActivityRepository } from '../../../repositories/userActivity.repository';
import { UserNutritionRepository } from '../../../repositories/userNutrition.repository';
import { UserNutritionFoodRepository } from '../../../repositories/userNutritionFood.repository';
import { UserWeightRepository } from '../../../repositories/userWeight.repository';
import { AwsS3Service } from '../../../shared/services/aws-s3.service';
import { ConfigService } from '../../../shared/services/config.service';
import { SlackService } from '../../../shared/services/slack.service';
import {
  addDays,
  formatDate,
  isDateBeforeDate,
} from '../../../utils/date';
import { NotificationService } from '../../notification/notification.service';
import { PostBeforeAfterDto } from '../dto/beforeAfter.dto';
import { CreatePostDto } from '../dto/create-post.dto';
import { DeletePostDto } from '../dto/deletePost.dto';
import { GetPostsDto } from '../dto/get-posts.dto';
import { PostActivityDto } from '../dto/postActivity.dto';
import { PostWeightDto } from '../dto/postWeight.dto';
import { ShareArticleDto } from '../dto/shareArticle.dto';
import { UpdatePostDto } from '../dto/updatePost.dto';

@Injectable()
export class PostService {
  constructor(
    private readonly _awsS3Service: AwsS3Service,
    private readonly _userWeightRepository: UserWeightRepository,
    private readonly _postRepository: PostRepository,
    private readonly _userRepository: UserRepository,
    private readonly _userActivityRepository: UserActivityRepository,
    private readonly _userNutritionRepository: UserNutritionRepository,
    private readonly _userNutritionFoodRepository: UserNutritionFoodRepository,
    private readonly _friendRepository: FriendRepository,
    private readonly _foodRepository: FoodRepository,
    private readonly _photoRepository: PhotoRepository,
    private readonly _commentRepository: CommentRepository,
    private readonly _configService: ConfigService,
    private readonly _activityRepository: ActivityRepository,
    private readonly _notificationService: NotificationService,
    private readonly _beforeAfterRepository: BeforeAfterRepository,
    private readonly _hashTagRepository: HashtagRepository,
    private readonly _slackService: SlackService,
  ) {}

  public async getPosts(getPostsDto: GetPostsDto, user: UserEntity) {
    const {
      order,
      take,
      skip,
      dateFrom,
      dateTo,
      postType,
    } = getPostsDto;

    const endDate =
      dateTo && dateTo !== dateFrom
        ? new Date(dateTo)
        : addDays(new Date(dateFrom), 1);

    const dateRange = dateFrom
      ? [
          formatDate('YYYY-MM-DD', new Date(dateFrom)),
          formatDate('YYYY-MM-DD', endDate),
        ]
      : null;

    const queryOptions = {
      order,
      take,
      skip,
      where: {
        where: {
          user,
          ...(postType ? { type: postType } : {}),
          ...(dateRange
            ? { createdAt: Between(dateRange[0], dateRange[1]) }
            : {}),
        },
      },
    };
    return this._postRepository.getPostsWithRelations(
      user,
      null,
      queryOptions,
    );
  }

  public async createWeightPost(post: IWeightPost, userId: string) {
    const { weight } = post;

    const previousWeightT = await this._userWeightRepository.findOne({
      where: { user: userId },
      order: { createdAt: 'DESC' },
    });

    if (!previousWeightT || !previousWeightT.weight) {
      // take from user onboarding original weight and return this._userWeightRepository.create();
    }
    // ^^^ move to sep FN

    const previousWeight = previousWeightT.weight; // todo

    // TODO make it more clear
    const weightDifference = Number(
      (weight - previousWeight).toFixed(1),
    );

    return this._userWeightRepository.save({
      weight,
      weightDifference,
      user: <any>userId,
    });

    // if (postWeightDto.postOnFeed) {
    //   if (!postWeightDto.photos && !postWeightDto.text) {
    //     throw new SimpleTextMustHaveAtLeastATextOrAPhoto();
    //   }

    //   const post = this._postRepository.create();

    //   post.text =
    //     postWeightDto.text === null ? '' : postWeightDto.text;
    //   post.type = PostTypeEnum.WEIGHT;
    //   post.user = <any>postWeightDto.userId;
    //   post.userWeight = userWeight;

    //   if (postWeightDto.friendsToTag) {
    //     const friendsToTag = await this._tagFriends(
    //       [],
    //       postWeightDto.friendsToTag,
    //       postWeightDto.userId,
    //     );
    //     post.tags = friendsToTag;
    //   }

    //   const createdPost = await this._savePostMiddleware(post, {
    //     weightDifference,
    //     ...postWeightDto,
    //   });

    //   if (useDefaultImage) {
    //     const [defaultImage] = postWeightDto.photos;
    //     const photo = this._photoRepository.create();
    //     photo.path = defaultImage;
    //     photo.post = <any>createdPost.id;
    //     const savedPhoto = await this._photoRepository.save(photo);
    //     post.photos = [savedPhoto];
    //   }

    //   if (
    //     postWeightDto.photos &&
    //     postWeightDto.photos.length &&
    //     !useDefaultImage
    //   ) {
    //     await this._addPhoto(
    //       post,
    //       createdPost,
    //       postWeightDto.photos,
    //     );
    //   }

    //   const dtoPost = createdPost.toDto();
    //   dtoPost.weightDifference = weightDifference;
    //   return dtoPost;
    // }
  }

  public async verifyFriendsToTag(
    friendsIdsToTag: string[],
    userId: string,
  ) {
    if (friendsIdsToTag && friendsIdsToTag.length) {
      const relationship = await this._friendRepository.find({
        where: {
          user: userId,
          friend: In(friendsIdsToTag),
        },
        relations: ['friend'],
      });

      return relationship.map(({ friend }) => friend).filter(Boolean);
    }
    return [];
  }

  private async _verifyFriendsToTag(
    friendsIdsToTag: string[],
    userId: string,
  ) {
    const relationship = await this._friendRepository.find({
      where: {
        user: userId,
        friend: In(friendsIdsToTag),
      },
      relations: ['friend'],
    });

    return relationship.map(({ friend }) => friend).filter(Boolean);
  }

  private async _addPhoto(
    post: PostEntity,
    createdPost: PostEntity,
    photosId: string[],
  ) {
    const photos = await this._photoRepository.addPhotos(
      post.id,
      photosId,
    );
    createdPost.photos = photos;
  }

  public async createSimplePost(
    postDto: CreatePostDto,
    userId: string,
    defaultImages: string,
  ) {
    if (!postDto.photos && !postDto.text) {
      throw new SimpleTextMustHaveAtLeastATextOrAPhoto();
    }
    const { text, postType, photos } = postDto;

    const post = this._postRepository.create();
    post.text = text;
    post.user = <any>userId;
    post.type = postType;

    const createdPost = await this._savePostMiddleware(
      post,
      {},
      userId,
    );

    if (defaultImages) {
      const photo = this._photoRepository.create();
      photo.path = defaultImages[0];
      photo.post = <any>createdPost.id;
      const savedPhoto = await this._photoRepository.save(photo);
      post.photos = [savedPhoto];
    }

    if (photos && photos.length && !defaultImages) {
      await this._addPhoto(post, createdPost, photos);
    }

    return createdPost;
  }

  public async postActivity(
    postActivityDto: PostActivityDto,
    useDefaultImage: boolean,
  ) {
    try {
      let friendsToTag: any[];

      const userActivity = this._userActivityRepository.create();
      const post = this._postRepository.create();

      const userLastWeightQuery = this._userWeightRepository
        .createQueryBuilder('userWeight')
        .where('userWeight.user = :id', {
          id: postActivityDto.userId,
        })
        .orderBy('created_at', 'DESC')
        .getOne();

      const activityQuery = this._activityRepository
        .createQueryBuilder('activity')
        .where('activity.id = :id', {
          id: postActivityDto.activityId,
        })
        .getOne();

      const res = await Promise.all([
        userLastWeightQuery,
        activityQuery,
      ]);

      userActivity.duration = postActivityDto.duration;
      userActivity.activity = <any>postActivityDto.activityId;
      userActivity.user = <any>postActivityDto.userId;
      userActivity.calorieBurned = Math.round(
        ((res[1].met * 3.5 * res[0].weight) / 200) *
          postActivityDto.duration,
      );
      await this._userActivityRepository.save(userActivity);

      post.text = postActivityDto.text;
      post.type = PostTypeEnum.ACTIVITY;
      post.userActivity = userActivity;
      post.user = <any>postActivityDto.userId;

      if (postActivityDto.friendsToTag) {
        friendsToTag = await this._verifyFriendsToTag(
          postActivityDto.friendsToTag,
          postActivityDto.userId,
        );
        post.tags = friendsToTag;
      }
      const createdPost = await this._savePostMiddleware(
        post,
        postActivityDto,
        postActivityDto.userId,
      );

      if (useDefaultImage) {
        const [defaultImage] = postActivityDto.photos;
        const photo = this._photoRepository.create();
        photo.path = defaultImage;
        photo.post = <any>createdPost.id;
        const savedPhoto = await this._photoRepository.save(photo);
        post.photos = [savedPhoto];
      }

      if (
        postActivityDto.photos &&
        postActivityDto.photos.length &&
        !useDefaultImage
      ) {
        await this._addPhoto(
          post,
          createdPost,
          postActivityDto.photos,
        );
      }
      return createdPost;
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  public async postBeforeAfter(
    postBeforeAfterDto: PostBeforeAfterDto,
  ) {
    /*if (
      postBeforeAfterDto.weightBefore < postBeforeAfterDto.weightAfter
    ) {
      throw new BadRequestException(
        'validation_weight_after_must_less_before',
      );
    }*/
    const post = this._postRepository.create();
    post.text = postBeforeAfterDto.text;
    post.user = <any>postBeforeAfterDto.userId;
    post.type = PostTypeEnum.BEFORE_AFTER;

    const createdPost = await this._savePostMiddleware(
      post,
      postBeforeAfterDto,
      postBeforeAfterDto.userId,
    );

    const beforeAfter = this._beforeAfterRepository.create();
    beforeAfter.weightBefore = postBeforeAfterDto.weightBefore;
    beforeAfter.weightAfter = postBeforeAfterDto.weightAfter;
    beforeAfter.post = post;
    this._beforeAfterRepository.save(beforeAfter);

    const photoBefore = this._photoRepository.create();
    const photoAfter = this._photoRepository.create();

    photoBefore.post = createdPost;
    photoBefore.type = MediaTypeEnum.BEFORE;
    photoBefore.path = `feed/${postBeforeAfterDto.photoBefore}`;

    photoAfter.post = createdPost;
    photoAfter.type = MediaTypeEnum.AFTER;
    photoAfter.path = `feed/${postBeforeAfterDto.photoAfter}`;

    this._photoRepository.save(photoBefore);
    this._photoRepository.save(photoAfter);

    /*writePostDto.friendsToTag.map((taggedFriendId) => {
        this._notificationService.addNotification({
          from: writePostDto.userId,
          postId: createdPost.id,
          userId: taggedFriendId,
          type: NotificationEnum.TAG_FRIEND,
          // taggedFriend: postActivityDto.friendsToTag,
        });
      });*/

    /*if (
      postBeforeAfterDto.photoBefore &&
      postBeforeAfterDto.photoAfter
    ) {
      await this._addPhoto(
        post,
        createdPost,
        postBeforeAfterDto.photoBefore,
      );
      await this._addPhoto(
        post,
        createdPost,
        postBeforeAfterDto.photoAfter,
      );
    }*/

    return createdPost.toDto();
    // return createdPost;
  }

  public async postWeight(
    postWeightDto: PostWeightDto,
    useDefaultImage: boolean,
  ) {
    try {
      const userWeight = this._userWeightRepository.create();
      const previousWeight = await this._userWeightRepository
        .createQueryBuilder('weight')
        .where('weight.user_id = :id', { id: postWeightDto.userId })
        .orderBy('created_at', 'DESC')
        .getOne();

      const weightDifference =
        postWeightDto.weight - previousWeight.weight;
      userWeight.weight = postWeightDto.weight;
      userWeight.weightDifference = Number(
        weightDifference.toFixed(1),
      );
      userWeight.user = <any>postWeightDto.userId;
      await this._userWeightRepository.save(userWeight);

      if (!postWeightDto.photos && !postWeightDto.text) {
        throw new SimpleTextMustHaveAtLeastATextOrAPhoto();
      }

      const post = this._postRepository.create();

      post.text =
        postWeightDto.text === null ? '' : postWeightDto.text;
      post.type = PostTypeEnum.WEIGHT;
      post.user = <any>postWeightDto.userId;
      post.status = postWeightDto.postOnFeed
        ? PostStatusEnum.PUBLIC
        : PostStatusEnum.PRIVATE;
      post.userWeight = userWeight;

      if (postWeightDto.friendsToTag) {
        const friendsToTag = await this._verifyFriendsToTag(
          postWeightDto.friendsToTag,
          postWeightDto.userId,
        );
        post.tags = <any>friendsToTag;
      }

      const createdPost = await this._savePostMiddleware(
        post,
        {
          weightDifference,
          ...postWeightDto,
        },
        postWeightDto.userId,
      );

      if (useDefaultImage) {
        const [defaultImage] = postWeightDto.photos;
        const photo = this._photoRepository.create();
        photo.path = defaultImage;
        photo.post = <any>createdPost.id;
        const savedPhoto = await this._photoRepository.save(photo);
        post.photos = [savedPhoto];
      }

      if (
        postWeightDto.photos &&
        postWeightDto.photos.length &&
        !useDefaultImage
      ) {
        await this._addPhoto(post, createdPost, postWeightDto.photos);
      }

      const dtoPost = createdPost.toDto();
      dtoPost.weightDifference = weightDifference;
      return dtoPost;
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  public async shareArticle(request: ShareArticleDto) {
    const post = this._postRepository.create();
    post.user = <any>request.userId;
    post.type = PostTypeEnum.SHARED_ARTICLE;
    post.article = <any>request.articleId;
    return this._savePostMiddleware(post, request, request.userId);
  }

  public async updatePost(
    updatePostDto: UpdatePostDto,
    postId: string,
  ) {
    try {
      let friendsToTag: UserEntity[];
      if (updatePostDto.friendsToTag) {
        await this._verifyFriendsToTag(
          updatePostDto.friendsToTag,
          updatePostDto.userId,
        );
      }

      const post = await this._postRepository
        .createQueryBuilder('post')
        .where('post.id = :postId', { postId })
        .andWhere('post.user = :user', { user: updatePostDto.userId })
        .getOne();
      if (!post) {
        throw new PostNotFoundException();
      }

      post.text = updatePostDto.text;
      post.user = <any>updatePostDto.userId;

      if (friendsToTag) {
        post.tags = friendsToTag;
      }

      const updatedPost = await this._savePostMiddleware(
        post,
        UpdatePostDto,
        updatePostDto.userId,
      );

      if (updatePostDto.photos && updatePostDto.photos.length) {
        await this._addPhoto(post, updatedPost, updatePostDto.photos);
      }

      return updatedPost;
    } catch (err) {
      throw new BadRequestException(err);
    }
  }

  public async deletePost(
    deletePostDto: DeletePostDto,
    user: UserEntity,
  ) {
    const isAdmin = user.roles.includes(UserRoleEnum.ADMIN);

    const post = await this._postRepository.findOne(
      deletePostDto.postId,
      { relations: ['user', 'comments', 'likes'] },
    );

    if (!post) {
      throw new PostNotFoundException();
    }

    if (post.type === PostTypeEnum.WEIGHT) {
      await this._userWeightRepository.update(
        { post: <any>post.id },
        { post: null },
      );
    }

    if (isAdmin) {
      const env = this._configService.nodeEnv;
      const deletionTime = new Date().toLocaleDateString('fr', {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
      });
      this._slackService
        .sendToContactChannel(
          `
        ${deletionTime} on nodeEnv - ${env}
        Admin - ${user.pseudo} had deleted
        ${post.user.pseudo} post's with id - ${post.id}`,
        )
        .then();
    }

    if (
      isAdmin ||
      post.user.id === deletePostDto.userId ||
      post.user === null
    ) {
      return this._postRepository.delete(post.id);
    }

    throw new BadRequestException(
      "Unauthorized action : you don't own the post",
    );
  }

  public async uploadPhoto(photo: IFile) {
    const imageId = await this._awsS3Service.uploadImage(
      photo,
      TypeUploadEnum.FEED,
    );
    const photoEntity = this._photoRepository.create();
    photoEntity.path = imageId;

    await this._photoRepository.save(photoEntity);

    return photoEntity.id;
  }

  private async _sendNotifications(post: PostEntity, dto: any) {
    const postType = post.type;
    switch (postType) {
      case PostTypeEnum.FOOD:
      case PostTypeEnum.ACTIVITY:
        dto.friendsToTag.map((taggedFriendId) => {
          this._notificationService.addNotification({
            from: dto.userId,
            postId: post.id,
            userId: taggedFriendId,
            type: NotificationEnum.TAG_FRIEND,
            taggedFriend: dto.friendsToTag,
          });
        });
        break;
      case PostTypeEnum.WEIGHT: {
        if (dto.weightDifference < 0) {
          const friends = await this._friendRepository
            .createQueryBuilder('friends')
            .where('friends.user = :userID', {
              userID: dto.userId,
            })
            .loadAllRelationIds()
            .getMany();

          friends.map((friend) => {
            this._notificationService.addNotification({
              from: dto.userId,
              postId: post.id,
              type: NotificationEnum.FRIEND_LOST_WEIGHT,
              userId: <any>friend.friend,
            });
          });
        }
        break;
      }
      default:
    }
  }

  private async _notifyMentionedUsersInPost(
    post: PostEntity,
    userId: string,
  ) {
    if (post.text) {
      const mentions = post.text.match(
        /@[\p{L}0-9_.-]*[^\s.?!,:;()^*&$#]/gu,
      );

      if (mentions && mentions.length) {
        const pseudos = mentions.map((w) => w.slice(1));

        const us = await this._userRepository.find({
          where: { pseudo: In(pseudos) },
        });
        (us || []).map(({ id }) => {
          this._notificationService.addNotification({
            type: NotificationEnum.MENTION_USER_IN_POST,
            postId: post.id,
            userId: id,
            from: userId,
          });
        });
      }
    }
  }

  public async createPostHashTags(
    text: string,
    postType: PostTypeEnum,
    userId: string,
  ) {
    let hashtags: string[] = [];
    const user = await this._userRepository
      .createQueryBuilder('user')
      .where('user.id = :userId', { userId })
      .leftJoinAndSelect('user.badges', 'badges')
      .getOne();

    if (postType === PostTypeEnum.BEFORE_AFTER) {
      hashtags.push('evolution');
    }

    if (text && text.includes('#')) {
      const hashtagsInText = text
        .replace(/(\r\n|\n|\r)/gm, ' ')
        .split(' ')
        .filter((w) => w.charAt(0) === '#')
        .map((w) => w.slice(1));

      hashtags = [...hashtags, ...hashtagsInText];
    }

    if (user.email === 'thomas@fatster.app') {
      hashtags.push('coach');
    }

    if (user.badges && user.badges.length) {
      const badgesNames = new Set(
        user.badges.map(({ nameEn }) => toLower(nameEn)),
      );
      const isPro = badgesNames.has('pro');
      const isAmbassador = badgesNames.has('ambassador');

      if (isPro) {
        hashtags.push('pro');
      }

      if (isAmbassador) {
        hashtags.push('star');
      }
    }

    const expireNewUserHashtagDate = addDays(user.createdAt, 2);
    const isBefore = isDateBeforeDate(
      new Date(),
      expireNewUserHashtagDate,
    );

    if (isBefore) {
      hashtags.push('hello');
    }

    return Array.from(new Set(hashtags));
  }

  public async createPostMentions(text: string) {
    const mentions =
      text && text.match(/@[\p{L}0-9_.-]*[^\s.?!,:;()^*&$#]/gu);

    if (mentions && mentions.length) {
      const pseudos = mentions.map((w) => w.slice(1));

      return this._userRepository.find({
        where: { pseudo: In(pseudos) },
      });
    }

    return [];
  }

  private async _enhancePostWithHashtag(post: PostEntity) {
    let hashtags: string[] = [];
    const userId: string = <any>post.user;
    const user = await this._userRepository
      .createQueryBuilder('user')
      .where('user.id = :userId', { userId })
      .leftJoinAndSelect('user.badges', 'badges')
      .getOne();

    if (post.type === PostTypeEnum.BEFORE_AFTER) {
      hashtags.push('evolution');
    }

    if (post.text && post.text.includes('#')) {
      const hashtagsInText = post.text
        .replace(/(\r\n|\n|\r)/gm, ' ')
        .split(' ')
        .filter((w) => w.charAt(0) === '#')
        .map((w) => w.slice(1));

      hashtags = [...hashtags, ...hashtagsInText];
    }

    if (user.email === 'thomas@fatster.app') {
      hashtags.push('coach');
    }

    if (user.badges && user.badges.length) {
      const badgesNames = new Set(
        user.badges.map(({ nameEn }) => toLower(nameEn)),
      );
      const isPro = badgesNames.has('pro');
      const isAmbassador = badgesNames.has('ambassador');

      if (isPro) {
        hashtags.push('pro');
      }

      if (isAmbassador) {
        hashtags.push('star');
      }
    }

    const expireNewUserHashtagDate = addDays(user.createdAt, 2);
    const postCreatedDate = post.createdAt || new Date();
    const isBefore = isDateBeforeDate(
      postCreatedDate,
      expireNewUserHashtagDate,
    );

    if (isBefore) {
      hashtags.push('hello');
    }

    post.hashtags = Array.from(new Set(hashtags));

    const mentions =
      post.text &&
      post.text.match(/@[\p{L}0-9_.-]*[^\s.?!,:;()^*&$#]/gu);

    if (mentions && mentions.length) {
      const pseudos = mentions.map((w) => w.slice(1));

      const us = await this._userRepository.find({
        where: { pseudo: In(pseudos) },
      });
      post.mentionedUsersPseudo = (us || []).map(
        ({ pseudo }) => pseudo,
      );
    }

    return post;
  }

  private async _savePostMiddleware(
    post: PostEntity,
    dto: any,
    userId,
  ) {
    const postWithHashtags = await this._enhancePostWithHashtag(post);
    const savedPost = await this._postRepository.save(
      postWithHashtags,
    );

    this._sendNotifications(savedPost, dto);
    this._notifyMentionedUsersInPost(savedPost, userId);

    return savedPost;
  }
}
