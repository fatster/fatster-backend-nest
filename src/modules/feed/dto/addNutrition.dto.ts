import {
  ApiHideProperty,
  ApiProperty,
  ApiPropertyOptional,
} from '@nestjs/swagger';
import { IsArray, IsEnum, IsOptional } from 'class-validator';

import {
  HealthyTypeEnum,
  PostStatusEnum,
  TimeOfTheDayEnum,
} from '../../../common/enum/enum';

export class AddNutritionDto {
  @ApiPropertyOptional()
  @IsOptional()
  readonly text: string;

  @ApiProperty({ enum: ['no', 'average', 'yes'] })
  @IsEnum(HealthyTypeEnum)
  healthyType?: HealthyTypeEnum;

  @ApiProperty({
    enum: ['breakfast', 'lunchTime', 'dinnerTime', 'snack'],
  })
  @IsEnum(TimeOfTheDayEnum)
  timeOfTheDay: TimeOfTheDayEnum;

  @ApiPropertyOptional()
  @IsOptional()
  readonly friendsToTag: string[];

  @ApiPropertyOptional()
  @IsOptional()
  readonly status: PostStatusEnum = PostStatusEnum.PUBLIC;

  @ApiPropertyOptional()
  @IsOptional()
  photos: string[];

  @ApiPropertyOptional()
  @IsArray()
  readonly foods: any[];

  @ApiHideProperty()
  userId: string;
}
