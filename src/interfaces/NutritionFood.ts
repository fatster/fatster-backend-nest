import { NutritionFoodType } from '../common/enum/enum';

interface ICommonFood {
  portionType: NutritionFoodType;
  id: string;
}

export interface INutritionCaloriesFood extends ICommonFood {
  portionType: NutritionFoodType.Calories;
  calories: number;
}

export interface INutritionPortionsFood extends ICommonFood {
  portionType: NutritionFoodType.Portions;
  energyEuRegulation: number;
  portion: number;
}

export interface INutritionGramsFood extends ICommonFood {
  portionType: NutritionFoodType.Grams;
  energyEuRegulation: number;
  grams: number;
}

export type NutritionFood =
  | INutritionCaloriesFood
  | INutritionPortionsFood
  | INutritionGramsFood;
