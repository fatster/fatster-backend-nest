'use strict';

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class RemoveCommentDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  readonly commentId: string;

  @IsNotEmpty()
  @IsString()
  readonly userId: string;

  constructor(commentId: string, userId: string) {
    this.commentId = commentId;
    this.userId = userId;
  }
}
