import { IsIn } from 'class-validator';
import { AfterLoad, Column, Entity, ManyToOne } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { MediaTypeEnum } from '../common/enum/enum';
import { PhotoDto } from '../dto/photo.dto';
import { PostEntity } from './post.entity';

@Entity({ name: 'photo' })
export class PhotoEntity extends AbstractEntity<PhotoDto> {
  @Column({ nullable: false })
  path: string;

  @ManyToOne(() => PostEntity, (post) => post.photos, {
    onDelete: 'CASCADE',
  })
  post: PostEntity;

  @Column({
    type: 'enum',
    enum: MediaTypeEnum,
    default: MediaTypeEnum.PHOTO,
  })
  @IsIn(['photo', 'video', 'before', 'after'])
  type?: MediaTypeEnum;

  @Column({ nullable: true })
  thumbnail: string;

  @AfterLoad()
  prefixWithUrl() {
    switch (this.type) {
      case MediaTypeEnum.PHOTO:
        this.path = `${process.env.S3_BASE_URL}${this.path}?w=400`;
        break;
      case MediaTypeEnum.AFTER:
      case MediaTypeEnum.BEFORE:
        // eslint-disable-next-line max-len
        this.path = `${process.env.S3_BASE_URL}${this.path}?fit=clip&w=1000&h=1000&q=20&mark64=aHR0cHM6Ly93d3cuZmF0c3Rlci5hcHAvd3AtY29udGVudC91cGxvYWRzLzIwMjAvMDQvZmF2aWNvbi5wbmc&mark-w=160&mark-h=50&mark-y=850&mark-tile=grid&mark-pad=106&mark-alpha=5`;
        break;
      default:
    }
  }

  dtoClass = PhotoDto;
}
