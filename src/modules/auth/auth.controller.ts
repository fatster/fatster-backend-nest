import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import * as appleSignIn from 'apple-signin-auth';

import { PayloadSuccessDto } from '../../common/dto/PayloadSuccessDto';
import { AuthUser } from '../../decorators/auth-user.decorator';
import { UserDto } from '../../dto/user.dto';
import { UserEntity } from '../../entities/user.entity';
import {
  AuthGuard,
  AuthGuardFacebook,
} from '../../guards/auth.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
import { UserService } from '../user/user.service';
import { AuthService } from './auth.service';
import { AppleProfileDto } from './dto/appleProfile.dto';
import { LoginPayloadDto } from './dto/loginPayload.dto';
import { ResetPasswordDto } from './dto/resetPassword.dto';
import { UserLoginDto } from './dto/userLogin.dto';
import { UserMailDto } from './dto/userMail.dto';
import { UserRegisterDto } from './dto/userRegister.dto';
import { UserVerificationCodeDto } from './dto/userVerificationCode.dto';

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
  constructor(
    private readonly _userService: UserService,
    private readonly _authService: AuthService,
  ) {}

  @Post('login')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    type: LoginPayloadDto,
    description: 'User info with access token',
  })
  async userLogin(
    @Body() userLoginDto: UserLoginDto,
  ): Promise<PayloadSuccessDto> {
    const userEntity = await this._authService.validateUser(
      userLoginDto,
    );

    const token = await this._authService.createToken(userEntity);
    const data = new LoginPayloadDto(userEntity.toDto(), token);

    return {
      data,
      message: 'User info with access token',
    };
  }

  @Post('register')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    type: UserDto,
    description: 'Successfully Registered',
  })
  async userRegister(
    @Body() userRegisterDto: UserRegisterDto,
  ): Promise<PayloadSuccessDto> {
    const createdUser = await this._userService.createUser(
      userRegisterDto,
    );
    const token = await this._authService.createToken(createdUser);
    return {
      message: 'User info with access token',
      data: { token, user: createdUser.toDto() },
    };
  }

  @UseGuards(AuthGuardFacebook)
  @Get('facebook')
  @ApiQuery({ name: 'access_token', type: String })
  @ApiOkResponse({
    type: LoginPayloadDto,
    description: 'User info with access token',
  })
  async getTokenAfterFacebookSignIn(
    @Req() req,
  ): Promise<PayloadSuccessDto> {
    const token = await this._authService.createToken(req.user);
    const user = await this._userService.findOne({ id: req.user.id });
    return {
      message: 'User info with access token',
      data: new LoginPayloadDto(user.toDto(), token),
    };
  }

  @Post('apple')
  @ApiOkResponse({
    type: LoginPayloadDto,
    description: 'User info with access token',
  })
  async getTokenAfterAppleSignIn(
    @Body() appleProfile: AppleProfileDto,
  ): Promise<PayloadSuccessDto> {
    let appleJWT;
    try {
      appleJWT = await appleSignIn.verifyIdToken(
        appleProfile.appleId,
        {
          audience: 'com.socialcrunch.fatsterApp',
          ignoreExpiration: true, // ignore token expiry (never expires)
        },
      );
    } catch (error) {
      throw Error(error);
    }

    appleProfile.appleId = appleJWT.sub;
    appleProfile.email = appleProfile.email || appleJWT.email;

    const user = await this._userService.findOrCreateFromAppleId(
      appleProfile,
    );
    const token = await this._authService.createToken(user);
    const userWithToken = await this._userService.findOne({
      id: user.id,
    });
    return {
      message: 'User info with access token',
      data: new LoginPayloadDto(userWithToken.toDto(), token),
    };
  }

  @Post('password/reset')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Password changed',
  })
  async resetPassword(
    @Body() resetPasswordDto: ResetPasswordDto,
  ): Promise<PayloadSuccessDto> {
    await this._authService.resetPassword(resetPasswordDto);
    return {
      message: 'Password changed',
    };
  }

  @Post('password/sendCode')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'code sent',
  })
  async sendCode(
    @Body() email: UserMailDto,
  ): Promise<PayloadSuccessDto> {
    const response = await this._authService.sendCode(email);
    return {
      message: 'code sent',
      data: response,
    };
  }

  @Post('password/verifyCode')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Verification succeded',
  })
  async enterVerificationCode(
    @Body() userVerificationCodeDto: UserVerificationCodeDto,
  ): Promise<PayloadSuccessDto> {
    const response = await this._authService.enterVerificationCode(
      userVerificationCodeDto,
    );
    return {
      message: 'code sent',
      data: response,
    };
  }

  @Get('logout')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'logout succesful',
  })
  async logeout(
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    await this._authService.logout(user.id);
    return {
      message: 'logout succesful',
    };
  }
}
