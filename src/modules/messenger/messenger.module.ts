import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { MessengerRoomEntity } from '../../entities/messengerRoom.entity';
import { FriendRepository } from '../../repositories/friend.repository';
import { MessengerMessageRepository } from '../../repositories/messengerMessage.repository';
import { MessengerController } from './messenger.controller';
import { MessengerService } from './messenger.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      FriendRepository,
      MessengerMessageRepository,
      MessengerRoomEntity,
    ]),
  ],
  providers: [MessengerService],
  controllers: [MessengerController],
  exports: [MessengerService],
})
export class MessengerModule {}
