import { Injectable } from '@nestjs/common';

import {
  Language,
  TranslationService,
  Version,
} from '../../shared/services/translation.service';

@Injectable()
export class TranslationEndpointService {
  constructor(
    private readonly _translationService: TranslationService,
  ) {}

  async getTranslation(
    language: Language,
    disableCache: boolean,
    version: Version,
  ) {
    try {
      return await this._translationService.getTranslationsForLang(
        language,
        disableCache,
        version,
      );
    } catch (error) {
      throw Error(error);
    }
  }
}
