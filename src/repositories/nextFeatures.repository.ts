import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { NextFeaturesEntity } from '../entities/nextFeatures.entity';

@EntityRepository(NextFeaturesEntity)
export class NextFeaturesRepository extends Repository<
  NextFeaturesEntity
> {}
