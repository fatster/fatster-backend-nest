import { ValueTransformer } from 'typeorm';

import { UtilsService } from '../../providers/utils.service';

export class PasswordTransformer implements ValueTransformer {
  to(value) {
    return value ? UtilsService.generateHash(value) : value;
  }
  from(value) {
    return value;
  }
}
