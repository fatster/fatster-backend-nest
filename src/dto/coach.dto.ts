import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { CoachTypeEnum } from '../common/enum/enum';
import { CoachCategoryDto } from './coachCategory.dto';

export class CoachDto extends AbstractDto {
  @ApiPropertyOptional()
  nameFr: string;

  @ApiPropertyOptional()
  nameEn: string;

  @ApiPropertyOptional()
  type: CoachTypeEnum;

  @ApiPropertyOptional()
  coachCategory: CoachCategoryDto;

  @ApiPropertyOptional()
  descriptionEn: string;

  @ApiPropertyOptional()
  descriptionFr: string;
}
