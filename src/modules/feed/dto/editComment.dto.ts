import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class EditCommentDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  text: string;

  commentId: string;

  userId: string;
}
