import { Column, Entity, JoinTable, ManyToMany } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { BadgeDto } from '../dto/badge.dto';
import { UserEntity } from './user.entity';

@Entity({ name: 'badge' })
export class BadgeEntity extends AbstractEntity<BadgeDto> {
  @Column()
  nameEn: string;

  @Column()
  nameFr: string;

  @Column()
  descriptionEn: string;

  @Column()
  descriptionFr: string;

  @ManyToMany(() => UserEntity, (user) => user.badges)
  @JoinTable()
  users?: UserEntity[];

  dtoClass = BadgeDto;
}
