import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';

import { PayloadSuccessDto } from '../../common/dto/PayloadSuccessDto';
import { UserRoleEnum } from '../../common/enum/enum';
import { AuthUser } from '../../decorators/auth-user.decorator';
import { UserEntity } from '../../entities/user.entity';
import { AuthGuard } from '../../guards/auth.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
import { BadgeService } from './badge.service';
import { AddBadgeDto } from './dto/addBadge.dto';

@Controller('badges')
@ApiTags('Badge')
export class BadgeController {
  constructor(private _badgeService: BadgeService) {}

  @Get('/')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'get all badges' })
  async getUserById(): Promise<PayloadSuccessDto> {
    const badges = await this._badgeService.getAll();

    return {
      message: 'user',
      data: badges,
    };
  }

  @Post('addBadge/')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({ description: 'apply badge to user Admin only' })
  async addBadge(
    @Body() addBadgeDto: AddBadgeDto,
    @AuthUser() user: UserEntity,
  ) {
    const isAdmin = user && user.roles.includes(UserRoleEnum.ADMIN);
    if (isAdmin) {
      const { userId, badge } = addBadgeDto;
      const userWithBadge = await this._badgeService.addBadgeToUser(
        userId,
        badge,
      );
      return {
        message: `badge applied to user ${userWithBadge.pseudo}`,
      };
    }

    return { message: 'you are not allowed to do it' };
  }
}
