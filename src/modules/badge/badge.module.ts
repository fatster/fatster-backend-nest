import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { BadgeRepository } from '../../repositories/badge.repository';
import { PostRepository } from '../../repositories/post.repository';
import { UserRepository } from '../../repositories/user.repository';
import { AuthModule } from '../auth/auth.module';
import { BadgeController } from './badge.controller';
import { BadgeService } from './badge.service';

@Module({
  imports: [
    forwardRef(() => AuthModule),
    TypeOrmModule.forFeature([
      UserRepository,
      BadgeRepository,
      PostRepository,
    ]),
  ],
  controllers: [BadgeController],
  exports: [BadgeService],
  providers: [BadgeService],
})
export class BadgeModule {}
