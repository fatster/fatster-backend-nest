import { ApiProperty } from '@nestjs/swagger';
import { IsString, MinLength } from 'class-validator';

export class ChangePasswordDto {
  @IsString()
  @MinLength(6)
  @ApiProperty({ minLength: 6 })
  oldPassword: string;

  @IsString()
  @MinLength(6)
  @ApiProperty({ minLength: 6 })
  newPassword: string;
}
