import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { isDto } from '../common/utils/entities';
import { UserWeightEntity } from '../entities/userWeight.entity';
import { UserDto } from './user.dto';

export class UserWeightDto extends AbstractDto {
  @ApiPropertyOptional()
  weight: number;

  @ApiPropertyOptional()
  user: UserDto;

  @ApiPropertyOptional()
  weightDifference: number;

  constructor(userWeight: UserWeightEntity) {
    super(userWeight);
    this.weight = userWeight.weight;
    this.weightDifference = userWeight.weightDifference;
    this.user = isDto(userWeight.user)
      ? userWeight.user.toDto()
      : <any>userWeight.user;
  }
}
