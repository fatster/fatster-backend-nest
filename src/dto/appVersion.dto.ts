import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { PlatformEnum } from '../common/enum/enum';
import { AppVersionEntity } from '../entities/appVersion.entity';

export class AppVersionDto extends AbstractDto {
  @ApiPropertyOptional()
  platform: PlatformEnum;

  @ApiPropertyOptional()
  version: string;

  constructor(appVersion: AppVersionEntity) {
    super(appVersion);
    Object.assign(this, appVersion);
  }
}
