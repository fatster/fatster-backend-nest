import { Column, Entity, ManyToOne } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { VideoDto } from '../dto/video.dto';
import { PostEntity } from './post.entity';

@Entity({ name: 'video' })
export class VideoEntity extends AbstractEntity<VideoDto> {
  @Column({ nullable: false })
  path: string;

  @ManyToOne(() => PostEntity, (post) => post.videos, {
    onDelete: 'CASCADE',
  })
  post: PostEntity;

  dtoClass = VideoDto;
}
