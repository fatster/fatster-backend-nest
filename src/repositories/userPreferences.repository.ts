import { EntityRepository, Repository } from 'typeorm';

import { UserPreferencesEntity } from '../entities/userPreferences.entity';

@EntityRepository(UserPreferencesEntity)
export class UserPreferencesRepository extends Repository<
  UserPreferencesEntity
> {}
