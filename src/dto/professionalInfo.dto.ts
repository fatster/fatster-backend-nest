import {
  ApiProperty,
  ApiPropertyOptional,
  OmitType,
} from '@nestjs/swagger';
import {
  ArrayUnique,
  IsArray,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

import { AbstractDto } from '../common/dto/AbstractDto';
import { isDto } from '../common/utils/entities';
import { ProfessionalInfoEntity } from '../entities/professionalInfo.entity';
import { LanguageEnum } from '../shared/services/translation.service';
import { UserDto } from './user.dto';

export class ProfessionalInfoDto extends AbstractDto {
  @IsString()
  @IsOptional()
  @ApiPropertyOptional()
  bio?: string;

  @IsString()
  @IsOptional()
  @ApiPropertyOptional()
  certificate?: string;

  @IsString()
  @IsOptional()
  @ApiPropertyOptional()
  company?: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  profession: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  specialty: string;

  @IsString()
  @IsOptional()
  @ApiPropertyOptional()
  address?: string;

  @IsString()
  @IsOptional()
  @ApiPropertyOptional()
  zipCode?: string;

  @IsString()
  @IsOptional()
  @ApiPropertyOptional()
  city?: string;

  @IsArray()
  @IsNotEmpty()
  @ArrayUnique()
  @IsEnum(LanguageEnum, { each: true })
  @ApiProperty()
  languages: LanguageEnum[];

  user: UserDto;

  constructor(info: ProfessionalInfoEntity) {
    super(info);
    Object.assign(this, info);
    this.user = isDto(info.user) ? info.user.toDto() : info.user;
  }
}

export class CreateProfessionalInfoDto extends OmitType(
  ProfessionalInfoDto,
  <const>['user', 'id'],
) {}
