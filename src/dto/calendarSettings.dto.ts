import { ApiProperty, OmitType } from '@nestjs/swagger';
import {
  ArrayMaxSize,
  ArrayMinSize,
  ArrayNotEmpty,
  IsArray,
  IsNotEmpty,
  IsString,
} from 'class-validator';

import { AbstractDto } from '../common/dto/AbstractDto';
import { isDto } from '../common/utils/entities';
import { CalendarSettingsEntity } from '../entities/calendarSettings.entity';
import { UserDto } from './user.dto';

export type WorkTimeTuple = [string, string];
export type WorkDaysRange = [string, string];

export class CalendarSettingsDto extends AbstractDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  region: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  country: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  dataFormat: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  timeFormat: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  timeZone: string;

  @IsArray()
  @ArrayNotEmpty()
  @ArrayMaxSize(2)
  @ArrayMinSize(2)
  @ApiProperty()
  workTimeRange: WorkTimeTuple;

  @IsArray()
  @ArrayNotEmpty()
  @ArrayMaxSize(2)
  @ArrayMinSize(2)
  @ApiProperty()
  workDaysRange: WorkDaysRange;

  user: UserDto;

  constructor(info: CalendarSettingsEntity) {
    super(info);
    Object.assign(this, info);
    this.user = isDto(info.user) ? info.user.toDto() : info.user;
  }
}

export class CreateCalendarSettingsDto extends OmitType(
  CalendarSettingsDto,
  <const>['user', 'id'],
) {}
