import { BadRequestException, Injectable } from '@nestjs/common';

import {
  FriendRequestEnum,
  NotificationEnum,
  StatusFriendRequestEnum,
} from '../../common/enum/enum';
import { FriendRequestEntity } from '../../entities/friendRequest.entity';
import { CanNotSendFriendRequestToHimselfException } from '../../exceptions/can-not-send-friend-request-to-himself.exception';
import { FriendRequestAlreadyExistException } from '../../exceptions/friend-request-already-exist.exception';
import { FriendRequestNotFoundException } from '../../exceptions/friend-request-not-found.exception';
import { FriendRepository } from '../../repositories/friend.repository';
import { FriendRequestRepository } from '../../repositories/friendRequest.repository';
import { MessengerRoomRepository } from '../../repositories/messengerRoom.repository';
import { NotificationService } from '../notification/notification.service';
import { ProcessFriendDto } from './dto/processFriend.dto';

@Injectable()
export class FriendService {
  constructor(
    private readonly _friendRepository: FriendRepository,
    private readonly _friendRequestRepository: FriendRequestRepository,
    private readonly _notificationService: NotificationService,
    private readonly _messengerRoomRepository: MessengerRoomRepository,
  ) {}

  private _createFriendEntity(from, to, messengerRoomId) {
    const friend = this._friendRepository.create();
    friend.user = from;
    friend.friend = to;
    friend.messengerRoom = messengerRoomId;
    return friend;
  }

  private _getQueryCountFriend() {
    return this._friendRepository
      .createQueryBuilder('friends')
      .select('friends.user_id AS userId')
      .addSelect('COUNT(*) AS count')
      .groupBy('userId');
  }

  public async getFriendList(userId: string) {
    const userQb = this._getQueryCountFriend();

    const friends = await this._friendRepository
      .createQueryBuilder('friends')
      .addSelect('"countFriends".count')
      .addSelect('friends.messenger_room_id')
      .where('friends.user = :userID', { userID: userId })
      .leftJoin(
        '(' + userQb.getQuery() + ')',
        'countFriends',
        '"countFriends".userId = friends.friend_id',
      )
      .leftJoin('friends.friend', 'users')
      .addSelect('users.pseudo')
      .addSelect('users.gender')
      .addSelect('users.id')
      .addSelect('users.photoId')
      .addSelect('users.targetLooseWeight')
      .addSelect('"countFriends".count AS friendsCount')
      .getRawAndEntities();

    const res = friends.entities.toDtos();

    friends.raw.map((value, index) => {
      if (res[index].friend.photoId) {
        res[index].friend.photoId =
          res[index].friend.photoId + '&fit=clip&w=200&h=200';
      }
      res[index].friend.friendsCount = value.friendscount
        ? parseInt(value.friendscount, 10)
        : 0;
    });

    return res;
  }

  public async sendFriendRequest(
    userId: string,
    toUserId,
    sendNotification = true,
  ) {
    if (userId === toUserId) {
      throw new CanNotSendFriendRequestToHimselfException();
    }
    const friendRequestAlreadyExist = await this._friendRequestRepository.findOne(
      {
        where: [
          { from: userId, to: toUserId, status: 'pending' },
          { from: toUserId, to: userId, status: 'pending' },
        ],
      },
    );
    if (friendRequestAlreadyExist) {
      throw new FriendRequestAlreadyExistException();
    }

    if (await this._isFriend(userId, toUserId)) {
      throw new BadRequestException('Already Friend');
    }

    let friendReq = this._friendRequestRepository.create();

    friendReq.from = <any>userId;
    friendReq.to = <any>toUserId;
    friendReq.status = StatusFriendRequestEnum.PENDING;

    friendReq = await this._friendRequestRepository.save({
      ...friendReq,
      fromId: friendReq.from,
      toId: friendReq.to,
    });

    if (sendNotification) {
      this._notificationService.addNotification({
        type: NotificationEnum.NEW_FRIEND_REQUEST,
        userId: toUserId,
        from: userId,
        friendRequestId: friendReq.id,
      });
    }

    return friendReq;
  }

  public async processFriendRequest(
    requestDto: ProcessFriendDto,
    sendNotification = true,
  ) {
    const friendRequest = await this._friendRequestRepository.findOne(
      requestDto.friendRequestId,
      {
        loadRelationIds: true,
      },
    );
    if (friendRequest) {
      if (
        await this._isFriend(
          <any>friendRequest.from,
          <any>friendRequest.to,
        )
      ) {
        throw new BadRequestException(
          'can not send a friend request to friend',
        );
      }
      const arrayPromise: any[] = [];

      switch (requestDto.status) {
        case StatusFriendRequestEnum.ACCEPTED:
          if (sendNotification) {
            arrayPromise.push(
              this._notificationService.addNotification({
                type: NotificationEnum.NEW_FRIEND,
                userId: <any>friendRequest.from,
                from: <any>friendRequest.to,
              }),
            );
          }
          await this._acceptFriendRequest(friendRequest);
          break;
        case StatusFriendRequestEnum.REFUSED:
          arrayPromise.push(
            this._refuseFriendRequest(
              requestDto.userId,
              friendRequest,
            ),
          );
          break;
        case StatusFriendRequestEnum.CANCELED:
          arrayPromise.push(
            this._cancelFriendRequest(
              requestDto.userId,
              friendRequest,
            ),
          );
          break;
        default:
          throw new BadRequestException('Bad status');
      }

      return Promise.all(arrayPromise);
    }
    throw new FriendRequestNotFoundException();
  }

  private _deleteFriendRequest(id: string) {
    return this._friendRequestRepository.delete({
      id,
    });
  }

  public async getFriendRequests(
    userId: string,
    requestType: FriendRequestEnum,
  ) {
    const userQb = this._getQueryCountFriend();

    const requests = await this._friendRequestRepository
      .createQueryBuilder('friendRequest')
      .addSelect('friendRequest.id AS requestId')
      .where(
        requestType === FriendRequestEnum.RECEIVED
          ? 'friendRequest.to = :id'
          : 'friendRequest.from = :id',
        { id: userId },
      )
      .andWhere('friendRequest.status = :status', {
        status: StatusFriendRequestEnum.PENDING,
      })
      .leftJoin(
        '(' + userQb.getQuery() + ')',
        'countFriends',
        requestType === FriendRequestEnum.RECEIVED
          ? '"countFriends".userId = friendRequest.from_id'
          : '"countFriends".userId = friendRequest.to_id',
      )
      .leftJoin(
        requestType === FriendRequestEnum.RECEIVED
          ? 'friendRequest.from'
          : 'friendRequest.to',
        'users',
      )
      .addSelect('users.pseudo')
      .addSelect('users.gender')
      .addSelect('users.id')
      .addSelect('users.photoId')
      .addSelect('users.targetLooseWeight')
      .addSelect('"countFriends".count AS friendsCount')
      .getRawAndEntities();

    const res = requests.entities.toDtos();

    requests.raw.map((value, index) => {
      res[index][
        requestType === FriendRequestEnum.RECEIVED ? 'from' : 'to'
      ].friendsCount = value.friendscount
        ? parseInt(value.friendscount, 10)
        : 0;
    });

    return res;
  }

  public async removeFriend(userId: string, friendUserId: string) {
    const row1 = await this._friendRepository.findOne({
      where: {
        friend: friendUserId,
        user: userId,
      },
    });
    const row2 = await this._friendRepository.findOne({
      where: {
        friend: userId,
        user: friendUserId,
      },
    });

    const friendRequest = await this._friendRequestRepository.findOne(
      {
        where: [
          {
            from: userId,
            to: friendUserId,
          },
          {
            to: userId,
            from: friendUserId,
          },
        ],
      },
    );

    return Promise.all([
      this._friendRepository.remove(row1),
      this._friendRepository.remove(row2),
      this._friendRequestRepository.remove(friendRequest),
    ]);
  }

  private async _isFriend(userId: string, friendId: string) {
    const friend = await this._friendRepository.count({
      where: { user: userId, friend: friendId },
    });
    return !(friend === 0);
  }

  private async _acceptFriendRequest(
    friendRequest: FriendRequestEntity,
  ) {
    friendRequest.status = StatusFriendRequestEnum.ACCEPTED;
    const messengerRoom = this._messengerRoomRepository.create();
    await this._messengerRoomRepository.save(messengerRoom);

    return Promise.all([
      this._friendRepository.save(
        this._createFriendEntity(
          friendRequest.from,
          friendRequest.to,
          messengerRoom.id,
        ),
      ),
      this._friendRepository.save(
        this._createFriendEntity(
          friendRequest.to,
          friendRequest.from,
          messengerRoom.id,
        ),
      ),
      this._friendRequestRepository.save(friendRequest),
    ]);
  }

  private async _cancelFriendRequest(
    userId: string,
    friendRequest: FriendRequestEntity,
  ) {
    if (friendRequest.from === <any>userId) {
      return this._deleteFriendRequest(friendRequest.id);
    }
    throw new BadRequestException(
      "can not cancel a friend request you didn't send",
    );
  }

  private async _refuseFriendRequest(
    userId: string,
    friendRequest: FriendRequestEntity,
  ) {
    if (friendRequest.to === <any>userId) {
      return this._deleteFriendRequest(friendRequest.id);
    }
    throw new BadRequestException(
      "can not refuse a friend request you didn't receive",
    );
  }
}
