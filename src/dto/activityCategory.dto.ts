import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { ActivityDto } from './activity.dto';

export class ActivityCategoryDto extends AbstractDto {
  @ApiPropertyOptional()
  textEn: string;

  @ApiPropertyOptional()
  textFr: string;

  @ApiPropertyOptional()
  activities: ActivityDto[];
}
