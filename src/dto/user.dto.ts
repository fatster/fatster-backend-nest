import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { GenderEnum, UserRoleEnum } from '../common/enum/enum';
import { isDto, isDtos } from '../common/utils/entities';
import { UserEntity } from '../entities/user.entity';
import { CalendarDto } from '../modules/calendar/calendar.dto';
import { BadgeDto } from './badge.dto';
import { BlockDto } from './block.dto';
import { CalendarSettingsDto } from './calendarSettings.dto';
import { CommentDto } from './comment.dto';
import { FriendDto } from './friend.dto';
import { FriendRequestDto } from './friendRequest.dto';
import { LikeDto } from './like.dto';
import { PostDto } from './post.dto';
import { ProfessionalInfoDto } from './professionalInfo.dto';
import { ReportDto } from './report.dto';
import { UserPreferencesDto } from './userPreferences.dto';
import { UserWeightDto } from './userWeight.dto';

export class UserDto extends AbstractDto {
  @ApiPropertyOptional()
  email: string;

  @ApiPropertyOptional()
  name?: string;

  @ApiPropertyOptional()
  surname?: string;

  @ApiPropertyOptional()
  pseudo?: string;

  @ApiPropertyOptional()
  originalWeight?: number;

  @ApiPropertyOptional()
  targetLooseWeight?: number;

  @ApiPropertyOptional()
  targetWeight?: number;

  @ApiPropertyOptional()
  height?: number;

  @ApiPropertyOptional({ enum: GenderEnum })
  gender?: GenderEnum;

  @ApiPropertyOptional({ enum: UserRoleEnum, isArray: true })
  roles?: UserRoleEnum;

  @ApiPropertyOptional()
  authMethod?: string;

  @ApiPropertyOptional()
  photoId?: string;

  @ApiPropertyOptional()
  birthDate?: Date;

  @ApiPropertyOptional()
  phone?: string;

  @ApiPropertyOptional()
  age?: number;

  @ApiPropertyOptional()
  profileCompleted?: boolean;

  posts?: PostDto[];

  reports?: ReportDto[];

  userWeights?: UserWeightDto[];

  invitedFriends?: FriendRequestDto[];

  friendRequestBy?: FriendRequestDto[];

  friends?: FriendDto[];

  userBlocked?: BlockDto[];

  comments?: CommentDto[];

  likes?: LikeDto[];

  blockedBy?: BlockDto[];

  notifications?: Notification[];

  friendsCount?: number;

  language: string;

  professionalInfo?: ProfessionalInfoDto;

  calendarSettings?: CalendarSettingsDto;

  events?: CalendarDto[];

  @ApiPropertyOptional()
  lastConnection?: Date;

  @ApiPropertyOptional()
  fromFirebase?: boolean;

  @ApiPropertyOptional()
  badges?: BadgeDto[];

  @ApiPropertyOptional()
  userPreferences?: UserPreferencesDto;

  constructor(user: UserEntity) {
    super(user);
    this.id = user.id;
    this.email = user.email;
    this.gender = user.gender;
    this.pseudo = user.pseudo;
    this.surname = user.surname;
    this.name = user.name;
    this.birthDate = user.birthDate;
    this.age = user.age;
    this.height = user.height;
    this.originalWeight = user.originalWeight;
    this.targetLooseWeight = user.targetLooseWeight;
    this.targetWeight = user.targetWeight;
    this.profileCompleted = user.profileCompleted;
    this.photoId = user.photoId;
    this.roles = user.roles;
    this.phone = user.phone;
    this.userWeights = isDtos(user.userWeights)
      ? user.userWeights.toDtos()
      : <any>user.userWeights;
    this.badges = isDtos(user.badges)
      ? user.badges.toDtos()
      : <any>user.badges;
    this.events = (<any>user.events || []).map((e) =>
      isDto(e) ? e.toDto() : e,
    );
    this.userPreferences = isDto(user.userPreferences)
      ? user.userPreferences.toDto()
      : <any>user.userPreferences;
    this.professionalInfo = isDto(user.professionalInfo)
      ? user.professionalInfo.toDto()
      : user.professionalInfo;
    this.calendarSettings = isDto(user.calendarSettings)
      ? user.calendarSettings.toDto()
      : user.calendarSettings;
  }
}
