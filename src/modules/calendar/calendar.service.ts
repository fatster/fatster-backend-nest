import { Injectable } from '@nestjs/common';
import { uniq } from 'lodash';

import { UserRepository } from '../../repositories/user.repository';
import { CreateCalendarDto } from './calendar.dto';
import { CalendarRepository } from './calendar.repository';

@Injectable()
export class CalendarService {
  constructor(
    private readonly _calendarRepository: CalendarRepository,
    private readonly _userRepository: UserRepository,
  ) {}

  public async getAll(ownerId: string) {
    return this._calendarRepository.find({
      where: { ownerId },
      relations: ['participants'],
    });
  }

  public async saveEvent(
    { participants, ...createCalendarDto }: CreateCalendarDto,
    ownerId: string,
  ) {
    const participantsWithOwner = uniq([
      ...participants,
      ownerId,
    ]).map((id) => this._userRepository.create({ id }));

    return this._calendarRepository.save({
      ...createCalendarDto,
      ownerId,
      participants: participantsWithOwner,
    });
  }
}
