import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { MessengerMessageRepository } from '../../repositories/messengerMessage.repository';
import { NextFeaturesRepository } from '../../repositories/nextFeatures.repository';
import { PhotoRepository } from '../../repositories/photo.repository';
import { PostRepository } from '../../repositories/post.repository';
import { UserRepository } from '../../repositories/user.repository';
import { UserActivityRepository } from '../../repositories/userActivity.repository';
import { UserWeightRepository } from '../../repositories/userWeight.repository';
import { KpiController } from './kpi.controller';
import { KpiService } from './kpi.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UserRepository,
      UserWeightRepository,
      PostRepository,
      PhotoRepository,
      UserActivityRepository,
      MessengerMessageRepository,
      NextFeaturesRepository,
    ]),
  ],
  controllers: [KpiController],
  exports: [KpiService],
  providers: [KpiService],
})
export class KpiModule {}
