'use strict';

import { NotificationEnum } from '../../../common/enum/enum';

export class AddNotificationDto {
  readonly type: NotificationEnum;

  readonly from?: string;

  readonly postId?: string;

  readonly commentId?: string;

  readonly likeId?;

  readonly friendRequestId?: string;

  readonly taggedFriend?: string[];

  userId?: string;
}
