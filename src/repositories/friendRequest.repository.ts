import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { FriendRequestEntity } from '../entities/friendRequest.entity';

@EntityRepository(FriendRequestEntity)
export class FriendRequestRepository extends Repository<
  FriendRequestEntity
> {}
