'use strict';

import { BadRequestException } from '@nestjs/common';

export class LikeExistsException extends BadRequestException {
  constructor(error?: string) {
    super('error.like_exists', error);
  }
}
