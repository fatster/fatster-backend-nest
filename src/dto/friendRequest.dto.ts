import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { StatusFriendRequestEnum } from '../common/enum/enum';
import { isDto } from '../common/utils/entities';
import { FriendRequestEntity } from '../entities/friendRequest.entity';
import { UserDto } from './user.dto';

export class FriendRequestDto extends AbstractDto {
  @ApiPropertyOptional()
  from: UserDto;

  @ApiPropertyOptional()
  to: UserDto;

  @ApiPropertyOptional()
  status: StatusFriendRequestEnum;

  constructor(friendRequest: FriendRequestEntity) {
    super(friendRequest);
    this.status = friendRequest.status;
    this.from = isDto(friendRequest.from)
      ? friendRequest.from.toDto()
      : friendRequest.from;
    this.to = isDto(friendRequest.to)
      ? friendRequest.to.toDto()
      : friendRequest.to;
  }
}
