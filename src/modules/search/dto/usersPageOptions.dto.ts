import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsIn,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  Min,
} from 'class-validator';

import { PageOptionsDto } from '../../../common/dto/PageOptionsDto';
import {
  Language,
  LanguageEnum,
} from '../../../shared/services/translation.service';

export class UsersPageOptionsDto extends PageOptionsDto {
  @ApiProperty()
  @IsOptional()
  readonly gender: string[];

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  @IsOptional()
  @IsIn(Object.values(LanguageEnum))
  readonly language?: Language;

  @ApiPropertyOptional({
    default: 1,
  })
  @IsInt()
  @Min(0)
  @IsOptional()
  readonly minAge: number;

  @ApiPropertyOptional({
    default: 100,
  })
  @IsInt()
  @Min(0)
  @IsOptional()
  readonly maxAge: number;

  @ApiPropertyOptional({
    default: 1,
  })
  @IsInt()
  @Min(0)
  @IsOptional()
  readonly minTargetWeightLoose: number;

  @ApiPropertyOptional({
    default: 100,
  })
  @IsInt()
  @Min(0)
  @IsOptional()
  readonly maxTargetWeightLoose: number;
}
