import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { isDto } from '../common/utils/entities';
import { NotificationEntity } from '../entities/notification.entity';
import { CommentDto } from './comment.dto';
import { FriendRequestDto } from './friendRequest.dto';
import { LikeDto } from './like.dto';
import { PostDto } from './post.dto';
import { UserDto } from './user.dto';

export class NotificationDto extends AbstractDto {
  @ApiPropertyOptional()
  seen: boolean;

  @ApiPropertyOptional()
  type: string;

  @ApiPropertyOptional()
  user: UserDto;

  @ApiPropertyOptional()
  from: UserDto;

  @ApiPropertyOptional()
  post: PostDto;

  @ApiPropertyOptional()
  comment: CommentDto;

  @ApiPropertyOptional()
  like: LikeDto;

  @ApiPropertyOptional()
  friendRequest: FriendRequestDto;

  constructor(notification: NotificationEntity) {
    super(notification);
    this.seen = notification.seen;
    this.type = notification.type;
    this.user = isDto(notification.user)
      ? notification.user.toDto()
      : <any>notification.user;
    this.from = isDto(notification.from)
      ? notification.from.toDto()
      : <any>notification.from;
    this.post = isDto(notification.post)
      ? notification.post.toDto()
      : <any>notification.post;
    this.comment = isDto(notification.comment)
      ? notification.comment.toDto()
      : <any>notification.comment;
    this.like = isDto(notification.like)
      ? notification.like.toDto()
      : <any>notification.like;
    this.friendRequest = isDto(notification.friendRequest)
      ? notification.friendRequest.toDto()
      : <any>notification.friendRequest;
  }
}
