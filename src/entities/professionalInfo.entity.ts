import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { ProfessionalInfoDto } from '../dto/professionalInfo.dto';
import { LanguageEnum } from '../shared/services/translation.service';
import { UserEntity } from './user.entity';

@Entity({ name: 'user_professional_info' })
export class ProfessionalInfoEntity extends AbstractEntity<
  ProfessionalInfoDto
> {
  @Column({ type: 'text', nullable: true })
  bio: string;

  @Column({ type: 'text', nullable: true })
  certificate: string;

  @Column({ type: 'text', nullable: true })
  company: string;

  @Column({ type: 'text' })
  profession: string;

  @Column({ type: 'text' })
  specialty: string;

  @Column({ type: 'text', nullable: true })
  address: string;

  @Column({ type: 'text', nullable: true })
  zipCode: string;

  @Column({ type: 'text', nullable: true })
  city: string;

  @Column({
    type: 'text',
    array: true,
    nullable: true,
    default: '{}',
  })
  languages: LanguageEnum[];

  @OneToOne(() => UserEntity, (user) => user.professionalInfo, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  user: UserEntity;

  dtoClass = ProfessionalInfoDto;
}
