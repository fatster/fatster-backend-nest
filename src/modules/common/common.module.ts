import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AppVersionRepository } from '../../repositories/appVersion.repository';
import { ContactUsRepository } from '../../repositories/contactUs.repository';
import { NextFeaturesRepository } from '../../repositories/nextFeatures.repository';
import { UserRepository } from '../../repositories/user.repository';
import { CommonController } from './common.controller';
import { CommonService } from './common.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ContactUsRepository,
      NextFeaturesRepository,
      UserRepository,
      AppVersionRepository,
    ]),
  ],
  controllers: [CommonController],
  exports: [CommonService],
  providers: [CommonService],
})
export class CommonModule {}
