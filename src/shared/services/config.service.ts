import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import * as dotenv from 'dotenv';

import { IAwsConfig } from '../../interfaces/IAwsConfig';
import { IAwsSesConfig } from '../../interfaces/IAwsSesConfig';
import { SnakeNamingStrategy } from '../../snake-naming.strategy';

export class ConfigService {
  constructor() {
    const nodeEnv = this.nodeEnv;
    dotenv.config({
      path: `.env.${nodeEnv}`,
    });

    // Replace \\n with \n to support multiline strings in AWS
    for (const envName of Object.keys(process.env)) {
      process.env[envName] = process.env[envName].replace(
        /\\n/g,
        '\n',
      );
    }
  }

  public get(key: string): string {
    return process.env[key];
  }

  public getNumber(key: string): number {
    return Number(this.get(key));
  }

  get nodeEnv(): string {
    return this.get('NODE_ENV');
  }

  get typeOrmConfig(): TypeOrmModuleOptions {
    const entities = [
      __dirname + '/../../entities/*.entity{.ts,.js}',
      __dirname + '/../../modules/calendar/*.entity{.ts,.js}',
    ];
    const migrations = [__dirname + '/../../migrations/*{.ts,.js}'];

    const common: TypeOrmModuleOptions = {
      entities,
      migrations,
      name: 'default',
      keepConnectionAlive: true,
      type: 'postgres',
      synchronize: false,
      logging: true,
      namingStrategy: new SnakeNamingStrategy(),
    };

    if (this.nodeEnv === 'prod') {
      return {
        ...common,
        replication: {
          master: {
            host: this.get('POSTGRES_HOST'),
            port: this.getNumber('POSTGRES_PORT'),
            username: this.get('POSTGRES_USERNAME'),
            password: this.get('POSTGRES_PASSWORD'),
            database: this.get('POSTGRES_DATABASE'),
          },
          slaves: [
            {
              host: this.get('POSTGRES_REPLICA_V1_HOST'),
              port: this.getNumber('POSTGRES_PORT'),
              username: this.get('POSTGRES_USERNAME'),
              password: this.get('POSTGRES_PASSWORD'),
              database: this.get('POSTGRES_DATABASE'),
            },
            {
              host: this.get('POSTGRES_REPLICA_V2_HOST'),
              port: this.getNumber('POSTGRES_PORT'),
              username: this.get('POSTGRES_USERNAME'),
              password: this.get('POSTGRES_PASSWORD'),
              database: this.get('POSTGRES_DATABASE'),
            },
          ],
        },
      };
    }

    return {
      ...common,
      host: this.get('POSTGRES_HOST'),
      port: this.getNumber('POSTGRES_PORT'),
      username: this.get('POSTGRES_USERNAME'),
      password: this.get('POSTGRES_PASSWORD'),
      database: this.get('POSTGRES_DATABASE'),
    };
  }

  get awsS3Config(): IAwsConfig {
    return {
      accessKeyId: this.get('AWS_S3_ACCESS_KEY_ID'),
      secretAccessKey: this.get('AWS_S3_SECRET_ACCESS_KEY'),
      bucketName: this.get('S3_BUCKET_NAME'),
    };
  }

  get awsSesConfig(): IAwsSesConfig {
    return {
      accessKeyId: this.get('AWS_SES_ACCES_KEY_ID'),
      secretAccessKey: this.get('AWS_SES_SECRET_ACCES_KEY'),
      region: this.get('REGION'),
    };
  }
}
