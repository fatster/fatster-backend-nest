import { ValueTransformer } from 'typeorm';

export class EmailTransformer implements ValueTransformer {
  to(value) {
    return value ? value.toLowerCase() : value;
  }
  from(value) {
    return value;
  }
}
