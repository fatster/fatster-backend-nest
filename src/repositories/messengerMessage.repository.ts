import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { MessengerMessageEntity } from '../entities/messengerMessage.entity';

@EntityRepository(MessengerMessageEntity)
export class MessengerMessageRepository extends Repository<
  MessengerMessageEntity
> {}
