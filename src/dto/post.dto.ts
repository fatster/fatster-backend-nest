import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { PostStatusEnum, PostTypeEnum } from '../common/enum/enum';
import { isDto, isDtos } from '../common/utils/entities';
import { PostEntity } from '../entities/post.entity';
import { ArticleDto } from './article.dto';
import { BeforeAfterDto } from './beforeAfter.dto';
import { CommentDto } from './comment.dto';
import { LikeDto } from './like.dto';
import { PhotoDto } from './photo.dto';
import { ReportDto } from './report.dto';
import { UserDto } from './user.dto';
import { UserActivityDto } from './userActivity.dto';
import { UserNutritionDto } from './userNutrition.dto';
import { UserWeightDto } from './userWeight.dto';
import { VideoDto } from './video.dto';

export class PostDto extends AbstractDto {
  @ApiPropertyOptional()
  text: string;

  @ApiPropertyOptional()
  likes?: LikeDto[];

  @ApiPropertyOptional()
  comments?: CommentDto[];

  @ApiPropertyOptional()
  user: UserDto;

  @ApiPropertyOptional()
  status: PostStatusEnum;

  @ApiPropertyOptional()
  userActivity?: UserActivityDto;

  @ApiPropertyOptional()
  userNutrition?: UserNutritionDto;

  @ApiPropertyOptional()
  userWeight?: UserWeightDto;

  @ApiPropertyOptional()
  weightDifference?: number;

  @ApiPropertyOptional()
  reports?: ReportDto[];

  @ApiPropertyOptional()
  type?: PostTypeEnum;

  @ApiPropertyOptional()
  commentsCount?: number;

  @ApiPropertyOptional()
  likesCount?: number;

  @ApiPropertyOptional()
  tags?: UserDto[];

  @ApiPropertyOptional()
  photos?: PhotoDto[];

  @ApiPropertyOptional()
  videos?: VideoDto[];

  @ApiPropertyOptional()
  beforeAfter?: BeforeAfterDto[];

  article: ArticleDto;

  mentionedUsersPseudo: string[];

  hashtags: string[];

  constructor(post: PostEntity) {
    super(post);
    this.text = post.text;
    this.mentionedUsersPseudo = post.mentionedUsersPseudo;
    this.hashtags = post.hashtags;
    this.status = post.status ? post.status : PostStatusEnum.PUBLIC;
    this.likes = isDto(post.likes)
      ? post.likes.toDtos()
      : <any>post.likes;
    this.comments = isDtos(post.comments)
      ? post.comments.toDtos()
      : <any>post.comments;
    this.tags = isDtos(post.tags)
      ? post.tags.toDtos()
      : <any>post.tags;
    this.user = isDto(post.user) ? post.user.toDto() : <any>post.user;
    this.userActivity = isDto(post.userActivity)
      ? post.userActivity.toDto()
      : <any>post.userActivity;
    this.userNutrition = isDto(post.userNutrition)
      ? post.userNutrition.toDto()
      : post.userNutrition;
    this.userWeight = isDto(post.userWeight)
      ? post.userWeight.toDto()
      : post.userWeight;
    this.type = post.type;
    this.article = isDto(post.article)
      ? post.article.toDto()
      : post.article;
    this.commentsCount = post.commentsCount;
    this.likesCount = post.likesCount;
    this.photos = isDtos(post.photos)
      ? post.photos.toDtos
      : <any>post.photos;
    this.videos = isDtos(post.videos)
      ? post.videos.toDtos
      : <any>post.videos;
    this.beforeAfter = isDtos(post.beforeAfter)
      ? post.beforeAfter.toDto()
      : <any>post.beforeAfter;
  }
}
