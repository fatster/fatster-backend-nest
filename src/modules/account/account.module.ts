import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { FriendModule } from '../../modules/friend/friend.module';
import { NotificationModule } from '../../modules/notification/notification.module';
import { FriendRepository } from '../../repositories/friend.repository';
import { NotificationRepository } from '../../repositories/notification.repository';
import { UserRepository } from '../../repositories/user.repository';
import { UserPreferencesRepository } from '../../repositories/userPreferences.repository';
import { UserWeightRepository } from '../../repositories/userWeight.repository';
import { UserModule } from '..//user/user.module';
import { AccountController } from './account.controller';
import { AccountService } from './account.service';

@Module({
  imports: [
    forwardRef(() => FriendModule),
    forwardRef(() => UserModule),
    forwardRef(() => NotificationModule),
    TypeOrmModule.forFeature([
      NotificationRepository,
      UserRepository,
      UserWeightRepository,
      FriendRepository,
      UserPreferencesRepository,
    ]),
  ],
  controllers: [AccountController],
  exports: [AccountService],
  providers: [AccountService],
})
export class AccountModule {}
