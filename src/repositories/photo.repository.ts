/* eslint-disable @typescript-eslint/no-unused-vars */
import { In, Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { PostTypeEnum, TypeUploadEnum } from '../common/enum/enum';
import { PhotoEntity } from '../entities/photo.entity';

@EntityRepository(PhotoEntity)
export class PhotoRepository extends Repository<PhotoEntity> {
  public async updatePostId(postId: string, photoIds: string[]) {
    await this.createQueryBuilder('photo')
      .update()
      .set({ post: <any>postId })
      .whereInIds(photoIds)
      .execute();
    return this.find({
      where: { id: In(photoIds) },
    });
  }

  public async savePhotos(names: string[]) {
    const data = names.map((name) =>
      this.create({ path: `feed/${name}` }),
    );
    return this.save(data);
  }

  public async addPhotos(postId: string, filenames: string[]) {
    const promiseArray = [];
    filenames.map((filename) => {
      const photo = this.create();
      photo.path = `feed/${filename}`;
      photo.post = <any>postId;
      promiseArray.push(this.save(photo));
    });
    return Promise.all(promiseArray);
  }
}
