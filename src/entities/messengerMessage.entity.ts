import { Column, Entity, ManyToOne } from 'typeorm';

import { AbstractEntity } from '../common/abstract.entity';
import { MessengerMessageDto } from '../dto/messengerMessage.dto';
import { MessengerRoomEntity } from './messengerRoom.entity';

@Entity({ name: 'messenger_message' })
export class MessengerMessageEntity extends AbstractEntity<
  MessengerMessageDto
> {
  @Column({ nullable: false })
  text: string;

  @ManyToOne(
    () => MessengerRoomEntity,
    (messengerRoom) => messengerRoom.messages,
  )
  messengerRoom: MessengerRoomEntity;

  dtoClass = MessengerMessageDto;
}
