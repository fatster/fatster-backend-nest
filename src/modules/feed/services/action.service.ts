import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { In, IsNull } from 'typeorm';

import {
  LikeTypeEnum,
  NotificationEnum,
} from '../../../common/enum/enum';
import { CommentEntity } from '../../../entities/comment.entity';
import { FriendEntity } from '../../../entities/friend.entity';
import { UserEntity } from '../../../entities/user.entity';
import { CommentNotFoundException } from '../../../exceptions/comment-not-found.exception';
import { LikeExistsException } from '../../../exceptions/like-exists.exception';
import { PostNotFoundException } from '../../../exceptions/post-not-found.exception';
import { CommentRepository } from '../../../repositories/comment.repository';
import { FriendRepository } from '../../../repositories/friend.repository';
import { FriendRequestRepository } from '../../../repositories/friendRequest.repository';
import { LikeRepository } from '../../../repositories/like.repository';
import { PostRepository } from '../../../repositories/post.repository';
import { UserRepository } from '../../../repositories/user.repository';
import { NotificationService } from '../../notification/notification.service';
import { AddCommentDto } from '.././dto/addComment.dto';
import { EditCommentDto } from '.././dto/editComment.dto';
import { RemoveCommentDto } from '.././dto/removeComment.dto';

@Injectable()
export class FeedActionService {
  constructor(
    private readonly _commentRepository: CommentRepository,
    private readonly _likeRepository: LikeRepository,
    private readonly _postRepository: PostRepository,
    private readonly _userRepository: UserRepository,
    private readonly _friendRepository: FriendRepository,
    private readonly _notificationService: NotificationService,
    private readonly _friendRequestRepository: FriendRequestRepository,
  ) {}

  public async likePost(userId: string, postId: string) {
    // @Todo, remove this, and use composite primary key on user/post
    // but it actually doesn't work with typeorm
    const post = await this._postRepository.findOne(postId, {
      loadRelationIds: true,
    });
    if (!post) {
      throw new PostNotFoundException();
    }

    const existingLike = await this._likeRepository.findByUserPost(
      userId,
      postId,
    );

    if (!existingLike) {
      let like = this._likeRepository.create();
      like.userId = userId;
      like.postId = postId;
      like.type = LikeTypeEnum.POST;

      like = await this._likeRepository.save(like);

      await this._notificationService.addNotification({
        postId,
        userId: <any>post.user,
        from: userId,
        type: NotificationEnum.LIKE_POST,
        likeId: like.id,
      });

      return like;
    }
    throw new LikeExistsException();
  }

  public async unLikePost(userId: string, postId: string) {
    if (!(await this._postRepository.findOne(postId))) {
      throw new PostNotFoundException();
    }

    const existingLike = await this._likeRepository.findByUserPost(
      userId,
      postId,
    );
    if (existingLike) {
      return this._likeRepository.remove(existingLike);
    }
    throw new NotFoundException('not_found_likes');
  }

  public async likeComment(userId: string, commentId: string) {
    const comment = await this._commentRepository.findOne(commentId, {
      loadRelationIds: true,
    });

    if (!comment) {
      throw new CommentNotFoundException();
    }

    const existingLike = await this._likeRepository.findByUserComment(
      userId,
      commentId,
    );
    if (!existingLike) {
      let like = this._likeRepository.create();
      like.userId = userId;
      like.commentId = commentId;
      like.type = LikeTypeEnum.COMMENT;

      like = await this._likeRepository.save(like);

      this._notificationService.addNotification({
        commentId,
        from: userId,
        postId: comment.postId,
        userId: <any>comment.user,
        type: NotificationEnum.LIKE_COMMENT,
        likeId: like.id,
      });

      return like;
    }
    throw new LikeExistsException();
  }

  public async unLikeComment(userId: string, commentId: string) {
    if (!(await this._commentRepository.findOne(commentId))) {
      throw new CommentNotFoundException();
    }

    const existingLike = await this._likeRepository.findByUserComment(
      userId,
      commentId,
    );
    if (existingLike) {
      return this._likeRepository.remove(existingLike);
    }
    return new NotFoundException();
  }

  public async getComments(userId: string, postId: string) {
    if (!(await this._postRepository.findOne(postId))) {
      throw new PostNotFoundException();
    }

    const comments = await this._commentRepository
      .createQueryBuilder('comments')
      .select([
        'comments.id',
        'comments.createdAt',
        'comments.updatedAt',
        'comments.text',
        'comments.mentionedUsersPseudo',
        'comments.likesCount',
        'user.id',
        'user.pseudo',
        'user.photoId',
        'user.gender',
        'likes.userId',
        'children.id',
        'children.createdAt',
        'children.mentionedUsersPseudo',
        'children.updatedAt',
        'children.text',
        'children.user.id',
        'children.user.pseudo',
        'children.user.photoId',
        'children.user.gender',
      ])
      .leftJoin('comments.children', 'children')
      .leftJoin('comments.user', 'user')
      .leftJoin('comments.likes', 'likes')
      .leftJoin('children.user', 'children.user')
      .where({ postId, parent: IsNull() })
      .orderBy('comments.createdAt', 'ASC')
      .addOrderBy('children.createdAt', 'ASC')
      .getMany();

    const results = [];
    comments.forEach((comment) => {
      const isLiked = comment.likes.find(
        (like) => like.userId === userId,
      )
        ? true
        : false;

      delete comment.likes;
      results.push({ ...comment, isLiked });
    });
    return results;
  }

  public async addComment(addCommentRequestDto: AddCommentDto) {
    const comment = this._commentRepository.create();
    comment.postId = addCommentRequestDto.postId;
    comment.userId = addCommentRequestDto.userId;
    comment.text = addCommentRequestDto.text;

    let parent: CommentEntity;
    if (addCommentRequestDto.parentId) {
      parent = await this._commentRepository.findOneOrFail({
        where: { id: addCommentRequestDto.parentId },
        relations: ['parent'],
      });

      comment.parent = parent.parent
        ? parent.parent
        : <any>addCommentRequestDto.parentId;
    }

    const post = await this._postRepository.findOne(
      addCommentRequestDto.postId,
      {
        loadRelationIds: true,
      },
    );

    const commentWithMentions = await this._applyMentionedUsersInComment(
      comment,
    );

    const addedComment = await this._commentRepository.save(
      commentWithMentions,
    );

    if (addCommentRequestDto.parentId) {
      this._notificationService.addNotification({
        from: addCommentRequestDto.userId,
        postId: addCommentRequestDto.postId,
        userId: parent.userId,
        type: NotificationEnum.COMMENT_COMMENT,
        commentId: addedComment.id,
      });
    } else {
      this._notificationService.addNotification({
        from: addCommentRequestDto.userId,
        postId: addCommentRequestDto.postId,
        userId: <any>post.user,
        type: NotificationEnum.COMMENT_POST,
        commentId: addedComment.id,
      });
    }

    this._notifyMentionedUsersInComment(
      addedComment,
      addCommentRequestDto.userId,
    );

    return this._commentRepository
      .createQueryBuilder('comments')
      .select([
        'comments.id',
        'comments.createdAt',
        'comments.updatedAt',
        'comments.text',
        'comments.mentionedUsersPseudo',
        'user.id',
        'user.pseudo',
        'user.photoId',
        'user.gender',
        'post_id',
      ])
      .leftJoin('comments.user', 'user')
      .where({ id: addedComment.id })
      .getOne();
  }

  public async editComment(editCommentDto: EditCommentDto) {
    const comment = await this._commentRepository.findOne(
      editCommentDto.commentId,
    );

    if (!comment) {
      throw new CommentNotFoundException();
    }

    if (comment.userId === editCommentDto.userId) {
      comment.text = editCommentDto.text;
      const commentWithMentions = await this._applyMentionedUsersInComment(
        comment,
      );

      const updatedComment = await this._commentRepository.save(
        commentWithMentions,
      );

      this._notifyMentionedUsersInComment(
        updatedComment,
        editCommentDto.userId,
      );

      return updatedComment;
    }
    throw new BadRequestException(
      "Unauthorized action : you don't own the comment",
    );
  }

  private async _notifyMentionedUsersInComment(
    comment: CommentEntity,
    userId: string,
  ) {
    if (comment.text) {
      const haveMention = comment.text.match(
        /@[\p{L}0-9_.-]*[^\s.?!,:;()^*&$#]/gu,
      );
      if (haveMention && haveMention.length) {
        const pseudos = haveMention.map((w) => w.slice(1));

        for (const pseudo of pseudos) {
          const user = await this._userRepository.findOne({ pseudo });
          if (user) {
            this._notificationService.addNotification({
              type: NotificationEnum.MENTION_USER_IN_COMMENT,
              postId: comment.postId,
              userId: user.id,
              from: userId,
            });
          }
        }
      }
    }
  }

  private async _applyMentionedUsersInComment(
    comment: CommentEntity,
  ): Promise<CommentEntity> {
    if (comment.text) {
      const mentions = comment.text.match(
        /@[\p{L}0-9_.-]*[^\s.?!,:;()^*&$#]/gu,
      );

      if (mentions && mentions.length) {
        const pseudos = mentions.map((w) => w.slice(1));

        const us = await this._userRepository.find({
          where: { pseudo: In(pseudos) },
        });
        comment.mentionedUsersPseudo = (us || []).map(
          ({ pseudo }) => pseudo,
        );
      }
    }
    return comment;
  }

  public async removeComment(
    removeCommentRequestDto: RemoveCommentDto,
  ) {
    const comment = await this._commentRepository.findOne(
      removeCommentRequestDto.commentId,
    );

    if (!comment) {
      throw new CommentNotFoundException();
    }

    if (comment.userId === removeCommentRequestDto.userId) {
      return this._commentRepository.remove(comment);
    }
    throw new BadRequestException(
      "Unauthorized action : you don't own the post",
    );
  }

  public async getTags(userId: string, postId: string) {
    const post = await this._postRepository
      .createQueryBuilder('post')
      .select([
        'post.id',
        'tags.id',
        'tags.pseudo',
        'tags.photoId',
        'tags.gender',
      ])
      .leftJoin('post.tags', 'tags')
      .where({ id: postId })
      .getOne();

    if (!post) {
      throw new PostNotFoundException();
    }

    const tags = post.tags;
    if (tags.length > 0) {
      const userIds = tags.map((tag) => tag.id);
      const friendRequests = await this._friendRequestRepository
        .createQueryBuilder('friendRequests')
        .where([
          { from: userId, to: In(userIds) },
          { to: userId, from: In(userIds) },
        ])
        .leftJoinAndSelect('friendRequests.from', 'from')
        .leftJoinAndSelect('friendRequests.to', 'to')
        .getMany();

      const queries = [];
      for (const tag of tags) {
        const query = this._friendRepository
          .createQueryBuilder('friends_user1')
          .select(['user_id', 'friend_id'])
          .where({ user: userId })
          .innerJoin(
            (qb) =>
              qb
                .subQuery()
                .select(['user_id', 'friend_id'])
                .from(FriendEntity, 'friend')
                .where(`user_id= '${tag.id}'`),
            'friends_user2',
            'friends_user1.friend_id = friends_user2.friend_id',
          )
          .getCount();
        queries.push(query);
      }

      const results = await Promise.all(queries);

      const response = [];
      tags.forEach((tag, index) => {
        const elem: any = { ...tag };
        if (tag.id !== userId) {
          elem.friendRequest = {
            status: 'not invited',
          };
          elem.commonFriends = results[index];
          const friendRequest = friendRequests.find(
            (el) => el.from.id === tag.id || el.to.id === tag.id,
          );
          if (friendRequest) {
            elem.friendRequest = {
              status: friendRequest.status,
              id: friendRequest.id,
              isOut: friendRequest.from.id === userId,
            };
          }
        }
        response.push(elem);
      });

      return response;
    }
    return [];
  }

  public async getLikes(
    id: string,
    me: UserEntity,
    type: LikeTypeEnum,
  ) {
    if (
      type === LikeTypeEnum.COMMENT &&
      !(await this._commentRepository.findOne(id))
    ) {
      throw new CommentNotFoundException();
    }
    if (
      type === LikeTypeEnum.POST &&
      !(await this._postRepository.findOne(id))
    ) {
      throw new PostNotFoundException();
    }

    const likes = await this._likeRepository
      .createQueryBuilder('like')
      .select([
        'like.id',
        'like.createdAt',
        'like.type',
        'user.id',
        'user.pseudo',
        'user.photoId',
        'user.gender',
      ])
      .leftJoin('like.user', 'user')
      .where('like.postId = :id OR like.commentId = :id', { id })
      .andWhere('like.type = :type', { type })
      .getMany();

    if (likes.length > 0) {
      const userIds = likes.map((like) => like.user.id);
      const friendRequests = await this._friendRequestRepository
        .createQueryBuilder('friendRequests')
        .where([
          { from: me.id, to: In(userIds) },
          { to: me.id, from: In(userIds) },
        ])
        .leftJoinAndSelect('friendRequests.from', 'from')
        .leftJoinAndSelect('friendRequests.to', 'to')
        .getMany();

      const queries = [];
      for (const like of likes) {
        const query = this._friendRepository
          .createQueryBuilder('friends_user1')
          .select(['user_id', 'friend_id'])
          .where({ user: me.id })
          .innerJoin(
            (qb) =>
              qb
                .subQuery()
                .select(['user_id', 'friend_id'])
                .from(FriendEntity, 'friend')
                .where(`user_id= '${like.user.id}'`),
            'friends_user2',
            'friends_user1.friend_id = friends_user2.friend_id',
          )
          .getCount();
        queries.push(query);
      }

      const results = await Promise.all(queries);

      const response = [];
      let myLike;
      likes.forEach((like, index) => {
        const elem: any = { ...like };
        if (like.user.id !== me.id) {
          elem.user.friendRequest = {
            status: 'not invited',
          };
          elem.user.commonFriends = results[index];
          const friendRequest = friendRequests.find(
            (el) =>
              el.from.id === like.user.id ||
              el.to.id === like.user.id,
          );
          if (friendRequest) {
            elem.user.friendRequest = {
              status: friendRequest.status,
              id: friendRequest.id,
              isOut: friendRequest.from.id === me.id,
            };
          }
          response.push(elem);
        }
        if (like.user.id === me.id) {
          myLike = {
            createdAt: like.createdAt,
            isLiked: true,
            id: me.id,
            pseudo: me.pseudo,
            photoId: me.photoId,
          };
        }
      });
      if (myLike) {
        response.unshift(myLike);
      }
      return response;
    }
    return [];
  }
}
