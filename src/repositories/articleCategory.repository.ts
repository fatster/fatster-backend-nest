import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { ArticleCategoryEntity } from '../entities/articleCategory.entity';

@EntityRepository(ArticleCategoryEntity)
export class ArticleCategoryRepository extends Repository<
  ArticleCategoryEntity
> {}
