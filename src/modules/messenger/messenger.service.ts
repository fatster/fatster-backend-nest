import { Injectable } from '@nestjs/common';

import { MessengerMessageRepository } from '../../repositories/messengerMessage.repository';
import { GetMessageDto } from './dto/getMessage.dto';
import { PostMessageDto } from './dto/postMessage.dto';

@Injectable()
export class MessengerService {
  constructor(
    private _messengerMessageRepository: MessengerMessageRepository,
  ) {}
  async getMessageByRoomId(getMessageDto: GetMessageDto) {
    // TODO Check if the user can access to this chatroom
    return this._messengerMessageRepository.find({
      where: { id: getMessageDto.messengerRoomId },
    });
  }

  async postMessage(postMessageDto: PostMessageDto) {
    // Todo Check if the user can post in this chatroom
    const message = this._messengerMessageRepository.create();
    message.text = postMessageDto.text;
    message.messengerRoom = <any>postMessageDto.messengerRoomId;
    return this._messengerMessageRepository.save(message);
  }
}
