'use strict';

import { IsNotEmpty, IsString } from 'class-validator';

export class GetPostByIdDto {
  @IsNotEmpty()
  @IsString()
  readonly postId: string;

  constructor(postId: string) {
    this.postId = postId;
  }
}
