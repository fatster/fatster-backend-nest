import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { VerificationCodeEntity } from '../entities/verificationCode.entity';

@EntityRepository(VerificationCodeEntity)
export class VerificationCodeRepository extends Repository<
  VerificationCodeEntity
> {}
