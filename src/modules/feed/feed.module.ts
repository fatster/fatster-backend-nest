import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ActivityRepository } from '../../repositories/activity.repository';
import { ArticleRepository } from '../../repositories/article.repository';
import { BeforeAfterRepository } from '../../repositories/beforeAfter.repository';
import { BlockRepository } from '../../repositories/block.repository';
import { CoachRepository } from '../../repositories/coach.repository';
import { CommentRepository } from '../../repositories/comment.repository';
import { FoodRepository } from '../../repositories/food.repository';
import { FriendRepository } from '../../repositories/friend.repository';
import { FriendRequestRepository } from '../../repositories/friendRequest.repository';
import { HashtagRepository } from '../../repositories/hashtag.repository';
import { LikeRepository } from '../../repositories/like.repository';
import { PhotoRepository } from '../../repositories/photo.repository';
import { PostRepository } from '../../repositories/post.repository';
import { UserRepository } from '../../repositories/user.repository';
import { UserActivityRepository } from '../../repositories/userActivity.repository';
import { UserNutritionRepository } from '../../repositories/userNutrition.repository';
import { UserNutritionFoodRepository } from '../../repositories/userNutritionFood.repository';
import { UserPreferencesRepository } from '../../repositories/userPreferences.repository';
import { UserWeightRepository } from '../../repositories/userWeight.repository';
import { AuthModule } from '../auth/auth.module';
import { NotificationModule } from '../notification/notification.module';
import { FeedController } from './feed.controller';
import { FeedActionService } from './services/action.service';
import { FeedPostService } from './services/feed-post.service';
import { FeedFeedService } from './services/feed.service';
import { NutritionPostService } from './services/nutrition-post.service';
import { PostService } from './services/post.service';

@Module({
  imports: [
    forwardRef(() => AuthModule),
    forwardRef(() => NotificationModule),
    TypeOrmModule.forFeature([
      CommentRepository,
      LikeRepository,
      PostRepository,
      UserWeightRepository,
      UserActivityRepository,
      UserNutritionRepository,
      PhotoRepository,
      FriendRepository,
      UserRepository,
      FriendRequestRepository,
      BlockRepository,
      ArticleRepository,
      CoachRepository,
      UserPreferencesRepository,
      ActivityRepository,
      UserNutritionFoodRepository,
      BeforeAfterRepository,
      HashtagRepository,
      FoodRepository,
    ]),
  ],
  controllers: [FeedController],
  exports: [
    FeedFeedService,
    FeedPostService,
    PostService,
    FeedActionService,
  ],
  providers: [
    FeedFeedService,
    FeedPostService,
    PostService,
    FeedActionService,
    NutritionPostService,
  ],
})
export class FeedModule {}
