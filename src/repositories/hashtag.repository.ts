import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { HashtagEntity } from '../entities/hashtag.entity';

@EntityRepository(HashtagEntity)
export class HashtagRepository extends Repository<HashtagEntity> {}
