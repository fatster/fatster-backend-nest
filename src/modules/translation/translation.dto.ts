import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsBooleanString,
  IsIn,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

import {
  Language,
  LanguageEnum,
  LangVersion,
  Version,
} from '../../shared/services/translation.service';

export class GetTranslationDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  @IsIn(Object.values(LanguageEnum))
  readonly id: Language;

  @IsOptional()
  @IsBooleanString()
  @ApiPropertyOptional()
  readonly disableCache?: boolean;

  @IsString()
  @IsOptional()
  @ApiPropertyOptional()
  @IsIn(Object.values(LangVersion))
  readonly version?: Version;
}
