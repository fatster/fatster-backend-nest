import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';

import { PayloadSuccessDto } from '../../common/dto/PayloadSuccessDto';
import { AuthUser } from '../../decorators/auth-user.decorator';
import { UserDto } from '../../dto/user.dto';
import { UserEntity } from '../../entities/user.entity';
import { AuthGuard } from '../../guards/auth.guard';
import { AuthUserInterceptor } from '../../interceptors/auth-user-interceptor.service';
import { AddReportDto } from './dto/addReport.dto';
import { BlockUserDto } from './dto/blockUser.dto';
import { CalorieByActivityDto } from './dto/calorieByActivity.dto';
import { UnBlockUserDto } from './dto/unblockUser.dto';
import { UserService } from './user.service';

@Controller('user')
@ApiTags('User')
export class UserController {
  constructor(private _userService: UserService) {}

  @Get('/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @ApiOkResponse({ type: UserDto, description: 'me' })
  async getUserById(
    @Param('id') id,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const responseUser = await this._userService.findOneById(
      id,
      user,
    );
    return {
      message: 'user',
      data: {
        user: responseUser[0],
        friendRequest: {
          id: responseUser[1],
          status: responseUser[2],
          isOut: responseUser[3],
        },
      },
    };
  }

  @Post('report/')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({ type: UserDto, description: 'user stat' })
  async addReport(
    @Body() addReportDto: AddReportDto,
    @AuthUser() user: UserEntity,
  ) {
    addReportDto.userId = <any>user.id;
    await this._userService.addReport(addReportDto);
    return { message: 'report has been sent' };
  }

  @Get('calorie/me')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async getCalorie(@AuthUser() user: UserEntity) {
    const stat = await this._userService.getCalorie(user.id);
    return { message: 'user', data: stat };
  }

  @Get('calorie/byActivity')
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @HttpCode(HttpStatus.OK)
  async getCalorieByActivity(
    @AuthUser() user: UserEntity,
    @Query() calorieByActivityDto: CalorieByActivityDto,
  ) {
    const { activityId, duration } = calorieByActivityDto;
    const calories = await this._userService.getCalorieByActivity(
      user.id,
      activityId,
      duration,
    );
    return { message: 'user', data: calories };
  }

  @Get('stat/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @ApiOkResponse({ type: UserDto, description: 'user stat' })
  async getStat(@Param('id') id) {
    const stat = await this._userService.getStat(id);
    return { message: 'user', data: stat };
  }

  @Get('report/today')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({ type: UserDto, description: 'user stat' })
  async getTodayReport(@AuthUser() user: UserEntity) {
    const stat = await this._userService.getTodayReport(user);
    return { message: 'user todays report', data: stat };
  }

  @Get('report/history')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({ type: UserDto, description: 'user stat' })
  async getDaysHistory(@AuthUser() user: UserEntity) {
    const stat = await this._userService.getDaysHistory(user);
    return { message: 'user todays report', data: stat };
  }

  @Get('/block/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @ApiOkResponse({ description: 'User has been blocked.' })
  async blockUser(
    @Param('id') id,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    await this._userService.blockUser(new BlockUserDto(id, user));
    return { message: 'User has been blocked.' };
  }

  @Get('/unBlock/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @ApiOkResponse({ description: 'User has been unblocked.' })
  async unBlockUser(
    @Param('id') id,
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    await this._userService.unBlockUser(new UnBlockUserDto(id, user));
    return { message: 'User has been unblocked.' };
  }

  @Get('/blockedUsers/me')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Get blocked users.',
  })
  async getBlockedUsers(
    @AuthUser() user: UserEntity,
  ): Promise<PayloadSuccessDto> {
    const blockedUsers = await this._userService.getBlockedUsers(
      user.id,
    );
    return {
      message: 'Blocked Users',
      data: { blockedUsers },
    };
  }

  @Post('/lastConnexion')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @UseInterceptors(AuthUserInterceptor)
  @ApiBearerAuth()
  async updateLastConnexion(@AuthUser() user: UserEntity) {
    await this._userService.updateLastConnexion(user.id);
    return { message: 'last connexion has been updated' };
  }
}
