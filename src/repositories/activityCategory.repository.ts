import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { ActivityCategoryEntity } from '../entities/activityCategory.entity';

@EntityRepository(ActivityCategoryEntity)
export class ActivityCategoryRepository extends Repository<
  ActivityCategoryEntity
> {}
