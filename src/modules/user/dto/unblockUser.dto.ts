'use strict';

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

import { UserEntity } from '../../../entities/user.entity';

export class UnBlockUserDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  against: string;

  @IsString()
  @IsNotEmpty()
  from: string;

  constructor(against: string, from: UserEntity) {
    this.against = against;
    this.from = from.id;
  }
}
