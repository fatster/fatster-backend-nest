import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  ArrayMaxSize,
  ArrayMinSize,
  IsArray,
  IsBoolean,
  IsDateString,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  Max,
  Min,
} from 'class-validator';

import {
  HealthyTypeEnum,
  PostStatusEnum,
  PostTypeEnum,
  TimeOfTheDayEnum,
} from '../../../common/enum/enum';
import { CreateNutritionFoodDto } from './create-nutrition-food.dto';

const mainPostGroup = [
  PostTypeEnum.SIMPLE,
  PostTypeEnum.ACTIVITY,
  PostTypeEnum.BEFORE_AFTER,
  PostTypeEnum.FOOD,
  PostTypeEnum.WEIGHT,
];

export class CreatePostDto {
  @IsNotEmpty()
  @IsEnum(PostTypeEnum)
  @ApiProperty({ enum: Object.values(PostTypeEnum) })
  readonly postType: PostTypeEnum;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional({ groups: mainPostGroup })
  readonly text: string;

  @ApiPropertyOptional()
  @IsEnum(PostStatusEnum)
  @IsOptional({ groups: mainPostGroup })
  readonly status?: PostStatusEnum = PostStatusEnum.PUBLIC;

  @IsArray({ groups: mainPostGroup })
  @ArrayMinSize(2, { groups: [PostTypeEnum.BEFORE_AFTER] })
  @ArrayMaxSize(2, { groups: [PostTypeEnum.BEFORE_AFTER] })
  @ArrayMaxSize(1, {
    groups: [
      PostTypeEnum.FOOD,
      PostTypeEnum.ACTIVITY,
      PostTypeEnum.WEIGHT,
      PostTypeEnum.SIMPLE,
    ],
  })
  @ApiPropertyOptional()
  readonly photos: string[];

  @IsArray({
    groups: [
      PostTypeEnum.FOOD,
      PostTypeEnum.ACTIVITY,
      PostTypeEnum.WEIGHT,
    ],
  })
  @IsOptional({
    groups: [
      PostTypeEnum.FOOD,
      PostTypeEnum.ACTIVITY,
      PostTypeEnum.WEIGHT,
    ],
  })
  @ApiPropertyOptional()
  readonly friendsToTag: string[];

  @IsArray({ groups: [PostTypeEnum.FOOD] })
  @ArrayMinSize(1, { groups: [PostTypeEnum.FOOD] })
  @IsNotEmpty({ groups: [PostTypeEnum.FOOD] })
  @ApiPropertyOptional()
  readonly foods: CreateNutritionFoodDto[];

  @IsDateString({ groups: [PostTypeEnum.FOOD] })
  @IsOptional({ groups: [PostTypeEnum.FOOD] })
  @ApiPropertyOptional()
  readonly nutritionDate: Date;

  @IsEnum(HealthyTypeEnum, { groups: [PostTypeEnum.FOOD] })
  @ApiProperty({ enum: Object.values(HealthyTypeEnum) })
  readonly healthyType: HealthyTypeEnum;
  @IsNotEmpty({ groups: [PostTypeEnum.FOOD] })
  @IsEnum(TimeOfTheDayEnum, { groups: [PostTypeEnum.FOOD] })
  @ApiProperty({ enum: Object.values(TimeOfTheDayEnum) })
  readonly timeOfTheDay: TimeOfTheDayEnum;

  @ApiProperty()
  @IsNotEmpty({ groups: [PostTypeEnum.ACTIVITY] })
  @IsString({ groups: [PostTypeEnum.ACTIVITY] })
  readonly activityId: string;

  @ApiProperty()
  @IsNumber({}, { groups: [PostTypeEnum.ACTIVITY] })
  @IsNotEmpty({ groups: [PostTypeEnum.ACTIVITY] })
  readonly duration: number;

  @ApiProperty()
  @IsNotEmpty({ groups: [PostTypeEnum.BEFORE_AFTER] })
  @IsNumber({}, { groups: [PostTypeEnum.BEFORE_AFTER] })
  @Min(50, { groups: [PostTypeEnum.BEFORE_AFTER] })
  @Max(200, { groups: [PostTypeEnum.BEFORE_AFTER] })
  readonly weightBefore: number;

  @ApiProperty()
  @IsNotEmpty({ groups: [PostTypeEnum.BEFORE_AFTER] })
  @IsNumber({}, { groups: [PostTypeEnum.BEFORE_AFTER] })
  @Min(50, { groups: [PostTypeEnum.BEFORE_AFTER] })
  @Max(200, { groups: [PostTypeEnum.BEFORE_AFTER] })
  readonly weightAfter: number;

  @ApiProperty()
  @IsNumber({}, { groups: [PostTypeEnum.WEIGHT] })
  @IsNotEmpty({ groups: [PostTypeEnum.WEIGHT] })
  @Min(0, { groups: [PostTypeEnum.WEIGHT] })
  readonly weight: number;

  // TODO REPLACE WITH POST STATUS FOR ALL POSTS
  @ApiProperty()
  @IsBoolean({ groups: [PostTypeEnum.WEIGHT] })
  @IsNotEmpty({ groups: [PostTypeEnum.WEIGHT] })
  readonly postOnFeed: boolean;

  @ApiProperty()
  @IsString({ groups: [PostTypeEnum.SHARED_ARTICLE] })
  @IsNotEmpty({ groups: [PostTypeEnum.SHARED_ARTICLE] })
  readonly articleId: string;
}
