import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { ArticleEntity } from '../entities/article.entity';

export class ArticleDto extends AbstractDto {
  @ApiPropertyOptional()
  nameFr: string;

  @ApiPropertyOptional()
  nameEn: string;

  @ApiPropertyOptional()
  descriptionEn: string;

  @ApiPropertyOptional()
  descriptionFr: string;

  @ApiPropertyOptional()
  url: string;

  imageId: string;

  constructor(article: ArticleEntity) {
    super(article);
    this.nameEn = article.nameEn;
    this.nameFr = article.nameFr;
    this.imageId = article.imageId;
  }
}
