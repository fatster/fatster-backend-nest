import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { ContactUsEntity } from '../entities/contactUs.entity';

@EntityRepository(ContactUsEntity)
export class ContactUsRepository extends Repository<
  ContactUsEntity
> {}
