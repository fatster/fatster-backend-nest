import { Injectable } from '@nestjs/common';
import * as parse from 'csv-parse';

import { FoodEntity } from '../../entities/food.entity';
import { FoodRepository } from '../../repositories/food.repository';

@Injectable()
export class FoodService {
  constructor(private readonly _foodRepository: FoodRepository) {}

  public async getAllFood() {
    return this._foodRepository.find();
  }

  public async importFood(csv: string) {
    const food = await this._parseCSV(csv);

    return this._foodRepository.save(food);
  }

  private async _parseCSV(csv: string): Promise<FoodEntity[]> {
    return new Promise((resolve, reject) => {
      parse(csv, { delimiter: `,` }, (err, output) => {
        const [header, ...data] = output;

        const columnToIndexMap = header
          .map((i) => i.trim())
          .reduce((acc, columName, index) => {
            if (columName) {
              acc[index] = columName;
            }
            return acc;
          }, {});

        if (!Object.keys(columnToIndexMap).length) {
          return reject('empty header');
        }

        const foods = data.map((values) =>
          values.reduce((acc, v, i) => {
            const key = columnToIndexMap[i];
            if (key) {
              acc[key] = v;
            }
            return acc;
          }, {}),
        );

        return resolve(foods);
      });
    });
  }
}
