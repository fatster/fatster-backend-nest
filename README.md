<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://www.fatster.app/wp-content/uploads/2020/04/favicon.png" width="320" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">Fatster API</p>


## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Add .env\* files

Copy **.env.development**, **.env.prod and** **.env.staging** in the project folder (root)

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Deployment

Install the Serverless Framework, then set up it using your AWS credential :
https://www.serverless.com/framework/docs/getting-started/

```bash
# staging
$ npm run deploy:staging

# production
$ npm run deploy:prod
```
