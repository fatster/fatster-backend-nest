import { EntityRepository, Repository } from 'typeorm';

import { BeforeAfterEntity } from '../entities/beforeAfter.entity';

@EntityRepository(BeforeAfterEntity)
export class BeforeAfterRepository extends Repository<
  BeforeAfterEntity
> {}
