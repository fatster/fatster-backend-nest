'use strict';

export interface IAwsSesConfig {
  accessKeyId: string;
  secretAccessKey: string;
  region: string;
}
