'use strict';

import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString } from 'class-validator';

export class UserMailDto {
  @IsString()
  @IsEmail()
  @ApiProperty()
  readonly email: string;

  @ApiProperty()
  readonly language: string;
}
