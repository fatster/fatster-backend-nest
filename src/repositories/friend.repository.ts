import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { FriendEntity } from '../entities/friend.entity';

@EntityRepository(FriendEntity)
export class FriendRepository extends Repository<FriendEntity> {
  public async areFriends(userIdA: string, userIdB: string) {
    const relationship = await this.findOne({
      where: {
        user: userIdA,
        friend: userIdB,
      },
    });

    return relationship ? true : false;
  }
}
