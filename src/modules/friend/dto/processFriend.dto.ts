'use strict';

import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

import { StatusFriendRequestEnum } from '../../../common/enum/enum';

export class ProcessFriendDto {
  @IsNotEmpty()
  @ApiProperty({
    enum: StatusFriendRequestEnum,
    description: 'status = "accepted" or "refused" or "canceled" ',
  })
  readonly status: StatusFriendRequestEnum;

  @ApiHideProperty()
  friendRequestId: string;

  @ApiHideProperty()
  userId: string;
}
