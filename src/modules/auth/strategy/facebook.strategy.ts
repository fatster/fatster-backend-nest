import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import PassportFacebookToken, * as FacebookTokenStrategy from 'passport-facebook-token';

import { ConfigService } from '../../../shared/services/config.service';
import { UserService } from '../../user/user.service';
import { FbProfileDto } from '../dto/fbProfile.dto';

@Injectable()
export class FacebookStrategy extends PassportStrategy(
  FacebookTokenStrategy,
  'facebook-token',
) {
  constructor(
    private readonly _userService: UserService,
    private readonly _configService: ConfigService,
  ) {
    super({
      clientID: _configService.get('APP_ID'),
      clientSecret: _configService.get('APP_SECRET'),
    });
  }

  async validate(
    accessToken: string,
    refreshToken: string,
    profile: PassportFacebookToken.Profile,
    done: (err: any, result: any) => void,
  ) {
    // console.log(profile);
    const fbProfile = new FbProfileDto(
      profile.emails[0].value,
      profile.name.givenName,
      profile.name.familyName,
      profile.photos[0].value,
      profile.id,
    );
    const user = await this._userService.findOrCreateFromFacebook(
      fbProfile,
    );
    return done(null, user);
  }
}
