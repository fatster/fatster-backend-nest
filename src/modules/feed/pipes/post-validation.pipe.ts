import {
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { flatMap } from 'lodash';

import {
  NutritionFoodType,
  PostTypeEnum,
} from '../../../common/enum/enum';
import { Post } from '../../../interfaces/Post';
import { CreateNutritionFoodDto } from '../dto/create-nutrition-food.dto';
import { CreatePostDto } from '../dto/create-post.dto';

const foodTypes = new Set(Object.values(NutritionFoodType));

@Injectable()
export class PostTypeValidationPipe implements PipeTransform {
  async transform({
    data: post,
  }: {
    data: Post;
  }): Promise<CreatePostDto> {
    // ADD HERE MORE POST TYPES WITH CORRECT VALIDATION
    if (PostTypeEnum.FOOD !== post.type) {
      throw new BadRequestException(
        'please provide supported type of post',
      );
    }
    const groups = [post.type];

    if (
      PostTypeEnum.FOOD === post.type &&
      post.foods &&
      post.foods.length
    ) {
      // done validation in pipe due code above doesnt work in
      // CreateNutritionFoodDto class
      // @ValidateNested({ each: true, always: true })
      // @Type(() => CreateNutritionFoodDto)
      const foodErrors = await Promise.all(
        post.foods.map(async (food) => {
          if (!foodTypes.has(food.portionType)) {
            throw new BadRequestException(
              'please provide correct food type',
            );
          }

          const foodGroup = [food.portionType];
          const foodEntityClass = plainToClass(
            CreateNutritionFoodDto,
            food,
            { groups: foodGroup },
          );

          return validate(foodEntityClass, { groups: foodGroup });
        }),
      );

      if (flatMap(foodErrors).length > 0) {
        throw new BadRequestException(foodErrors);
      }
    }

    const entityClass = plainToClass(CreatePostDto, post, { groups });

    const errors = await validate(entityClass, { groups });

    if (errors.length > 0) {
      throw new BadRequestException(errors);
    }
    return entityClass;
  }
}
