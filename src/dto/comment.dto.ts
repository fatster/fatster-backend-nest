import { ApiPropertyOptional } from '@nestjs/swagger';

import { AbstractDto } from '../common/dto/AbstractDto';
import { isDto, isDtos } from '../common/utils/entities';
import { CommentEntity } from '../entities/comment.entity';
import { PostDto } from './post.dto';
import { UserDto } from './user.dto';

export class CommentDto extends AbstractDto {
  @ApiPropertyOptional()
  text: string;

  @ApiPropertyOptional()
  post: PostDto;

  @ApiPropertyOptional()
  user: UserDto;

  @ApiPropertyOptional()
  parent: CommentDto;

  @ApiPropertyOptional()
  children: CommentDto[];

  @ApiPropertyOptional()
  likesCount?: number;

  mentionedUsersPseudo?: string[];

  constructor(comment: CommentEntity) {
    super(comment);
    this.text = comment.text;
    this.mentionedUsersPseudo = comment.mentionedUsersPseudo;
    this.post = isDto(comment.post)
      ? comment.post.toDto()
      : <any>comment.post;
    this.user = isDto(comment.user)
      ? comment.user.toDto()
      : <any>comment.user;
    this.children = isDtos(comment.children)
      ? comment.children.toDtos()
      : <any>comment.children;
    this.parent = isDto(comment.parent)
      ? comment.parent.toDto()
      : <any>comment.parent;
    this.likesCount = comment.likesCount;
  }
}
